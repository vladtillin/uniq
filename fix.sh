#!/bin/bash

docker-compose exec php-cli vendor/bin/php-cs-fixer fix src/
docker-compose exec php-cli vendor/bin/php-cs-fixer fix tests/
