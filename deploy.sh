#!/bin/bash
set -e

export COLOR="green"

export $(cat .env | sed "s/#.*//g")
export COMPOSE_INTERACTIVE_NO_CLI=1

echo "Stopping docker containers..."

docker-compose stop
docker-compose rm -f
echo "Building project..."
docker-compose build
docker-compose up -d

echo "Preparing doctrine..."

docker-compose exec -T php-cli bash -c "composer install"
docker-compose exec -T php-cli bash -c "php bin/console cache:clear"
docker-compose exec -T php-cli bash -c "php bin/console doctrine:cache:clear-meta"
docker-compose exec -T php-cli bash -c "php bin/console doctrine:migrations:migrate --allow-no-migration --no-interaction --no-debug"

docker-compose exec -T rabbitmq bash -c "rabbitmqctl delete_user guest"
docker-compose exec -T rabbitmq bash -c "rabbitmqctl add_user $RABBITMQ_USER $RABBITMQ_PASSWORD"
docker-compose exec -T rabbitmq bash -c "rabbitmqctl set_user_tags $RABBITMQ_USER administrator"
docker-compose exec -T rabbitmq bash -c "rabbitmqctl set_permissions $RABBITMQ_USER -p / \".*\" \".*\" \".*\""

bash php-doc.sh

docker ps -f name=doctor_tillin
