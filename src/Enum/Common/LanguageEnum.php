<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Enum\Common;

/**
 * Class LanguageEnum.
 *
 * List of all languages, available on doctor-tillin.
 *
 * @author freevan04@gmail.com
 */
class LanguageEnum
{
    public const DE = 'de';

    public const EN = 'en';

    public const ES = 'es';

    public const FR = 'fr';

    public const IT = 'it';

    public const PL = 'pl';

    public const RU = 'ru';

    public const SV = 'sv';

    public const UK = 'uk';
}
