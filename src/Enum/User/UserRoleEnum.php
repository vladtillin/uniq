<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Enum\User;

/**
 * Class UserRoleEnum.
 *
 * All user roles, available on doctor-tillin.
 *
 * @author freevan04@gmail.com
 */
class UserRoleEnum
{
    public const PATIENT = 'ROLE_PATIENT';

    public const DOCTOR = 'ROLE_DOCTOR';

    public const ADMIN = 'ROLE_ADMIN';

    public const OWNER = 'ROLE_OWNER';
}
