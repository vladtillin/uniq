<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Enum\Minio;

/**
 * Class MinioBucketsEnum.
 *
 * Enum for minio. Buckets name, policy etc.
 *
 * @author freevan04@gmail.com
 */
class MinioBucketsEnum
{
    public const USER_AVATAR = 'user';

    public const PUBLIC_BUCKET_POLICY = '{ "Version": "2012-10-17", "Statement": [ { "Action": [ "s3:GetBucketLocation", "s3:ListBucket" ], "Effect": "Allow", "Principal": { "AWS": [ "*" ] }, "Resource": [ "arn:aws:s3:::%s" ], "Sid": "" }, { "Action": [ "s3:GetObject" ], "Effect": "Allow", "Principal": { "AWS": [ "*" ] }, "Resource": [ "arn:aws:s3:::%s/*" ], "Sid": "" } ] }';
}
