<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Enum\Controller\Api\App\Verification;

/**
 * Class PhoneNumberVerificationTypeEnum.
 *
 * @author freevan04@gmail.com
 */
class PhoneNumberVerificationTypeEnum
{
    public const SMS = 'sms';

    public const TELEGRAM_BOT = 'telegram_bot';
}
