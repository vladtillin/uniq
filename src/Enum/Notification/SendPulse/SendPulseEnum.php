<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Enum\Notification\SendPulse;

/**
 * Class SendPulseEnum.
 *
 * Sendpulse enums, endpoint urls etc.
 *
 * @author freevan04@gmail.com
 */
class SendPulseEnum
{
    public const SENDPULSE_URL = 'https://api.sendpulse.com/';

    public const SENDPULSE_VIBER_SMS_URL = 'viber';

    public const SENDPULSE_SENDER_INFO = 'viber/senders';

    public const SENDPULSE_SMS_URL = 'sms/send';
}
