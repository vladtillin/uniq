<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\MessageHandler\Notification\Email;

use App\Message\Notification\Email\EmailNotificationMessage;
use Psr\Log\LoggerInterface;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Attribute as Messenger;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class EmailNotificationMessageHandler.
 *
 * Sending email message with RabbitMQ.
 *
 * @author freevan04@gmail.com
 */
#[Messenger\AsMessageHandler]
class EmailNotificationMessageHandler
{
    private string $senderEmail;

    private LoggerInterface $emailNotificationLogger;

    private Environment $twig;

    private MailerInterface $mailer;

    /**
     * @param MailerInterface     $mailer
     * @param string              $senderEmail
     * @param LoggerInterface     $emailNotificationLogger
     * @param Environment         $twig
     * @param MessageBusInterface $messageBus
     */
    public function __construct(MailerInterface $mailer, string $senderEmail, LoggerInterface $emailNotificationLogger, Environment $twig, MessageBusInterface $messageBus)
    {
        $this->senderEmail = $senderEmail;
        $this->emailNotificationLogger = $emailNotificationLogger;
        $this->twig = $twig;
        $this->mailer = $mailer;
    }

    /**
     * @param EmailNotificationMessage $emailNotificationMessage
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function __invoke(EmailNotificationMessage $emailNotificationMessage)
    {
        $emailMessageDTO = $emailNotificationMessage->getEmailMessageDTO();

        $email = (new Email())
        ->from($this->senderEmail)
        ->to($emailMessageDTO->email)
        ->subject($emailMessageDTO->subject)
        ->html($this->twig->render($emailMessageDTO->htmlTemplate, $emailMessageDTO->context), 'text/html');

        $this->mailer->send($email);

//        $transport = (new Swift_SmtpTransport($this->mailerHost, $this->mailerPort, 'tls'))
//            ->setUsername($this->senderUsername)
//            ->setPassword($this->senderPassword);
//
//        $mailer = new Swift_Mailer($transport);
//        $message = new Swift_Message();
//        $message
//            ->setFrom($this->senderEmail, 'Tillin Gamers Club')
//            ->setSender($emailMessageDTO->email)
//            ->setSubject($emailMessageDTO->subject)
//            ->setBody($this->twig->render($emailMessageDTO->htmlTemplate, $emailMessageDTO->context), 'text/html');
//
//        $mailer->send($message);
        $this->emailNotificationLogger->info(sprintf('Message for email %s was sent', $emailMessageDTO->email));
    }
}
