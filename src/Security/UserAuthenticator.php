<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Security;

use App\Entity\User\Security\AuthHistory;
use App\Manager\User\Security\AuthHistoryManager;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * Class UserAuthenticator.
 *
 * Authenticator service for user.
 *
 * @author freevan04@gmail.com
 */
class UserAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    private UrlGeneratorInterface $urlGenerator;

    private AuthHistoryManager $authHistoryManager;

    /**
     * @param UrlGeneratorInterface $urlGenerator
     * @param AuthHistoryManager    $authHistoryManager
     */
    public function __construct(UrlGeneratorInterface $urlGenerator, AuthHistoryManager $authHistoryManager)
    {
        $this->urlGenerator = $urlGenerator;
        $this->authHistoryManager = $authHistoryManager;
    }

    /**
     * Authenticating user.
     *
     * @param Request $request
     *
     * @return Passport
     */
    public function authenticate(Request $request): Passport
    {
        $email = $request->request->get('email', '');

        if (empty($email)) {
            $phoneNumber = $request->request->get('phoneNumber', '');

            $request->getSession()->setName(Security::LAST_USERNAME, $phoneNumber);

            return new Passport(
                new UserBadge($phoneNumber),
                new PasswordCredentials($request->request->get('password', '')),
                [
                    new CsrfTokenBadge('authenticate', $request->request->get('_csrf_token')),
                    new RememberMeBadge(),
                ]
            );
        }

        $request->getSession()->set(Security::LAST_USERNAME, $email);

        return new Passport(
            new UserBadge($email),
            new PasswordCredentials($request->request->get('password', '')),
            [
                new CsrfTokenBadge('authenticate', $request->request->get('_csrf_token')),
            ]
        );
    }

    /**
     * Some actions and redirect, if authentication is success.
     * AuthHistory creating and adding to database.
     *
     * @param Request        $request
     * @param TokenInterface $token
     * @param string         $firewallName
     *
     * @return Response|null
     *
     * @throws Exception
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            return new RedirectResponse($targetPath);
        }

        $authHistory = new AuthHistory($token->getUser(), (string) $token, $token->getUserIdentifier());
        $this->authHistoryManager->create($authHistory);

        return new RedirectResponse($this->urlGenerator->generate('app'));
    }

    /**
     * Login url.
     *
     * @param Request $request
     *
     * @return string
     */
    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }
}
