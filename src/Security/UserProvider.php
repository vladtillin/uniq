<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Security;

use App\Entity\User;
use App\Manager\UserManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class UserProvider.
 *
 * @author freevan04@gmail.com
 */
class UserProvider implements UserProviderInterface, PasswordUpgraderInterface
{
    private UserManager $userManager;

    private Security $security;

    private UserPasswordHasherInterface $userPasswordHasher;

    /**
     * @param UserManager                 $userManager
     * @param Security                    $security
     * @param UserPasswordHasherInterface $userPasswordHasher
     */
    public function __construct(UserManager $userManager, Security $security, UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userManager = $userManager;
        $this->security = $security;
        $this->userPasswordHasher = $userPasswordHasher;
    }

    /**
     * Upgrading user's password.
     *
     * @param PasswordAuthenticatedUserInterface $user
     * @param string                             $newHashedPassword
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $user->setPassword($this->userPasswordHasher, $newHashedPassword);

        $this->userManager->update($user);
    }

    /**
     * Updating user for session.
     *
     * @param UserInterface $user
     *
     * @return UserInterface|User
     */
    public function refreshUser(UserInterface $user): UserInterface|User
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException();
        }

        return $this->userManager->findUserById($user->getId());
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass(string $class): bool
    {
        return User::class === $class || is_subclass_of($class, User::class);
    }

    /**
     * Loading user by nickname.
     *
     * @param string $identifier
     *
     * @return UserInterface
     */
    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        $user = $this->userManager->findUserByNickname($identifier);
        if (!$user instanceof User) {
            throw new UserNotFoundException();
        }

        return $user;
    }
}
