<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\ValueObject\Common;

use App\Enum\Common\LanguageEnum;

/**
 * Class LanguageValueObject.
 *
 * Value object to get the list of all available languages and validating language.
 *
 * @author freevan04@gmail.com
 */
class LanguageValueObject
{
    /**
     * @return array
     */
    public static function getLanguageList(): array
    {
        return (new \ReflectionClass(LanguageEnum::class))->getConstants();
    }

    /**
     * Example for $languageKey: es.
     *
     * @param string $languageKey
     *
     * @return bool
     */
    public static function validateLanguage(string $languageKey): bool
    {
        $languages = LanguageValueObject::getLanguageList();

        foreach ($languages as $language) {
            if ($language === $languageKey) {
                return true;
            }
        }

        return false;
    }
}
