<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Twig\SEO;

use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class TitleExtension.
 *
 * Twig functions.
 *
 * @author freevan04@gmail.com
 */
class TitleExtension extends AbstractExtension
{
    private Environment $twig;

    private TranslatorInterface $translator;

    /**
     * @param Environment         $twig
     * @param TranslatorInterface $translator
     */
    public function __construct(Environment $twig, TranslatorInterface $translator)
    {
        $this->twig = $twig;
        $this->translator = $translator;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('appHeader', [$this, 'renderAppHeader']),
            new TwigFunction('appMenu', [$this, 'renderAppMenu']),
            new TwigFunction('title', [$this, 'resolveTitle']),
        ];
    }

    /**
     * Twig function to get rendered header for page.
     *
     * @return string
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function renderAppHeader(): string
    {
        return $this->twig->render('app/blocks/header.html.twig');
    }

    /**
     * Twig function to render menu for app page.
     *
     * @return string
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function renderAppMenu(): string
    {
        return $this->twig->render('app/blocks/menu.html.twig');
    }

    /**
     * Twig function to choose the right title for page (from translations).
     *
     * @param string $templatePath
     *
     * @return string
     */
    public function resolveTitle(string $templatePath): string
    {
        return $this->translator->trans('titles.'.$templatePath, domain: 'titles');
    }
}
