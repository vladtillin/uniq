<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Twig;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class AppExtension.
 *
 * Twig functions.
 *
 * @author freevan04@gmail.com
 */
class AppExtension extends AbstractExtension
{
    private TokenStorageInterface $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('getUser', [$this, 'getUser']),
        ];
    }

    /**
     * Function to get user entity in twig file.
     *
     * @return UserInterface|null
     */
    public function getUser(): ?UserInterface
    {
        return $this->tokenStorage->getToken()->getUser();
    }
}
