<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\DTOBuilder\Form\Type\App\Settings;

use App\DTO\Form\Type\App\Settings\ProfileDataEditTypeDTO;
use App\Entity\User\ProfileData;
use App\Exception\InvalidArgumentException;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Form\FormInterface;

/**
 * Class ProfileDataEditTypeDTOBuilder.
 *
 * Builder for App\DTO\Form\Type\App\Settings\ProfileDataEditTypeDTO.
 *
 * @author freevan04@gmail.com
 */
class ProfileDataEditTypeDTOBuilder
{
    /**
     * Set data to form, and takes data from App\Entity\User\ProfileData entity.
     *
     * @param FormInterface $form
     * @param ProfileData   $profileData
     *
     * @return FormInterface
     */
    public static function setFormData(FormInterface $form, ProfileData $profileData): FormInterface
    {
        $profileDataEditTypeDTO = self::build($profileData);

        $form->get('name')->setData($profileDataEditTypeDTO->name);
        $form->get('surname')->setData($profileDataEditTypeDTO->surname);
        $form->get('patronymic')->setData($profileDataEditTypeDTO->patronymic);
        $form->get('birthDate')->setData($profileDataEditTypeDTO->birthDate);
        $form->get('country')->setData($profileDataEditTypeDTO->country);
        $form->get('city')->setData($profileDataEditTypeDTO->city);

        return $form;
    }

    /**
     * Change data in App\Entity\User\ProfileData, and takes data from App\DTO\Form\Type\App\Settings\ProfileDataEditTypeDTO.
     *
     * @param ProfileDataEditTypeDTO $profileDataEditTypeDTO
     * @param ProfileData            $profileData
     * @param string|null            $avatar
     *
     * @return ProfileData
     *
     * @throws InvalidArgumentException
     */
    public static function buildFromDTO(ProfileDataEditTypeDTO $profileDataEditTypeDTO, ProfileData $profileData, ?string $avatar = null): ProfileData
    {
        if (null === $profileData->getPatronymic()) {
            $profileData->setPatronymic($profileDataEditTypeDTO->patronymic);
        }

        if (null === $profileData->getBirthDate()) {
            $profileData->setBirthDate($profileDataEditTypeDTO->birthDate);
        }

        if (null === $profileData->getCountry()) {
            $profileData->setCountry($profileDataEditTypeDTO->country);
        }

        if (null === $profileData->getCity()) {
            $profileData->setCity($profileDataEditTypeDTO->city);
        }

        $profileData->setAvatar($avatar);

        return $profileData;
    }

    /**
     * Build App\DTO\Form\Type\App\Settings\ProfileDataEditTypeDTO from App\Entity\User\ProfileData.
     *
     * @param ProfileData $profileData
     *
     * @return ProfileDataEditTypeDTO
     */
    #[Pure]
    public static function build(ProfileData $profileData): ProfileDataEditTypeDTO
    {
        $profileDataEditTypeDTO = new ProfileDataEditTypeDTO();
        $profileDataEditTypeDTO->name = $profileData->getName();
        $profileDataEditTypeDTO->surname = $profileData->getSurname();
        $profileDataEditTypeDTO->patronymic = $profileData->getPatronymic();
        $profileDataEditTypeDTO->birthDate = $profileData->getBirthDate();
        $profileDataEditTypeDTO->avatar = null;
        $profileDataEditTypeDTO->country = $profileData->getCountry();
        $profileDataEditTypeDTO->city = $profileData->getCity();

        return $profileDataEditTypeDTO;
    }
}
