<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\DTOBuilder\Controller\Api\App;

use App\DTO\Controller\Api\App\NotificationsSettingsApiDTO;
use App\Entity\User\Notification\Notification;
use JetBrains\PhpStorm\Pure;

/**
 * Class NotificationsSettingsApiDTOBuilder.
 *
 * @author freevan04@gmail.com
 */
class NotificationsSettingsApiDTOBuilder
{
    /**
     * @param Notification $notification
     *
     * @return NotificationsSettingsApiDTO
     */
    #[Pure]
    public static function build(Notification $notification): NotificationsSettingsApiDTO
    {
        $notificationsSettingsApiDTO = new NotificationsSettingsApiDTO();
        $notificationsSettingsApiDTO->advertisingNotifications = $notification->isAdvertisingNotifications();
        $notificationsSettingsApiDTO->updatesNotifications = $notification->isUpdatesNotifications();
        $notificationsSettingsApiDTO->messagesNotifications = $notification->isMessagesNotifications();

        return $notificationsSettingsApiDTO;
    }

    /**
     * @param Notification                $notification
     * @param NotificationsSettingsApiDTO $notificationsSettingsApiDTO
     *
     * @return Notification
     */
    public static function modifyNotificationEntity(Notification $notification, NotificationsSettingsApiDTO $notificationsSettingsApiDTO): Notification
    {
        $notification->setAdvertisingNotifications($notificationsSettingsApiDTO->advertisingNotifications);
        $notification->setMessagesNotifications($notificationsSettingsApiDTO->messagesNotifications);
        $notification->setUpdatesNotifications($notificationsSettingsApiDTO->updatesNotifications);

        return $notification;
    }
}
