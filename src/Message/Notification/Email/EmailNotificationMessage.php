<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Message\Notification\Email;

use App\DTO\Notification\Email\EmailMessageDTO;

/**
 * Class EmailNotificationMessage.
 *
 * DTO to save data for email to rabbitmq.
 *
 * @author freevan04@gmail.com
 */
class EmailNotificationMessage
{
    private EmailMessageDTO $emailMessageDTO;

    /**
     * @param EmailMessageDTO $emailMessageDTO
     */
    public function __construct(EmailMessageDTO $emailMessageDTO)
    {
        $this->emailMessageDTO = $emailMessageDTO;
    }

    /**
     * @return EmailMessageDTO
     */
    public function getEmailMessageDTO(): EmailMessageDTO
    {
        return $this->emailMessageDTO;
    }
}
