<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Manager\User\Security;

use App\Entity\User\Security\AuthHistory;
use App\Repository\User\Security\AuthHistoryRepository;

/**
 * Class AuthHistoryManager.
 *
 * @author freevan04@gmail.com
 */
class AuthHistoryManager
{
    private AuthHistoryRepository $authHistoryRepository;

    /**
     * @param AuthHistoryRepository $authHistoryRepository
     */
    public function __construct(AuthHistoryRepository $authHistoryRepository)
    {
        $this->authHistoryRepository = $authHistoryRepository;
    }

    /**
     * @param AuthHistory $authHistory
     *
     * @return AuthHistory
     */
    public function create(AuthHistory $authHistory): AuthHistory
    {
        $this->authHistoryRepository->add($authHistory, true);

        return $authHistory;
    }

    /**
     * @param AuthHistory $authHistory
     *
     * @return AuthHistory
     */
    public function update(AuthHistory $authHistory): AuthHistory
    {
        $this->authHistoryRepository->save($authHistory);

        return $authHistory;
    }
}
