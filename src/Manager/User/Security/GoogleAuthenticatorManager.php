<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Manager\User\Security;

use App\Entity\User\Security\GoogleAuthenticator;
use App\Repository\User\Security\GoogleAuthenticatorRepository;

/**
 * Class GoogleAuthenticatorManager.
 *
 * @author freevan04@gmail.com
 */
class GoogleAuthenticatorManager
{
    private GoogleAuthenticatorRepository $googleAuthenticatorRepository;

    /**
     * @param GoogleAuthenticatorRepository $googleAuthenticatorRepository
     */
    public function __construct(GoogleAuthenticatorRepository $googleAuthenticatorRepository)
    {
        $this->googleAuthenticatorRepository = $googleAuthenticatorRepository;
    }

    /**
     * @param GoogleAuthenticator $googleAuthenticator
     *
     * @return GoogleAuthenticator
     */
    public function create(GoogleAuthenticator $googleAuthenticator): GoogleAuthenticator
    {
        $this->googleAuthenticatorRepository->add($googleAuthenticator, true);

        return $googleAuthenticator;
    }

    /**
     * @param GoogleAuthenticator $googleAuthenticator
     *
     * @return GoogleAuthenticator
     */
    public function update(GoogleAuthenticator $googleAuthenticator): GoogleAuthenticator
    {
        $this->googleAuthenticatorRepository->save();

        return $googleAuthenticator;
    }

    /**
     * @param GoogleAuthenticator $googleAuthenticator
     */
    public function remove(GoogleAuthenticator $googleAuthenticator): void
    {
        $this->googleAuthenticatorRepository->remove($googleAuthenticator);
    }
}
