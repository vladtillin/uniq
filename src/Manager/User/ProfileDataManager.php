<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Manager\User;

use App\Entity\User;
use App\Entity\User\ProfileData;
use App\Exception\EntityNotFoundException;
use App\Repository\User\ProfileDataRepository;

/**
 * Class ProfileDataManager.
 *
 * @author freevan04@gmail.com
 */
class ProfileDataManager
{
    private ProfileDataRepository $profileDataRepository;

    /**
     * @param ProfileDataRepository $profileDataRepository
     */
    public function __construct(ProfileDataRepository $profileDataRepository)
    {
        $this->profileDataRepository = $profileDataRepository;
    }

    /**
     * @param ProfileData $profileData
     *
     * @return ProfileData
     */
    public function create(ProfileData $profileData): ProfileData
    {
        $this->profileDataRepository->add($profileData);

        return $profileData;
    }

    /**
     * @param ProfileData $profileData
     *
     * @return ProfileData
     */
    public function update(ProfileData $profileData): ProfileData
    {
        $this->profileDataRepository->save($profileData);

        return $profileData;
    }

    /**
     * @param User $user
     *
     * @return ProfileData
     */
    public function getProfileDataForUser(User $user): ProfileData
    {
        $profileData = $this->profileDataRepository->findOneBy(['user' => $user]);

        if (null === $profileData) {
            throw new EntityNotFoundException(sprintf('ProfileData for user with id %s not found', $user->getId()));
        }

        return $profileData;
    }
}
