<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Manager\User\Notification;

use App\Entity\User\Notification\Notification;
use App\Repository\User\Notification\NotificationRepository;

/**
 * Class NotificationManager.
 *
 * @author freevan04@gmail.com
 */
class NotificationManager
{
    private NotificationRepository $notificationRepository;

    /**
     * @param NotificationRepository $notificationRepository
     */
    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * @param Notification $notification
     *
     * @return Notification
     */
    public function update(Notification $notification): Notification
    {
        $this->notificationRepository->save($notification);

        return $notification;
    }
}
