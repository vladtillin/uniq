<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Manager\User\Doctor;

use App\Entity\User\Doctor\DoctorVerification;
use App\Repository\User\Doctor\DoctorVerificationRepository;

/**
 * Class DoctorVerificationManager.
 *
 * @author freevan04@gmail.com
 */
class DoctorVerificationManager
{
    private DoctorVerificationRepository $doctorVerificationRepository;

    /**
     * @param DoctorVerificationRepository $doctorVerificationRepository
     */
    public function __construct(DoctorVerificationRepository $doctorVerificationRepository)
    {
        $this->doctorVerificationRepository = $doctorVerificationRepository;
    }

    /**
     * @param DoctorVerification $doctorVerification
     *
     * @return DoctorVerification
     */
    public function create(DoctorVerification $doctorVerification): DoctorVerification
    {
        $this->doctorVerificationRepository->add($doctorVerification);

        return $doctorVerification;
    }

    /**
     * @param DoctorVerification $doctorVerification
     *
     * @return DoctorVerification
     */
    public function update(DoctorVerification $doctorVerification): DoctorVerification
    {
        $this->doctorVerificationRepository->save();

        return $doctorVerification;
    }

    /**
     * @param DoctorVerification $doctorVerification
     *
     * @return DoctorVerification
     */
    public function delete(DoctorVerification $doctorVerification): DoctorVerification
    {
        $doctorVerification->delete();

        $this->doctorVerificationRepository->save();

        return $doctorVerification;
    }

    /**
     * Verifying doctor.
     *
     * @param DoctorVerification $doctorVerification
     *
     * @return DoctorVerification
     */
    public function verify(DoctorVerification $doctorVerification): DoctorVerification
    {
        $doctorVerification->getDoctorUserType()->setVerified(true);
        // TODO: Add event, to send email/sms for verification

        $this->doctorVerificationRepository->save();

        return $doctorVerification;
    }
}
