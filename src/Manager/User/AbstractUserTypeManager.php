<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Manager\User;

use App\Entity\User\AbstractUserType;
use App\Repository\User\AbstractUserTypeRepository;

/**
 * Class AbstractUserTypeManager.
 *
 * @author freevan04@gmail.com
 */
class AbstractUserTypeManager
{
    private AbstractUserTypeRepository $abstractUserTypeRepository;

    /**
     * @param AbstractUserTypeRepository $abstractUserTypeRepository
     */
    public function __construct(AbstractUserTypeRepository $abstractUserTypeRepository)
    {
        $this->abstractUserTypeRepository = $abstractUserTypeRepository;
    }

    /**
     * @param AbstractUserType $abstractUserType
     *
     * @return AbstractUserType
     */
    public function create(AbstractUserType $abstractUserType): AbstractUserType
    {
        $this->abstractUserTypeRepository->add($abstractUserType);

        return $abstractUserType;
    }

    /**
     * @param AbstractUserType $abstractUserType
     *
     * @return AbstractUserType
     */
    public function update(AbstractUserType $abstractUserType): AbstractUserType
    {
        $this->abstractUserTypeRepository->save($abstractUserType);

        return $abstractUserType;
    }
}
