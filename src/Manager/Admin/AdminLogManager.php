<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Manager\Admin;

use App\Entity\Admin\AdminLog;
use App\Repository\Admin\AdminLogRepository;

/**
 * Class AdminLogManager.
 */
class AdminLogManager
{
    private AdminLogRepository $adminLogRepository;

    /**
     * @param AdminLogRepository $adminLogRepository
     */
    public function __construct(AdminLogRepository $adminLogRepository)
    {
        $this->adminLogRepository = $adminLogRepository;
    }

    /**
     * @param AdminLog $adminLog
     *
     * @return AdminLog
     */
    public function create(AdminLog $adminLog): AdminLog
    {
        $this->adminLogRepository->add($adminLog);

        return $adminLog;
    }

    /**
     * @param AdminLog $adminLog
     *
     * @return AdminLog
     */
    public function update(AdminLog $adminLog): AdminLog
    {
        $this->adminLogRepository->save($adminLog);

        return $adminLog;
    }
}
