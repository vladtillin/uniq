<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Manager;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Class UserManager.
 *
 * @author freevan04@gmail.com
 */
class UserManager
{
    private UserRepository $userRepository;

    private UserPasswordHasherInterface $userPasswordHasher;

    /**
     * @param UserRepository              $userRepository
     * @param UserPasswordHasherInterface $userPasswordHasher
     */
    public function __construct(UserRepository $userRepository, UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userRepository = $userRepository;
        $this->userPasswordHasher = $userPasswordHasher;
    }

    /**
     * @param User $user
     *
     * @return User
     */
    public function create(User $user): User
    {
        $this->userRepository->add(entity: $user, flush: true);

        return $user;
    }

    /**
     * @param User $user
     *
     * @return User
     */
    public function update(User $user): User
    {
        $this->userRepository->save($user);

        return $user;
    }

    /**
     * @param User $user
     *
     * @return User
     */
    public function verifyEmail(User $user): User
    {
        $user->setEmailVerified(true);
        $this->userRepository->save($user);

        return $user;
    }

    /**
     * @param User $user
     *
     * @return User
     */
    public function verifyPhoneNumber(User $user): User
    {
        $user->setPhoneNumberVerified(true);
        $this->userRepository->save();

        return $user;
    }

    /**
     * @param User   $user
     * @param string $newPassword
     *
     * @return User
     */
    public function changeUserPassword(User $user, string $newPassword): User
    {
        $user->setPassword($this->userPasswordHasher, $newPassword);

        $this->userRepository->save($user);

        return $user;
    }

    /**
     * @param string $email
     *
     * @return User|null
     */
    public function findUserByEmail(string $email): ?User
    {
        return $this->userRepository->findOneBy(['email' => $email]);
    }

    /**
     * @param string $phoneNumber
     *
     * @return User|null
     */
    public function findUserByPhoneNumber(string $phoneNumber): ?User
    {
        return $this->userRepository->findOneBy(['phoneNumber' => $phoneNumber]);
    }

    /**
     * @param string $nickname
     *
     * @return User|null
     */
    public function findUserByNickname(string $nickname): ?User
    {
        return $this->userRepository->findOneBy(['nickname' => $nickname]);
    }

    /**
     * @param int $id
     *
     * @return User|null
     */
    public function findUserById(int $id): ?User
    {
        return $this->userRepository->find($id);
    }
}
