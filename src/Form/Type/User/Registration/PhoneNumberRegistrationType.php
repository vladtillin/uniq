<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Form\Type\User\Registration;

use App\DTO\User\Registration\UserPhoneNumberRegistrationDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PhoneNumberRegistrationType.
 *
 * Form for user registration with phone number.
 *
 * @author freevan04@gmail.com
 */
class PhoneNumberRegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phoneNumber', TextType::class, [
                'label' => 'forms.phone_number.phone_number_field.label',
            ])
            ->add('nickname', TextType::class, [
                'label' => 'forms.email.nickname_field.label',
            ])
            ->add('name', TextType::class, [
                'label' => 'forms.email.name_field.label',
            ])
            ->add('surname', TextType::class, [
                'label' => 'forms.email.surname_field.label',
            ])
            ->add('password', PasswordType::class, [
                'label' => 'forms.email.password_field.label',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'forms.email.submit.label',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserPhoneNumberRegistrationDTO::class,
            'translation_domain' => 'landing_pages_registration',
        ]);
    }
}
