<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Form\Type\User\Registration;

use App\DTO\User\Registration\UserEmailRegistrationDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EmailRegistrationType.
 *
 * Form for user registration with email.
 *
 * @author freevan04@gmail.com
 */
class EmailRegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'forms.email.email_field.label',
            ])
            ->add('nickname', TextType::class, [
                'label' => 'forms.email.nickname_field.label',
            ])
            ->add('name', TextType::class, [
                'label' => 'forms.email.name_field.label',
            ])
            ->add('surname', TextType::class, [
                'label' => 'forms.email.surname_field.label',
            ])
            ->add('password', PasswordType::class, [
                'label' => 'forms.email.password_field.label',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'forms.email.submit.label',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserEmailRegistrationDTO::class,
            'translation_domain' => 'landing_pages_registration',
        ]);
    }
}
