<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Form\Type\App\Settings;

use App\DTO\Form\Type\App\Settings\ProfileDataEditTypeDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

/**
 * Class ProfileDataEditType.
 *
 * Form for edit user profile data (Like avatar, country, city etc.).
 *
 * @author freevan04@gmail.com
 */
class ProfileDataEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'mapped' => false,
            ])
            ->add('surname', TextType::class, [
                'required' => true,
                'mapped' => false,
            ])
            ->add('patronymic', TextType::class, [
                'required' => false,
                'mapped' => false,
            ])
            ->add('birthDate', DateType::class, [
                'required' => false,
                'mapped' => false,
            ])
            ->add('avatar', FileType::class, [
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '40M',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpg',
                            'image/webp',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image',
                    ]),
                ],
            ])
            ->add('country', TextType::class, [
                'required' => false,
                'mapped' => false,
            ])
            ->add('city', TextType::class, [
                'required' => false,
                'mapped' => false,
            ])
            ->add('submit', SubmitType::class, []);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'empty_data' => function (FormInterface $form) {
                $profileDataEditTypeDTO = new ProfileDataEditTypeDTO();

                $profileDataEditTypeDTO->name = $form->get('name')->getData();
                $profileDataEditTypeDTO->surname = $form->get('surname')->getData();
                $profileDataEditTypeDTO->patronymic = $form->get('patronymic')->getData();
                $profileDataEditTypeDTO->birthDate = $form->get('birthDate')->getData();
                $profileDataEditTypeDTO->avatar = $form->get('avatar')->getData();
                $profileDataEditTypeDTO->country = $form->get('country')->getData();
                $profileDataEditTypeDTO->city = $form->get('city')->getData();

                return $profileDataEditTypeDTO;
            },
            'mapped' => false,
        ]);
    }
}
