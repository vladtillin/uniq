<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\DTO\Notification\Email;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class EmailMessageDTO.
 *
 * DTO for sending email message to user.
 *
 * @author freevan04@gmail.com
 */
class EmailMessageDTO
{
    /**
     * @var string receiver email
     */
    #[Assert\Email]
    public string $email;

    /**
     * @var string subject of the email message
     */
    #[Assert\NotBlank]
    public string $subject;

    /**
     * @var string Path to twig template ('base.html.twig' for example).
     */
    #[Assert\NotBlank]
    public string $htmlTemplate;

    /**
     * @var array some variables in template
     */
    #[Assert\NotBlank]
    public array $context = [];
}
