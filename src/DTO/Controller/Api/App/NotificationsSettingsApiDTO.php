<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\DTO\Controller\Api\App;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class NotificationsSettingsApiDTO.
 *
 * Notification settings for user, DTO.
 *
 * @author freevan04@gmail.com
 */
class NotificationsSettingsApiDTO
{
    #[
        Serializer\SerializedName('messagesNotifications'),
        Serializer\Type('boolean'),
    ]
    public bool $messagesNotifications = true;

    #[
        Serializer\SerializedName('updatesNotifications'),
        Serializer\Type('boolean'),
    ]
    public bool $updatesNotifications = true;

    #[
        Serializer\SerializedName('advertisingNotifications'),
        Serializer\Type('boolean'),
    ]
    public bool $advertisingNotifications = true;
}
