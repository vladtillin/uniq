<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\DTO\Secure\TwoFactorAuthentication;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class GoogleAuthenticatorSecretDTO.
 *
 * Google authenticator DTO.
 *
 * @author freevan04@gmail.com
 */
class GoogleAuthenticatorSecretDTO
{
    /**
     * @var string secret key
     */
    #[Assert\NotBlank]
    public string $secretKey;

    /**
     * @var string the url of QRCode image
     */
    #[
        Assert\NotBlank,
        Assert\Url,
    ]
    public string $QRCodeUrl;
}
