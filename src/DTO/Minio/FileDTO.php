<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\DTO\Minio;

/**
 * Class FileDTO.
 *
 * DTO for uploading file to minio.
 *
 * @author freevan04@gmail.com
 */
class FileDTO
{
    /**
     * @var string path to file on our server
     */
    public string $path;

    /**
     * @var string Type of the file (image/png, image/jpg, application/pdf etc.).
     */
    public string $type;

    /**
     * @var string name for file in minio
     */
    public string $minioFilename;

    /**
     * @var string bucket in minio
     */
    public string $minioBucketName;
}
