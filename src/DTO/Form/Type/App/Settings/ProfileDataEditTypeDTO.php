<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\DTO\Form\Type\App\Settings;

use DateTime;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProfileDataEditTypeDTO.
 *
 * DTO for ProfileData form.
 *
 * @author freevan04@gmail.com
 */
class ProfileDataEditTypeDTO
{
    #[Assert\NotBlank]
    public string $name;

    #[Assert\NotBlank]
    public string $surname;

    public ?string $patronymic;

    public ?DateTime $birthDate;

    public ?UploadedFile $avatar;

    public ?string $country;

    public ?string $city;
}
