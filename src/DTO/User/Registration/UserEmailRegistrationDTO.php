<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\DTO\User\Registration;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserEmailRegistrationDTO.
 *
 * DTO for user registration form by email.
 *
 * @author freevan04@gmail.com
 */
class UserEmailRegistrationDTO
{
    #[
        Assert\Email,
        Assert\NotBlank,
    ]
    public string $email;

    #[Assert\NotBlank]
    public string $nickname;

    #[Assert\NotBlank]
    public string $name;

    #[Assert\NotBlank]
    public string $surname;

    #[Assert\NotBlank]
    public string $password;
}
