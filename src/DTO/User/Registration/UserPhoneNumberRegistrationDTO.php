<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\DTO\User\Registration;

use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UserPhoneNumberRegistrationDTO.
 *
 * DTO for user registration form by phone number.
 *
 * @author freevan04@gmail.com
 */
class UserPhoneNumberRegistrationDTO
{
    #[NotBlank]
    public string $phoneNumber;

    #[NotBlank]
    public string $nickname;

    #[NotBlank]
    public string $name;

    #[NotBlank]
    public string $surname;

    #[NotBlank]
    public string $password;
}
