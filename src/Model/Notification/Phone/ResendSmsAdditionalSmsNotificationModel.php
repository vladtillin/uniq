<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Model\Notification\Phone;

/**
 * Class ResendSmsAdditionalSmsNotificationModel.
 *
 * This is data for sms, when viber message wasn't delivered, or the user doesn't have viber at all.
 *
 * @see https://sendpulse.com/ru/integrations/api/viber
 *
 * @author freevan04@gmail.com
 */
class ResendSmsAdditionalSmsNotificationModel
{
    private bool $status;

    private string $smsText = '';

    private string $smsSenderName = '';

    /**
     * @param bool $status
     */
    public function __construct(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getSmsSenderName(): string
    {
        return $this->smsSenderName;
    }

    /**
     * @param string $smsSenderName
     *
     * @return $this
     */
    public function setSmsSenderName(string $smsSenderName): static
    {
        $this->smsSenderName = $smsSenderName;

        return $this;
    }

    /**
     * @return string
     */
    public function getSmsText(): string
    {
        return $this->smsText;
    }

    /**
     * @param string $smsText
     *
     * @return $this
     */
    public function setSmsText(string $smsText): static
    {
        $this->smsText = $smsText;

        return $this;
    }
}
