<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Model\Notification\Phone;

use DateTime;
use JetBrains\PhpStorm\Pure;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class SmsNotificationModel.
 *
 * Sms notification.
 *
 * @see https://sendpulse.com/ru/integrations/api/bulk-sms
 *
 * @author freevan04@gmail.com
 */
class SmsNotificationModel
{
    #[Serializer\SerializedName(name: 'sender')]
    private string $sender;

    #[Serializer\SerializedName(name: 'phones')]
    private array $phones;

    #[Serializer\SerializedName(name: 'body')]
    private string $body;

    #[Serializer\SerializedName(name: 'transliterate')]
    private bool $transliterate = false;

    #[Serializer\SerializedName(name: 'emulate')]
    private bool $emulate = false;

    #[Serializer\SerializedName(name: 'route')]
    private SmsNotificationRouteModel $route;

    #[Serializer\SerializedName(name: 'date')]
    private string $date;

    /**
     * @param string $sender
     * @param array  $phones
     * @param string $body
     * @param string $date
     */
    #[Pure]
    public function __construct(string $sender, array $phones, string $body, string $date = 'now')
    {
        $this->sender = $sender;
        $this->phones = $phones;
        $this->body = $body;
        $this->date = $date;
        $this->route = new SmsNotificationRouteModel();
    }

    /**
     * @return string
     */
    public function getSender(): string
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     *
     * @return $this
     */
    public function setSender(string $sender): static
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * @return array
     */
    public function getPhones(): array
    {
        return $this->phones;
    }

    /**
     * @param array $phones
     *
     * @return $this
     */
    public function setPhones(array $phones): static
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     *
     * @return $this
     */
    public function setBody(string $body): static
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return bool
     */
    public function isTransliterate(): bool
    {
        return $this->transliterate;
    }

    /**
     * @param bool $transliterate
     *
     * @return $this
     */
    public function setTransliterate(bool $transliterate): static
    {
        $this->transliterate = $transliterate;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmulate(): bool
    {
        return $this->emulate;
    }

    /**
     * @param bool $emulate
     *
     * @return $this
     */
    public function setEmulate(bool $emulate): static
    {
        $this->emulate = $emulate;

        return $this;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string|DateTime $date
     *
     * @return $this
     */
    public function setDate(string|DateTime $date): static
    {
        if ($date instanceof DateTime) {
            $this->date = $date->format('Y-m-d G:i:s');
        } else {
            $this->date = $date;
        }

        return $this;
    }
}
