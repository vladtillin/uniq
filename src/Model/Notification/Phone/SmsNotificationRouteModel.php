<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Model\Notification\Phone;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class SmsNotificationRouteModel.
 *
 * Special canal for sms in ua.
 *
 * @see https://sendpulse.com/ru/integrations/api/bulk-sms
 *
 * @author freevan04@gmail.com
 */
class SmsNotificationRouteModel
{
    #[Serialized\SerializedName(name: 'sim_ua')]
    private string $ua = 'sim_ua';
}
