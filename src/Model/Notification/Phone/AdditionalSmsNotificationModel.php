<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Model\Notification\Phone;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class AdditionalSmsNotificationModel.
 *
 * Additional data for viber sms.
 *
 * @see https://sendpulse.com/ru/integrations/api/viber
 *
 * @author freevan04@gmail.com
 */
class AdditionalSmsNotificationModel
{
    #[Serializer\SerializedName(name: 'resend_sms')]
    private ResendSmsAdditionalSmsNotificationModel $resendSms;

    /**
     * @param ResendSmsAdditionalSmsNotificationModel $resendSms
     */
    public function __construct(ResendSmsAdditionalSmsNotificationModel $resendSms)
    {
        $this->resendSms = $resendSms;
    }

    /**
     * @return ResendSmsAdditionalSmsNotificationModel
     */
    public function getResendSms(): ResendSmsAdditionalSmsNotificationModel
    {
        return $this->resendSms;
    }

    /**
     * @param ResendSmsAdditionalSmsNotificationModel $resendSms
     *
     * @return $this
     */
    public function setResendSms(ResendSmsAdditionalSmsNotificationModel $resendSms): static
    {
        $this->resendSms = $resendSms;

        return $this;
    }
}
