<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Model\Notification\Phone;

use App\Exception\InvalidArgumentException;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ViberAndSmsNotificationModel.
 *
 * Viber messages, and sms after that.
 *
 * @see https://sendpulse.com/ru/integrations/api/viber
 *
 * @author freevan04@gmail.com
 */
class ViberAndSmsNotificationModel
{
    #[
        Serializer\SerializedName(name: 'task_name'),
        Assert\NotBlank,
    ]
    private string $taskName;

    #[
        Serializer\SerializedName(name: 'message_type'),
        Assert\NotBlank,
    ]
    private int $messageType;

    #[
        Serializer\SerializedName,
        Assert\NotBlank,
    ]
    private int $senderId;

    #[
        Serializer\SerializedName(name: 'message_live_time'),
        Assert\NotBlank,
    ]
    private int $messageLiveTime;

    #[
        Serializer\SerializedName(name: 'send_date'),
        Assert\NotBlank,
    ]
    private string $sendDate;

    #[Serializer\SerializedName(name: 'recipients')]
    private array $recipients = [];

    #[
        Serializer\SerializedName(name: 'message'),
        Assert\NotBlank,
    ]
    private string $message;

    #[
        Serializer\SerializedName(name: 'additional'),
        Assert\NotBlank,
    ]
    private AdditionalSmsNotificationModel $additional;

    /**
     * @param string                         $taskName
     * @param int                            $messageType
     * @param int                            $senderId
     * @param int                            $messageLiveTime
     * @param string                         $sendDate
     * @param array                          $recipients
     * @param string                         $message
     * @param AdditionalSmsNotificationModel $additional
     *
     * @throws InvalidArgumentException
     */
    public function __construct(string $taskName, int $messageType, int $senderId, int $messageLiveTime, string $sendDate, array $recipients, string $message, AdditionalSmsNotificationModel $additional)
    {
        $this->taskName = $taskName;
        $this->setMessageType($messageType);
        $this->senderId = $senderId;
        $this->setMessageLiveTime($messageLiveTime);
        $this->sendDate = $sendDate;
        $this->recipients = $recipients;
        $this->setMessage($message);
        $this->additional = $additional;
    }

    /**
     * @return string
     */
    public function getTaskName(): string
    {
        return $this->taskName;
    }

    /**
     * @param string $taskName
     *
     * @return $this
     */
    public function setTaskName(string $taskName): static
    {
        $this->taskName = $taskName;

        return $this;
    }

    /**
     * @return int
     */
    public function getMessageType(): int
    {
        return $this->messageType;
    }

    /**
     * @param int $messageType
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setMessageType(int $messageType): static
    {
        if (2 !== $messageType && 3 !== $messageType) {
            throw new InvalidArgumentException(sprintf('Message type is %s, but must be 2 or 3', $messageType));
        }

        $this->messageType = $messageType;

        return $this;
    }

    /**
     * @return int
     */
    public function getSenderId(): int
    {
        return $this->senderId;
    }

    /**
     * @param int $senderId
     *
     * @return $this
     */
    public function setSenderId(int $senderId): static
    {
        $this->senderId = $senderId;

        return $this;
    }

    /**
     * @return int
     */
    public function getMessageLiveTime(): int
    {
        return $this->messageLiveTime;
    }

    /**
     * @param int $messageLiveTime
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setMessageLiveTime(int $messageLiveTime): static
    {
        if ($messageLiveTime < 60 || $messageLiveTime > 86400) {
            throw new InvalidArgumentException(sprintf('Message live time is %s, but must be > 60 and < 86400', $messageLiveTime));
        }

        $this->messageLiveTime = $messageLiveTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getSendDate(): string
    {
        return $this->sendDate;
    }

    /**
     * @param string|\DateTime $sendDate
     *
     * @return $this
     */
    public function setSendDate(string|\DateTime $sendDate): static
    {
        if ($sendDate instanceof \DateTime) {
            $this->sendDate = $sendDate->format('Y-m-d G:i:s');
        } else {
            $this->sendDate = $sendDate;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getRecipients(): array
    {
        return $this->recipients;
    }

    /**
     * @param array $recipients
     *
     * @return $this
     */
    public function setRecipients(array $recipients): static
    {
        $this->recipients = $recipients;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setMessage(string $message): static
    {
        if (strlen($message > 1000)) {
            throw new InvalidArgumentException(sprintf('Message length is %s, but must be < 1000', strlen($message)));
        }

        $this->message = $message;

        return $this;
    }

    /**
     * @return AdditionalSmsNotificationModel
     */
    public function getAdditional(): AdditionalSmsNotificationModel
    {
        return $this->additional;
    }

    /**
     * @param AdditionalSmsNotificationModel $additional
     *
     * @return $this
     */
    public function setAdditional(AdditionalSmsNotificationModel $additional): static
    {
        $this->additional = $additional;

        return $this;
    }
}
