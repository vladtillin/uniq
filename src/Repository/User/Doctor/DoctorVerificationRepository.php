<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Repository\User\Doctor;

use App\Entity\User\Doctor\DoctorVerification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class DoctorVerificationRepository.
 *
 * @method DoctorVerification|null find($id, $lockMode = null, $lockVersion = null)
 * @method DoctorVerification|null findOneBy(array $criteria, array $orderBy = null)
 * @method DoctorVerification[]    findAll()
 * @method DoctorVerification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DoctorVerificationRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DoctorVerification::class);
    }

    /**
     * @param DoctorVerification $entity
     * @param bool               $flush
     */
    public function add(DoctorVerification $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function save(): void
    {
        $this->_em->flush();
    }

    /**
     * @param DoctorVerification $entity
     * @param bool               $flush
     */
    public function remove(DoctorVerification $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
