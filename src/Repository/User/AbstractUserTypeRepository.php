<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Repository\User;

use App\Entity\User\AbstractUserType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class AbstractUserTypeRepository.
 *
 * @author freevan04@gmail.com
 *
 * @method AbstractUserType|null find($id, $lockMode = null, $lockVersion = null)
 * @method AbstractUserType|null findOneBy(array $criteria, array $orderBy = null)
 * @method AbstractUserType[]    findAll()
 * @method AbstractUserType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AbstractUserTypeRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AbstractUserType::class);
    }

    /**
     * @param AbstractUserType $entity
     * @param bool             $flush
     */
    public function add(AbstractUserType $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param AbstractUserType $abstractUserType
     */
    public function save(AbstractUserType $abstractUserType)
    {
        $this->getEntityManager()->flush($abstractUserType);
    }

    /**
     * @param AbstractUserType $entity
     * @param bool             $flush
     */
    public function remove(AbstractUserType $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return AbstractUserType[] Returns an array of AbstractUserType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AbstractUserType
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
