<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Repository\User\Security;

use App\Entity\User\Security\AuthHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class AuthHistoryRepository.
 *
 * @author freevan04@gmail.com
 *
 * @method AuthHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuthHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuthHistory[]    findAll()
 * @method AuthHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthHistoryRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AuthHistory::class);
    }

    /**
     * @param AuthHistory $entity
     * @param bool        $flush
     */
    public function add(AuthHistory $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param AuthHistory $authHistory
     */
    public function save(AuthHistory $authHistory): void
    {
        $this->_em->flush($authHistory);
    }

    /**
     * @param AuthHistory $entity
     * @param bool        $flush
     */
    public function remove(AuthHistory $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
