<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Repository\User\Security;

use App\Entity\User\Security\GoogleAuthenticator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class GoogleAuthenticatorRepository.
 *
 * @method GoogleAuthenticator|null find($id, $lockMode = null, $lockVersion = null)
 * @method GoogleAuthenticator|null findOneBy(array $criteria, array $orderBy = null)
 * @method GoogleAuthenticator[]    findAll()
 * @method GoogleAuthenticator[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GoogleAuthenticatorRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GoogleAuthenticator::class);
    }

    /**
     * @param GoogleAuthenticator $entity
     * @param bool                $flush
     */
    public function add(GoogleAuthenticator $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function save(): void
    {
        $this->_em->flush();
    }

    /**
     * @param GoogleAuthenticator $entity
     * @param bool                $flush
     */
    public function remove(GoogleAuthenticator $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
