<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Repository\User;

use App\Entity\User\ProfileData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ProfileDataRepository.
 *
 * @method ProfileData|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProfileData|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProfileData[]    findAll()
 * @method ProfileData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfileDataRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProfileData::class);
    }

    /**
     * @param ProfileData $entity
     * @param bool        $flush
     */
    public function add(ProfileData $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param ProfileData $profileData
     */
    public function save(ProfileData $profileData): void
    {
        $this->getEntityManager()->flush($profileData);
    }

    /**
     * @param ProfileData $entity
     * @param bool        $flush
     */
    public function remove(ProfileData $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
