<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Repository\Admin;

use App\Entity\Admin\AdminLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class AdminLogRepository.
 *
 * @method AdminLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminLog[]    findAll()
 * @method AdminLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @author freevan04@gmail.com
 */
class AdminLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminLog::class);
    }

    /**
     * @param AdminLog $entity
     * @param bool     $flush
     */
    public function add(AdminLog $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush($entity);
        }
    }

    /**
     * @param AdminLog $adminLog
     */
    public function save(AdminLog $adminLog): void
    {
        $this->getEntityManager()->flush($adminLog);
    }

    /**
     * @param AdminLog $entity
     * @param bool     $flush
     */
    public function remove(AdminLog $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
