<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\App;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AppController.
 * Controller, for controlling all apps routes.
 *
 * @author freevan04@gmail.com
 */
class AppController extends AbstractController
{
    /**
     * Main page of the app.
     *
     * @return Response
     */
    #[Route('/app', name: 'app', methods: ['GET', 'POST'])]
    public function app(): Response
    {
        return $this->render('app/app.html.twig');
    }
}
