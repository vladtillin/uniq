<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\App\Settings;

use App\Entity\User;
use App\Manager\User\Security\GoogleAuthenticatorManager;
use App\Manager\UserManager;
use App\Service\Secure\TwoFactorAuthentication\GoogleAuthenticatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * CLass SecuritySettingsController.
 *
 * @author freevan04@gmail.com
 */
class SecuritySettingsController extends AbstractController
{
    /**
     * Security profile page route.
     *
     * @return Response
     */
    #[Route('/app/profile/security', name: 'app_profile_security', methods: ['GET'])]
    public function securityPage(): Response
    {
        return $this->render('app/settings/security.html.twig');
    }

    /**
     * New password page route.
     *
     * @param Request $request
     *
     * @return Response
     */
    #[Route('/app/profile/security/new-password', name: 'app_profile_security_new_password', methods: ['GET'])]
    public function newPassword(Request $request): Response
    {
        return $this->render('app/settings/new_password.html.twig');
    }

    /**
     * @return Response
     */
    #[Route('/app/profile/security/two-factor', name: 'app_profile_security_two_factor', methods: ['GET'])]
    public function twoFactor(): Response
    {
        return $this->render('app/settings/two_factor/two_factor.html.twig', [
            'isGoogleAuthenticator' => null !== $this->getUser()->getGoogleAuthenticator(),
        ]);
    }

    /**
     * @param GoogleAuthenticatorService $googleAuthenticatorService
     * @param SessionInterface           $session
     *
     * @return Response
     */
    #[Route('/app/profile/security/two-factor/google-authenticator/bind', name: 'app_profile_security_two_factor_google_authenticator_bind', methods: ['GET'])]
    public function bindGoogleAuthenticator(GoogleAuthenticatorService $googleAuthenticatorService, SessionInterface $session): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user->isGoogleAuthenticatorEnabled()) {
            return $this->redirectToRoute('app_profile_security_two_factor');
        }

        $googleAuthenticatorData = $googleAuthenticatorService->generateGoogleAuthenticatorData($user);
        $session->set('google_authenticator_secret_key', $googleAuthenticatorData->secretKey);

        return $this->render('app/settings/two_factor/google_authenticator/bind_google_authenticator.html.twig', [
            'googleAuthenticatorQRCodeImageUrl' => $googleAuthenticatorData->QRCodeUrl,
        ]);
    }

    /**
     * @param Request                    $request
     * @param SessionInterface           $session
     * @param GoogleAuthenticatorService $googleAuthenticatorService
     * @param GoogleAuthenticatorManager $googleAuthenticatorManager
     *
     * @return RedirectResponse
     */
    #[Route('/app/profile/security/two-factor/google-authenticator/bind', name: 'app_profile_security_two_factor_google_authenticator_bind_post', methods: ['POST'])]
    public function bindGoogleAuthenticatorPost(Request $request, SessionInterface $session, GoogleAuthenticatorService $googleAuthenticatorService, GoogleAuthenticatorManager $googleAuthenticatorManager): RedirectResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $secretCode = null;

        if ($session->has('google_authenticator_secret_key')) {
            $secretCode = $session->get('google_authenticator_secret_key');
        }

        $code = $request->request->get('googleAuthenticatorCode', null);

        if (null === $secretCode || null === $code) {
            $this->createAccessDeniedException();
        }

        if ($googleAuthenticatorService->checkCode($secretCode, $code)) {
            $googleAuthenticator = new User\Security\GoogleAuthenticator($user, $secretCode);
            $googleAuthenticatorManager->create($googleAuthenticator);

            return $this->redirectToRoute('app_profile_security_two_factor');
        }

        return $this->redirectToRoute('app_profile_security_two_factor_google_authenticator_bind');
    }

    /**
     * @param UserManager                $userManager
     * @param GoogleAuthenticatorService $googleAuthenticatorService
     *
     * @return Response
     */
    #[Route('/app/profile/security/two-factor/google-authenticator/unbind', name: 'app_profile_security_two_factor_google_authenticator_unbind', methods: ['GET'])]
    public function unbindGoogleAuthenticator(UserManager $userManager, GoogleAuthenticatorService $googleAuthenticatorService): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->isGoogleAuthenticatorEnabled()) {
            return $this->redirectToRoute('app_profile_security_two_factor');
        }

        return $this->render('app/settings/two_factor/google_authenticator/unbind_google_authenticator.html.twig');
    }

    #[Route('/app/profile/security/two-factor/google-authenticator/unbind', name: 'app_profile_security_two_factor_google_authenticator_unbind_post', methods: ['POST'])]
    public function unbindGoogleAuthenticatorPost(GoogleAuthenticatorManager $googleAuthenticatorManager, GoogleAuthenticatorService $googleAuthenticatorService, Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->isGoogleAuthenticatorEnabled()) {
            return $this->redirectToRoute('app_profile_security_two_factor');
        }

        $googleAuthenticatorCodeValidation = $googleAuthenticatorService->checkCode($user->getGoogleAuthenticatorSecret(), $request->request->get('googleAuthenticatorCode', null));

        if ($googleAuthenticatorCodeValidation) {
            $googleAuthenticatorManager->remove($user->getGoogleAuthenticator());

            return $this->redirectToRoute('app_profile_security_two_factor');
        }

        return $this->redirectToRoute('app_profile_security_two_factor_google_authenticator_unbind');
    }
}
