<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\App\Settings;

use App\DTO\Form\Type\App\Settings\ProfileDataEditTypeDTO;
use App\DTOBuilder\Form\Type\App\Settings\ProfileDataEditTypeDTOBuilder;
use App\Entity\User;
use App\Enum\Minio\MinioBucketsEnum;
use App\Exception\InvalidArgumentException;
use App\Form\Type\App\Settings\ProfileDataEditType;
use App\Manager\User\ProfileDataManager;
use App\Service\Minio\MinioService;
use App\Service\Upload\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SettingsController.
 *
 * @author freevan04@gmail.com
 */
class SettingsController extends AbstractController
{
    /**
     * User settings page in cabinet.
     *
     * @return Response
     */
    #[Route('/app/settings', name: 'app_settings', methods: ['GET'])]
    public function settings(): Response
    {
        return $this->render('app/settings/settings.html.twig');
    }

    /**
     * User change language page in cabinet.
     *
     * @return Response
     */
    #[Route('/app/language', name: 'app_language', methods: ['GET'])]
    public function language(): Response
    {
        return $this->render('app/settings/language.html.twig');
    }

    /**
     * Edit user data. If that fields already set by user, he can't change them again (birthDate, name, surname, patronymic, country, city).
     * The avatar user can change at every time.
     *
     * @param Request            $request
     * @param MinioService       $minioService
     * @param ProfileDataManager $profileDataManager
     * @param FileUploader       $fileUploader
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     */
    #[Route('/app/profile/edit', name: 'app_profile_data_edit', methods: ['GET', 'POST'])]
    public function profileDataEdit(Request $request, MinioService $minioService, ProfileDataManager $profileDataManager, FileUploader $fileUploader): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $profileDataEditForm = $this->createForm(ProfileDataEditType::class);
        $profileDataEditForm = ProfileDataEditTypeDTOBuilder::setFormData($profileDataEditForm, $user->getProfileData());

        $profileDataEditForm->handleRequest($request);

        if ($profileDataEditForm->isSubmitted() && $profileDataEditForm->isValid()) {
            /** @var ProfileDataEditTypeDTO $profileDataEditTypeDTO */
            $profileDataEditTypeDTO = $profileDataEditForm->getData();

            $fileDTO = $fileUploader->uploadUserAvatar($profileDataEditTypeDTO->avatar);
            $fileDTO->minioFilename = $fileUploader->generateFilenameForUser($user);
            $fileDTO->minioBucketName = MinioBucketsEnum::USER_AVATAR;

            $minioService->putFile($fileDTO);

            $profileData = ProfileDataEditTypeDTOBuilder::buildFromDTO($profileDataEditTypeDTO, $user->getProfileData(), $fileDTO->minioFilename);

            $profileDataManager->update($profileData);

            return $this->redirectToRoute('app_profile_data_edit');
        }

        return $this->render('app/settings/profile_data_edit.html.twig', [
            'editForm' => $profileDataEditForm->createView(),
        ]);
    }

    /**
     * Notification settings route for user.
     *
     * @return Response
     */
    #[Route('/app/profile/notification-settings', name: 'app_profile_notifications_settings', methods: ['GET'])]
    public function notificationsSettings(): Response
    {
        return $this->render('app/settings/notifications_settings.html.twig');
    }
}
