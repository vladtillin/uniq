<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\App\Verification;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VerificationController.
 *
 * @author freevan04@gmail.com
 */
class VerificationController extends AbstractController
{
    /**
     * @return Response
     */
    #[Route('/app/profile/verification', name: 'app_profile_verification', methods: ['GET'])]
    public function verification(): Response
    {
        return $this->render('app/settings/verification/verification_menu.html.twig');
    }

    /**
     * @return RedirectResponse|Response
     */
    #[Route('/app/profile/verification/email', name: 'app_profile_verification_email', methods: ['GET'])]
    public function emailVerification(): RedirectResponse|Response
    {
        return $this->render('app/settings/verification/email_verification.html.twig', [
            'userHasEmail' => null !== $this->getUser()->getEmail(),
        ]);
    }

    #[Route('/app/profile/verification/phone-number', name: 'app_profile_verification_phone_number')]
    public function phoneNumberVerification(): RedirectResponse|Response
    {
        return $this->render('app/settings/verification/phone_number_verification.html.twig', [
            'userHasPhoneNumber' => null !== $this->getUser()->getPhoneNumber(),
        ]);
    }
}
