<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PageController.
 * Controller for landing pages, like main page, contact, policy etc.
 *
 * @author freevan04@gmail.com
 */
class PageController extends AbstractController
{
    /**
     * Main page.
     *
     * @return Response
     */
    #[Route('/', name: 'main_page', methods: ['GET', 'POST'])]
    public function mainPage(): Response
    {
        return $this->render('landing_pages/landing_main_page.html.twig');
    }


}
