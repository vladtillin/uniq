<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\Api\User;

use App\Entity\User;
use App\Exception\InvalidArgumentException;
use App\Manager\User\ProfileDataManager;
use App\Manager\UserManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserApiController.
 *
 * @author freevan04@gmail.com
 */
class UserApiController extends AbstractFOSRestController
{
    /**
     * Api route, that changes user's language.
     *
     * @param Request            $request            user request
     * @param ProfileDataManager $profileDataManager profileDataManager, for update data
     *
     * @return JsonResponse empty response, when all is OK
     *
     * @throws InvalidArgumentException
     */
    #[Rest\Post('/api/v1/user/secure/changeLanguage', name: 'api_v1_user_secure_change_language')]
    public function changeLanguage(Request $request, ProfileDataManager $profileDataManager): JsonResponse
    {
        $profileData = $profileDataManager->getProfileDataForUser($this->getUser());
        $profileData->setLanguage($request->request->get('language', 'en'));

        $profileDataManager->update($profileData);

        return new JsonResponse();
    }

    /**
     * @param Request     $request
     * @param UserManager $userManager
     *
     * @return JsonResponse
     *
     * @throws InvalidArgumentException
     */
    #[Rest\Post('/api/v1/user/secure/setEmail', name: 'api_v1_user_secure_set_email')]
    public function setEmail(Request $request, UserManager $userManager): JsonResponse
    {
        $email = $request->request->get('email', '');

        if (empty(trim($email))) {
            throw new InvalidArgumentException('Empty email on userSetEmail API');
        }

        if (null !== $this->getUser()->getEmail()) {
            return new JsonResponse();
        }

        /** @var User $user */
        $user = $this->getUser();

        $user->setEmail($email);
        $userManager->update($user);

        return new JsonResponse();
    }

    /**
     * @param Request     $request
     * @param UserManager $userManager
     *
     * @return JsonResponse
     *
     * @throws InvalidArgumentException
     */
    #[Rest\Post('/api/v1/user/secure/setPhoneNumber', name: 'api_v1_user_secure_set_phone_number')]
    public function setPhoneNumber(Request $request, UserManager $userManager): JsonResponse
    {
        $phoneNumber = $request->request->get('phoneNumber', '');
        $phoneNumberCountry = $request->request->get('country', '');

        if (empty(trim($phoneNumber)) || empty(trim($phoneNumberCountry))) {
            throw new InvalidArgumentException('Empty phone number on userSetPhoneNumber API');
        }

        if (null !== $this->getUser()->getPhoneNumber()) {
            return new JsonResponse();
        }

        /** @var User $user */
        $user = $this->getUser();

        $user->setPhoneNumber($phoneNumber);
        $user->setPhoneNumberCountry($phoneNumberCountry);
        $userManager->update($user);

        return new JsonResponse();
    }

    /**
     * @param Request     $request
     * @param UserManager $userManager
     *
     * @return JsonResponse
     *
     * @throws InvalidArgumentException
     */
    #[Rest\Post('/api/v1/user/secure/checkPhoneNumberUnique', name: 'api_v1_user_secure_check_phone_number_unique')]
    public function checkPhoneNumberUnique(Request $request, UserManager $userManager): JsonResponse
    {
        $phoneNumber = $request->request->get('phoneNumber', null);

        $user = $userManager->findUserByPhoneNumber($phoneNumber);

        if (null === $user) {
            return new JsonResponse();
        } else {
            throw new InvalidArgumentException();
        }
    }

    /**
     * @param Request     $request
     * @param UserManager $userManager
     *
     * @return JsonResponse
     *
     * @throws InvalidArgumentException
     */
    #[Rest\Post('/api/v1/user/secure/checkEmailUnique', name: 'api_v1_user_secure_check_email_unique')]
    public function checkEmailUnique(Request $request, UserManager $userManager): JsonResponse
    {
        $email = $request->request->get('email', null);

        $user = $userManager->findUserByEmail($email);

        if (null === $user) {
            return new JsonResponse();
        } else {
            throw new InvalidArgumentException();
        }
    }
}
