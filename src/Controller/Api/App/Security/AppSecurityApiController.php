<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\Api\App\Security;

use App\Entity\User;
use App\Exception\InvalidArgumentException;
use App\Manager\UserManager;
use App\Service\Secure\TwoFactorAuthentication\GoogleAuthenticatorService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Class AppSecurityApiController.
 *
 * Api controller for user security.
 *
 * @author freevan04@gmail.com
 */
class AppSecurityApiController extends AbstractFOSRestController
{
    /**
     * Check if the oldPassword === password in database.
     *
     * @param Request                     $request
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param UserManager                 $userManager
     *
     * @return JsonResponse
     *
     * @throws InvalidArgumentException
     */
    #[Rest\Post('/api/v1/app/security/checkPassword', name: 'api_v1_app_security_check_password')]
    public function checkPassword(Request $request, UserPasswordHasherInterface $userPasswordHasher, UserManager $userManager): JsonResponse
    {
        $user = $this->getUser();

        if (null === $user) {
            $this->createAccessDeniedException();
        }

        $user = $userManager->findUserById($user->getId());

        $oldPassword = $request->request->get('oldPassword', '');

        if (!$userPasswordHasher->isPasswordValid($user, $oldPassword)) {
            throw new InvalidArgumentException('Invalid password');
        }

        return new JsonResponse();
    }

    /**
     * Change password for user, if password is valid.
     *
     * @param Request                     $request
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param UserManager                 $userManager
     *
     * @return JsonResponse
     *
     * @throws InvalidArgumentException
     */
    #[Rest\Post('/api/v1/app/security/changePassword', name: 'api_v1_app_security_change_password')]
    public function changePassword(Request $request, UserPasswordHasherInterface $userPasswordHasher, UserManager $userManager): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if (null === $user) {
            $this->createAccessDeniedException();
        }

        $user = $userManager->findUserById($user->getId());

        $password = $request->request->get('newPassword', null);

        if (null === $password) {
            throw new InvalidArgumentException('Password is empty');
        }

        if (strlen($password) < 6) {
            throw new InvalidArgumentException('Password is too short');
        }

        $userManager->changeUserPassword($user, $password);

        return new JsonResponse();
    }

    /**
     * @param Request                    $request
     * @param GoogleAuthenticatorService $googleAuthenticatorService
     *
     * @return JsonResponse
     *
     * @throws InvalidArgumentException
     */
    #[Rest\Post('/api/v1/app/security/googleAuthenticator/checkCode', name: 'api_v1_app_security_google_authenticator_check_code')]
    public function checkGoogleAuthenticatorCode(Request $request, GoogleAuthenticatorService $googleAuthenticatorService, SessionInterface $session): JsonResponse
    {
        $secretCode = $request->request->get('secretCode', null);
        $code = $request->request->get('code', null);

        if ($session->has('google_authenticator_secret_key') && null === $secretCode) {
            $secretCode = $session->get('google_authenticator_secret_key');
        }

        if (null === $secretCode) {
            $secretCode = $this->getUser()->getGoogleAuthenticator()?->getSecretCode();
        }

        if (null === $secretCode || null === $code) {
            $this->createAccessDeniedException();
        }

        $googleAuthenticatorCodeValidation = $googleAuthenticatorService->checkCode($secretCode, $code);

        if ($googleAuthenticatorCodeValidation) {
            return new JsonResponse();
        }

        throw new InvalidArgumentException('Invalid google authenticator code');
    }
}
