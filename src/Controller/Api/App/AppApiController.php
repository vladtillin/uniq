<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\Api\App;

use App\DTO\Controller\Api\App\NotificationsSettingsApiDTO;
use App\DTOBuilder\Controller\Api\App\NotificationsSettingsApiDTOBuilder;
use App\Entity\User;
use App\Manager\User\Notification\NotificationManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AppApiController.
 *
 * Api controller for app.
 *
 * @author freevan04@gmail.com
 */
class AppApiController extends AbstractFOSRestController
{
    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(private SerializerInterface $serializer)
    {
    }

    /**
     * Api route to get user notifications settings.
     *
     * @return JsonResponse
     */
    #[Rest\Get('/api/v1/app/notifications/settings', name: 'api_v1_app_notifications_settings_get')]
    public function getNotificationsSettings(): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if (null === $user) {
            $this->createAccessDeniedException();
        }

        return new JsonResponse($this->serializer->serialize(NotificationsSettingsApiDTOBuilder::build($user->getNotification()), 'json'));
    }

    /**
     * Api route to change user notifications settings.
     *
     * @param Request             $request
     * @param NotificationManager $notificationManager
     *
     * @return JsonResponse
     */
    #[Rest\Post('/api/v1/app/notificationsSettings', name: 'api_v1_app_notifications_settings_post')]
    public function changeNotificationsSettings(Request $request, NotificationManager $notificationManager): JsonResponse
    {
        $notificationsSettingsApiDTO = $this->serializer->deserialize($request->getContent(), NotificationsSettingsApiDTO::class, 'json');

        /** @var User $user */
        $user = $this->getUser();

        if (null === $user) {
            $this->createAccessDeniedException();
        }

        $userNotification = $user->getNotification();

        $userNotification = NotificationsSettingsApiDTOBuilder::modifyNotificationEntity($userNotification, $notificationsSettingsApiDTO);

        $notificationManager->update($userNotification);

        return new JsonResponse();
    }
}
