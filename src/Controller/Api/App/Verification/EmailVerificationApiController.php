<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\Api\App\Verification;

use App\Entity\User;
use App\Exception\InvalidArgumentException;
use App\Manager\UserManager;
use App\Service\Notification\Email\EmailConfigurator;
use App\Service\Notification\Email\EmailNotificationService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class EmailVerificationApiController.
 *
 * @author freevan04@gmail.com
 */
class EmailVerificationApiController extends AbstractFOSRestController
{
    /**
     * This method send verify email message, with verification code.
     *
     * @param Request                  $request
     * @param EmailNotificationService $emailNotificationService
     * @param EmailConfigurator        $emailConfigurator
     * @param SessionInterface         $session
     *
     * @return JsonResponse
     *
     * @throws TransportExceptionInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    #[Rest\Post('/api/v1/user/secure/email/sendVerifyEmail', name: 'api_v1_user_secure_email_send_verify_email')]
    public function sendVerifyEmailEmail(Request $request, EmailNotificationService $emailNotificationService, EmailConfigurator $emailConfigurator, SessionInterface $session): JsonResponse
    {
        $emailMessageDTO = $emailConfigurator->emailForVerification($this->getUser()->getEmail());
        $emailNotificationService->sendEmail($emailMessageDTO);
        dump($emailMessageDTO);
        $session->set('email_verification_code', $emailMessageDTO->context['verificationCode']);

        return new JsonResponse();
    }

    /**
     * This method check email verification method.
     *
     * @param Request          $request
     * @param UserManager      $userManager
     * @param SessionInterface $session
     *
     * @return JsonResponse
     *
     * @throws InvalidArgumentException
     */
    #[Rest\Post('/api/v1/user/secure/email/checkVerifyEmailCode', name: 'api_v1_user_secure_email_check_verify_email_code')]
    public function checkVerifyEmailCode(Request $request, UserManager $userManager, SessionInterface $session): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if (trim($request->request->get('code', 12)) === trim($session->get('email_verification_code', ''))) {
            $userManager->verifyEmail($user);
        } else {
            throw new InvalidArgumentException(sprintf('Invalid code'));
        }

        return new JsonResponse();
    }
}
