<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\Admin\User;

use App\Entity\Admin\AdminLog;
use App\Entity\User\ProfileData;
use App\Manager\Admin\AdminLogManager;
use App\Manager\User\ProfileDataManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\LanguageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

/**
 * Class ProfileDataCrudController.
 *
 * @author freevan04@gmail.com
 */
class ProfileDataCrudController extends AbstractCrudController
{
    private ProfileDataManager $profileDataManager;

    private AdminLogManager $adminLogManager;

    /**
     * @param ProfileDataManager $profileDataManager
     * @param AdminLogManager    $adminLogManager
     */
    public function __construct(ProfileDataManager $profileDataManager, AdminLogManager $adminLogManager)
    {
        $this->profileDataManager = $profileDataManager;
        $this->adminLogManager = $adminLogManager;
    }

    /**
     * @return string
     */
    public static function getEntityFqcn(): string
    {
        return ProfileData::class;
    }

    /**
     * @param Crud $crud
     *
     * @return Crud
     */
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Данные профиля пользователей')
            ->setPageTitle('detail', 'Данный профиля пользователя')
            ->setPageTitle('edit', 'Редактирование данных профиля пользователя');
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param ProfileData            $entityInstance
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $logText = sprintf('Обновил данные профиля id = %s, пользователя id = %s', $entityInstance->getId(), $entityInstance->getUser()->getId());

        $adminLog = new AdminLog($this->getUser(), $logText);
        $this->adminLogManager->create($adminLog);

        $this->profileDataManager->update($entityInstance);
    }

    /**
     * @param Filters $filters
     *
     * @return Filters
     */
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('birthDate')
            ->add('country')
            ->add('city')
            ->add('language')
            ->add('createdAt')
            ->add('updatedAt');
    }

    /**
     * @param string $pageName
     *
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')->hideOnForm(),
            AssociationField::new('user')->hideOnForm()->setRequired(true),
            TextField::new('name', 'Имя')->setRequired(true),
            TextField::new('surname', 'Фамилия')->setRequired(true),
            TextField::new('patronymic', 'Отчество')->hideOnIndex(),
            DateTimeField::new('birthDate', 'Дата рождения'),
            TextField::new('avatar', 'Аватар')->hideOnIndex(),
            TextField::new('country', 'Страна'),
            TextField::new('city', 'Город'),
            LanguageField::new('language', 'Язык'),
            DateTimeField::new('createdAt', 'Дата создания'),
            DateTimeField::new('updatedAt', 'Дата обновления'),
        ];
    }

    /**
     * @param Actions $actions
     *
     * @return Actions
     */
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE);
    }
}
