<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\Admin\User\Notification;

use App\Entity\Admin\AdminLog;
use App\Entity\User\Notification\Notification;
use App\Manager\Admin\AdminLogManager;
use App\Manager\User\Notification\NotificationManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;

/**
 * Class NotificationCrudController.
 *
 * @author freevan04@gmail.com
 */
class NotificationCrudController extends AbstractCrudController
{
    private NotificationManager $notificationManager;

    private AdminLogManager $adminLogManager;

    /**
     * @param NotificationManager $notificationManager
     * @param AdminLogManager     $adminLogManager
     */
    public function __construct(NotificationManager $notificationManager, AdminLogManager $adminLogManager)
    {
        $this->notificationManager = $notificationManager;
        $this->adminLogManager = $adminLogManager;
    }

    /**
     * @return string
     */
    public static function getEntityFqcn(): string
    {
        return Notification::class;
    }

    /**
     * @param Crud $crud
     *
     * @return Crud
     */
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Уведомления пользователей')
            ->setPageTitle('detail', 'Уведомления пользователя')
            ->setPageTitle('edit', 'Редактирование уведомлений пользователя');
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param Notification           $entityInstance
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $logText = sprintf('Обновил настройки уведомлений с id = %s, для пользователя с id = %s', $entityInstance->getId(), $entityInstance->getUser()->getId());

        $adminLog = new AdminLog($this->getUser(), $logText);
        $this->adminLogManager->create($adminLog);

        $this->notificationManager->update($entityInstance);
    }

    /**
     * @param Filters $filters
     *
     * @return Filters
     */
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('user')
            ->add('messagesNotifications')
            ->add('updatesNotifications')
            ->add('advertisingNotifications')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('deleted');
    }

    /**
     * @param string $pageName
     *
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')->setDisabled(),
            AssociationField::new('user', 'Пользователь')->setPermission('ROLE_OWNER'),
            BooleanField::new('messagesNotifications', 'Уведомления о сообщениях')->setPermission('ROLE_OWNER'),
            BooleanField::new('updatesNotifications', 'Уведомления об обновлениях')->setPermission('ROLE_OWNER'),
            BooleanField::new('advertisingNotifications', 'Рекламные уведомления')->setPermission('ROLE_OWNER'),
            DateTimeField::new('createdAt', 'Дата создания')->setDisabled(true),
            DateTimeField::new('updatedAt', 'Дата обновления')->setDisabled(true),
            BooleanField::new('deleted', 'Удалено')->setPermission('ROLE_OWNER'),
        ];
    }

    /**
     * @param Actions $actions
     *
     * @return Actions
     */
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE);
    }
}
