<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\Admin\User;

use App\Entity\Admin\AdminLog;
use App\Entity\User;
use App\Manager\Admin\AdminLogManager;
use App\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

/**
 * Class UserCrudController.
 *
 * Easyadmin controller for User entity.
 *
 * @author freevan04@gmail.com
 */
class UserCrudController extends AbstractCrudController
{
    private AdminLogManager $adminLogManager;

    private UserManager $userManager;

    /**
     * @param AdminLogManager $adminLogManager
     * @param UserManager     $userManager
     */
    public function __construct(AdminLogManager $adminLogManager, UserManager $userManager)
    {
        $this->adminLogManager = $adminLogManager;
        $this->userManager = $userManager;
    }

    /**
     * @return string
     */
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    /**
     * @param Crud $crud
     *
     * @return Crud
     */
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Пользователи')
            ->setPageTitle('detail', fn (User $user) => (string) $user)
            ->setPageTitle('edit', fn (User $user) => (string) $user);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param $entityInstance
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $logText = sprintf('Обновил данные пользователя с id %s', $entityInstance->getId());
        $adminLog = new AdminLog($this->getUser(), $logText);
        $this->adminLogManager->create($adminLog);
        $this->userManager->update($entityInstance);
    }

    /**
     * @param Filters $filters
     *
     * @return Filters
     */
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('email')
            ->add('phoneNumber')
            ->add('roles')
            ->add('nickname')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('deleted');
    }

    /**
     * @param string $pageName
     *
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')->setDisabled(),
            EmailField::new('email', 'Email')->setDisabled(),
            TextField::new('phoneNumber', 'Номер телефона')->setDisabled(),
            ArrayField::new('roles', 'Роли')->setDisabled(),
            ArrayField::new('roles', 'Роли')->setPermission('ROLE_OWNER')->onlyOnForms(),
            TextField::new('nickname', 'Никнейм')->setPermission('ROLE_OWNER')->setRequired(true),
            AssociationField::new('profileData', 'Данные профиля')->hideOnIndex(),
            CollectionField::new('authHistory', 'История авторизаций')->setPermission('ROLE_OWNER')->hideOnIndex()->hideOnForm(),
            AssociationField::new('googleAuthenticator', 'Гугл аутентификация')->hideOnForm()->setPermission('ROLE_OWNER')->hideOnIndex(),
            AssociationField::new('notification', 'Настройки уведомлений')->hideOnForm(),
            DateTimeField::new('createdAt', 'Дата создания')->hideOnForm(),
            DateTimeField::new('updatedAt', 'Дата обновления')->setPermission('ROLE_OWNER'),
            BooleanField::new('deleted', 'Удалено')->setPermission('ROLE_ADMIN')->setPermission('ROLE_OWNER')->hideOnIndex(),
        ];
    }

    /**
     * @param Actions $actions
     *
     * @return Actions
     */
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE);
    }
}
