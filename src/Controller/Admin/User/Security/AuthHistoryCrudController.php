<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\Admin\User\Security;

use App\Entity\Admin\AdminLog;
use App\Entity\User\Security\AuthHistory;
use App\Manager\Admin\AdminLogManager;
use App\Manager\User\Security\AuthHistoryManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

/**
 * Class AuthHistoryCrudController.
 *
 * @author freevan04@gmail.com
 */
class AuthHistoryCrudController extends AbstractCrudController
{
    private AuthHistoryManager $authHistoryManager;

    private AdminLogManager $adminLogManager;

    /**
     * @param AuthHistoryManager $authHistoryManager
     * @param AdminLogManager    $adminLogManager
     */
    public function __construct(AuthHistoryManager $authHistoryManager, AdminLogManager $adminLogManager)
    {
        $this->authHistoryManager = $authHistoryManager;
        $this->adminLogManager = $adminLogManager;
    }

    /**
     * @return string
     */
    public static function getEntityFqcn(): string
    {
        return AuthHistory::class;
    }

    /**
     * @param Crud $crud
     *
     * @return Crud
     */
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'История авторизаций пользователей')
            ->setPageTitle('detail', 'Авторизация пользователя')
            ->setPageTitle('edit', 'Редактирование авторизации пользователя');
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param AuthHistory            $entityInstance
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $logText = sprintf('Обновил историю авторизаций id = %s, для пользователя id = %s', $entityInstance->getId(), $entityInstance->getUser()->getId());

        $adminLog = new AdminLog($this->getUser(), $logText);
        $this->adminLogManager->update($adminLog);

        $this->authHistoryManager->update($entityInstance);
    }

    /**
     * @param Filters $filters
     *
     * @return Filters
     */
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('user')
            ->add('token')
            ->add('identifier')
            ->add('createdAt')
            ->add('deleted');
    }

    /**
     * @param string $pageName
     *
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')->setDisabled(true),
            AssociationField::new('user', 'Пользователь')->setDisabled(true),
            TextField::new('token', 'Токен')->setDisabled(true),
            TextField::new('identifier', 'Идентификатор')->setDisabled(true),
            DateTimeField::new('createdAt', 'Дата создания')->setDisabled(true),
            DateTimeField::new('updatedAt', 'Дата обновления')->setDisabled(true),
            BooleanField::new('deleted', 'Удалено')->setPermission('ROLE_OWNER'),
        ];
    }

    /**
     * @param Actions $actions
     *
     * @return Actions
     */
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE, Action::EDIT, Action::NEW);
    }
}
