<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\Admin\User\Security;

use App\Entity\Admin\AdminLog;
use App\Entity\User\Security\GoogleAuthenticator;
use App\Manager\Admin\AdminLogManager;
use App\Manager\User\Security\GoogleAuthenticatorManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

/**
 * Class GoogleAuthenticatorCrudController.
 *
 * @author freevan04@gmail.com
 */
class GoogleAuthenticatorCrudController extends AbstractCrudController
{
    private GoogleAuthenticatorManager $googleAuthenticatorManager;

    private AdminLogManager $adminLogManager;

    /**
     * @param GoogleAuthenticatorManager $googleAuthenticatorManager
     * @param AdminLogManager            $adminLogManager
     */
    public function __construct(GoogleAuthenticatorManager $googleAuthenticatorManager, AdminLogManager $adminLogManager)
    {
        $this->googleAuthenticatorManager = $googleAuthenticatorManager;
        $this->adminLogManager = $adminLogManager;
    }

    /**
     * @return string
     */
    public static function getEntityFqcn(): string
    {
        return GoogleAuthenticator::class;
    }

    /**
     * @param Crud $crud
     *
     * @return Crud
     */
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Гугл аутентификация пользователей')
            ->setPageTitle('detail', 'Гугл аутентификация пользователя')
            ->setPageTitle('edit', 'Редактирование гугл аутентификации пользователя');
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param GoogleAuthenticator    $entityInstance
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $logText = sprintf('Обновил данные гугл аутентификации id = %s, пользователя id = %s', $entityInstance->getId(), $entityInstance->getUser()->getId());

        $adminLog = new AdminLog($this->getUser(), $logText);
        $this->adminLogManager->update($adminLog);

        $this->googleAuthenticatorManager->update($entityInstance);
    }

    /**
     * @param Filters $filters
     *
     * @return Filters
     */
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('user')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('deleted');
    }

    /**
     * @param string $pageName
     *
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID'),
            AssociationField::new('user', 'Пользователь'),
            TextField::new('secretCode', 'Секретный код')->setDisabled(true)->setPermission('ROLE_OWNER'),
            DateTimeField::new('createdAt', 'Дата создания'),
            DateTimeField::new('updatedAt', 'Дата обновления'),
            BooleanField::new('deleted', 'Удалено')->setPermission('ROLE_OWNER'),
        ];
    }

    /**
     * @param Actions $actions
     *
     * @return Actions
     */
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE);
    }
}
