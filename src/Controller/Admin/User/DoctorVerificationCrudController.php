<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\Admin\User;

use App\Entity\Admin\AdminLog;
use App\Entity\User\Doctor\DoctorVerification;
use App\Manager\Admin\AdminLogManager;
use App\Manager\User\Doctor\DoctorVerificationManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

/**
 * Class DoctorVerificationCrudController.
 *
 * @author freevan04@gmail.com
 */
class DoctorVerificationCrudController extends AbstractCrudController
{
    private DoctorVerificationManager $doctorVerificationManager;

    private AdminLogManager $adminLogManager;

    /**
     * @param DoctorVerificationManager $doctorVerificationManager
     * @param AdminLogManager           $adminLogManager
     */
    public function __construct(DoctorVerificationManager $doctorVerificationManager, AdminLogManager $adminLogManager)
    {
        $this->doctorVerificationManager = $doctorVerificationManager;
        $this->adminLogManager = $adminLogManager;
    }

    /**
     * @return string
     */
    public static function getEntityFqcn(): string
    {
        return DoctorVerification::class;
    }

    /**
     * @param Crud $crud
     *
     * @return Crud
     */
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Верификации докторов')
            ->setPageTitle('detail', 'Верификация доктора')
            ->setPageTitle('edit', 'Редактирование верификации доктора');
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param DoctorVerification     $entityInstance
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $logText = sprintf('Изменил верификацию id = %s, для пользователя id = %s', $entityInstance->getId(), $entityInstance->getDoctorUserType()->getUser()->getId());

        $adminLog = new AdminLog($this->getUser(), $logText);
        $this->adminLogManager->create($adminLog);

        $this->doctorVerificationManager->update($entityInstance);
    }

    /**
     * @param Filters $filters
     *
     * @return Filters
     */
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('name')
            ->add('surname')
            ->add('patronymic')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('deleted');
    }

    /**
     * @param string $pageName
     *
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            AssociationField::new('doctorUserType', 'Доктор'),
            TextField::new('name', 'Имя'),
            TextField::new('surname', 'Фамилия'),
            TextField::new('patronymic', 'Отчество'),
            TextField::new('selfieFileId', 'Селфи'), // TODO: Create image
            TextField::new('documentFrontSideFileId', 'Фото документа 1'),
            TextField::new('documentBackSideFileId', 'Фото документа 2'),
            DateTimeField::new('createdAt', 'Дата создания'),
            DateTimeField::new('updatedAt', 'Дата обновления'),
            BooleanField::new('deleted', 'Удалено'),
        ];
    }

    /**
     * @param Actions $actions
     *
     * @return Actions
     */
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE);
    }
}
