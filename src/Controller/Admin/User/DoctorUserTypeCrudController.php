<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\Admin\User;

use App\Entity\Admin\AdminLog;
use App\Entity\User\DoctorUserType;
use App\Manager\Admin\AdminLogManager;
use App\Manager\User\AbstractUserTypeManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;

/**
 * Class DoctorUserTypeCrudController.
 *
 * @author freevan04@gmail.com
 */
class DoctorUserTypeCrudController extends AbstractCrudController
{
    private AbstractUserTypeManager $abstractUserTypeManager;

    private AdminLogManager $adminLogManager;

    /**
     * @param AbstractUserTypeManager $abstractUserTypeManager
     * @param AdminLogManager         $adminLogManager
     */
    public function __construct(AbstractUserTypeManager $abstractUserTypeManager, AdminLogManager $adminLogManager)
    {
        $this->abstractUserTypeManager = $abstractUserTypeManager;
        $this->adminLogManager = $adminLogManager;
    }

    /**
     * @return string
     */
    public static function getEntityFqcn(): string
    {
        return DoctorUserType::class;
    }

    /**
     * @param Crud $crud
     *
     * @return Crud
     */
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Доктора')
            ->setPageTitle('detail', 'Детали доктора')
            ->setPageTitle('edit', 'Редактирование доктора');
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param DoctorUserType         $entityInstance
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $logText = sprintf('Обновил доктора id = %s, для пользователя id = %s', $entityInstance->getId(), $entityInstance->getUser()->getId());

        $adminLog = new AdminLog($this->getUser(), $logText);
        $this->adminLogManager->create($adminLog);

        $this->abstractUserTypeManager->update($entityInstance);
    }

    /**
     * @param Filters $filters
     *
     * @return Filters
     */
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('verified')
            ->add('patients');
    }

    /**
     * @param string $pageName
     *
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            BooleanField::new('verified', 'Пройдена ли верификация'),
            AssociationField::new('verification', 'Верификация')->setPermission('ROLE_OWNER'),
            CollectionField::new('patients', 'Пациенты')->setDisabled(true),
        ];
    }

    /**
     * @param Actions $actions
     *
     * @return Actions
     */
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE);
    }
}
