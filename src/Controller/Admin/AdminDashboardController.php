<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminDashboardController.
 *
 * Admin dashboard.
 *
 * @author freevan04@gmail.com
 */
class AdminDashboardController extends AbstractDashboardController
{
    /**
     * Index function, returns Response with rendered admin (easyadmin) dashboard.
     *
     * @return Response
     */
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/dashboard/admin_dashboard.html.twig');
    }

    /**
     * The function, that configures dashboard.
     *
     * @return Dashboard
     */
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<img src="img/logo/app_logo.png">ADMIN')
            ->setFaviconPath('img/favicon/favicon.ico');
    }

    /**
     * Configures menu of admin.
     *
     * @return iterable
     */
    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Главная', 'fa fa-home');

        yield MenuItem::linkToCrud('Пользователи', 'fa fa-user', User::class);
        yield MenuItem::linkToCrud('Доктора', 'fa fa-user', User\DoctorUserType::class);
        yield MenuItem::linkToCrud('Верификация', 'fa fa-user', User\Doctor\DoctorVerification::class);
        yield MenuItem::linkToCrud('Уведомления', 'fa fa-user', User\Notification\Notification::class)->setPermission('ROLE_OWNER');
        yield MenuItem::linkToCrud('Пациенты', 'fa fa-user', User\PatientUserType::class);
        yield MenuItem::linkToCrud('Данные профиля', 'fa fa-user', User\ProfileData::class)->setPermission('ROLE_OWNER');
        yield MenuItem::linkToCrud('История авторизация', 'fa fa-user', User\Security\AuthHistory::class)->setPermission('ROLE_OWNER');
        yield MenuItem::linkToCrud('Гугл аутентификация', 'fa fa-user', User\Security\GoogleAuthenticator::class)->setPermission('ROLE_OWNER');
    }
}
