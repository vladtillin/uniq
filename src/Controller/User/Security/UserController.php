<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Controller\User\Security;

use App\DTO\User\Registration\UserEmailRegistrationDTO;
use App\DTO\User\Registration\UserPhoneNumberRegistrationDTO;
use App\Entity\User;
use App\Exception\InvalidArgumentException;
use App\Form\Type\User\Registration\EmailRegistrationType;
use App\Form\Type\User\Registration\PhoneNumberRegistrationType;
use App\Manager\UserManager;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class UserController.
 * It's main controller for user. For some pages for user, out of the app logic. (like registration, auth etc.).
 *
 * @author freevan04@gmail.com
 */
class UserController extends AbstractController
{
    /**
     * Registration page, where user can choose - he is a doctor or a patient.
     * And he will be redirected on registration page (doctor or patient respectively).
     *
     * @return Response
     */
    #[Route('/reg', name: 'user_security_user_registration', methods: ['GET'])]
    public function registration(): Response
    {
        return $this->render('landing_pages/registration.html.twig');
    }

    /**
     * Patient registration. User can register with email or with phoneNumber.
     *
     * @param Request                     $request
     * @param UserManager                 $userManager
     * @param UserPasswordHasherInterface $passwordHasher
     *
     * @return RedirectResponse|Response
     *
     * @throws InvalidArgumentException
     */
    #[Route('/reg/patient', name: 'user_security_user_registration_patient')]
    public function patientRegistration(Request $request, UserManager $userManager, UserPasswordHasherInterface $passwordHasher): RedirectResponse|Response
    {
        $emailRegistrationForm = $this->createForm(EmailRegistrationType::class);
        $phoneNumberRegistrationForm = $this->createForm(PhoneNumberRegistrationType::class);

        $emailRegistrationForm->handleRequest($request);
        $phoneNumberRegistrationForm->handleRequest($request);

        if ($emailRegistrationForm->isSubmitted() && $emailRegistrationForm->isValid()) {
            /** @var UserEmailRegistrationDTO $userDTO */
            $userDTO = $emailRegistrationForm->getData();

            $user = User::createWithEmail($passwordHasher, $userDTO->password, $userDTO->nickname, $userDTO->name, $userDTO->surname, $userDTO->email);
            $user->setAbstractUserType(new User\PatientUserType($user));
            $user->getProfileData()->setLanguage($request->getLocale());

            $userManager->create($user);

            return $this->redirectToRoute('app_login'); // TODO: Create redirect to auth route
        }

        if ($phoneNumberRegistrationForm->isSubmitted() && $phoneNumberRegistrationForm->isValid()) {
            /** @var UserPhoneNumberRegistrationDTO $userDTO */
            $userDTO = $phoneNumberRegistrationForm->getData();

            $user = User::createWithPhoneNumber($passwordHasher, $userDTO->password, $userDTO->nickname, $userDTO->name, $userDTO->surname, $userDTO->phoneNumber);
            $user->setAbstractUserType(new User\PatientUserType($user));
            $user->getProfileData()->setLanguage($request->getLocale());

            $userManager->create($user);

            return $this->redirectToRoute('app_login'); // TODO: Create redirect to auth route
        }

        return $this->render('landing_pages/patient_registration.html.twig', [
            'emailRegistrationForm' => $emailRegistrationForm->createView(),
            'phoneNumberRegistrationForm' => $phoneNumberRegistrationForm->createView(),
        ]);
    }

    /**
     * Doctor registration. User can register with email or with phoneNumber.
     *
     * @param Request                     $request
     * @param UserManager                 $userManager
     * @param UserPasswordHasherInterface $passwordHasher
     *
     * @return RedirectResponse|Response
     *
     * @throws InvalidArgumentException
     */
    #[Route('/reg/doctor', name: 'user_security_user_registration_doctor')]
    public function doctorRegistration(Request $request, UserManager $userManager, UserPasswordHasherInterface $passwordHasher): RedirectResponse|Response
    {
        $emailRegistrationForm = $this->createForm(EmailRegistrationType::class);
        $phoneNumberRegistrationForm = $this->createForm(PhoneNumberRegistrationType::class);

        $emailRegistrationForm->handleRequest($request);
        $phoneNumberRegistrationForm->handleRequest($request);

        if ($emailRegistrationForm->isSubmitted() && $emailRegistrationForm->isValid()) {
            /** @var UserEmailRegistrationDTO $userDTO */
            $userDTO = $emailRegistrationForm->getData();

            $user = User::createWithEmail($passwordHasher, $userDTO->password, $userDTO->nickname, $userDTO->name, $userDTO->surname, $userDTO->email);
            $user->setAbstractUserType(new User\DoctorUserType($user));
            $user->getProfileData()->setLanguage($request->getLocale());

            $userManager->create($user);

            return $this->redirectToRoute('app_login'); // TODO: Create redirect to auth route
        }

        if ($phoneNumberRegistrationForm->isSubmitted() && $phoneNumberRegistrationForm->isValid()) {
            /** @var UserPhoneNumberRegistrationDTO $userDTO */
            $userDTO = $phoneNumberRegistrationForm->getData();

            $user = User::createWithPhoneNumber($passwordHasher, $userDTO->password, $userDTO->nickname, $userDTO->name, $userDTO->surname, $userDTO->phoneNumber);
            $user->setAbstractUserType(new User\DoctorUserType($user));
            $user->getProfileData()->setLanguage($request->getLocale());

            $userManager->create($user);

            return $this->redirectToRoute('app_login'); // TODO: Create redirect to auth route
        }

        return $this->render('landing_pages/doctor_registration.html.twig', [
            'emailRegistrationForm' => $emailRegistrationForm->createView(),
            'phoneNumberRegistrationForm' => $phoneNumberRegistrationForm->createView(),
        ]);
    }

    /**
     * User login page. Where user can login with email, or with phone number. One field for phoneNumber and email.
     *
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return Response
     */
    #[Route(path: '/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('landing_pages/auth.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * Logout url, when user is here, he will be redirected and logout.
     */
    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
