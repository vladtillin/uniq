<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\DataFixtures;

use App\Entity\User;
use App\Exception\InvalidArgumentException;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Class AppFixtures.
 *
 * @author freevan04@gmail.com
 */
class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $userPasswordHasher;

    /**
     * @param UserPasswordHasherInterface $userPasswordHasher
     */
    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    /**
     * @param ObjectManager $manager
     *
     * @throws InvalidArgumentException
     */
    public function load(ObjectManager $manager): void
    {
        $user = User::createWithEmail($this->userPasswordHasher, 'Doctor123456', 'admin', 'Ivan', 'Tillin', 'freevan04@gmail.com');

        $manager->persist($user);

        $user->setAbstractUserType(new User\DoctorUserType($user));

        $manager->flush();
    }
}
