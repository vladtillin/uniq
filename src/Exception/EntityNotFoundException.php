<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

/**
 * Class EntityNotFoundException.
 */
class EntityNotFoundException extends NotFoundHttpException
{
    /**
     * @param string         $message
     * @param Throwable|null $previous
     * @param int            $code
     * @param array          $headers
     */
    public function __construct(string $message = '', Throwable $previous = null, int $code = 400, array $headers = [])
    {
        parent::__construct($message, $previous, $code, $headers);
    }
}
