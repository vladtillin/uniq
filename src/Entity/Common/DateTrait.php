<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\Common;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait DateTrait.
 *
 * !!Entity need HasLifecycleCallbacks.!!
 * Uses with App\Entity\Common\DateInterface.
 */
trait DateTrait
{
    #[ORM\Column(name: 'created_at', type: 'datetime', nullable: false)]
    private DateTime $createdAt;

    #[ORM\Column(name: 'updated_at', type: 'datetime', nullable: true)]
    private ?DateTime $updatedAt;

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return $this
     */
    #[ORM\PrePersist]
    public function setCreatedAt(): static
    {
        if (!isset($this->createdAt)) {
            $this->createdAt = new DateTime('now');
        }

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return $this
     */
    #[ORM\PreUpdate]
    public function setUpdatedAt($updatedAt = null): static
    {
        $this->updatedAt = new DateTime('now');

        return $this;
    }
}
