<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\Common;

use DateTime;

/**
 * Interface DateInterface.
 *
 * Interface to guarantee, that class contains createdAt and updatedAt. So u can get these parameters.
 */
interface DateInterface
{
    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime;

    /**
     * @return $this
     */
    public function setCreatedAt(): static;

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime;

    /**
     * @return $this
     */
    public function setUpdatedAt(?DateTime $updatedAt = null): static;
}
