<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\Common;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait DeletableTrait.
 *
 * Work with App\Entity\Common\DeletableTrait.
 */
trait DeletableTrait
{
    #[ORM\Column(name: 'deleted', type: 'boolean', nullable: false)]
    private bool $deleted = false;

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @return $this
     */
    public function delete(): static
    {
        $this->deleted = true;

        return $this;
    }
}
