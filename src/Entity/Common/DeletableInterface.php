<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\Common;

/**
 * Interface DeletableInterface.
 *
 * Guarantee that class has deleted parameter.
 */
interface DeletableInterface
{
    /**
     * @return bool
     */
    public function isDeleted(): bool;

    /**
     * @return $this
     */
    public function delete(): static;
}
