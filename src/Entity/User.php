<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity;

use App\Entity\Admin\AdminLog;
use App\Entity\Common\DateInterface;
use App\Entity\Common\DateTrait;
use App\Entity\Common\DeletableInterface;
use App\Entity\Common\DeletableTrait;
use App\Entity\User\AbstractUserType;
use App\Entity\User\DoctorUserType;
use App\Entity\User\Notification\Notification;
use App\Entity\User\ProfileData;
use App\Entity\User\Security\AuthHistory;
use App\Entity\User\Security\GoogleAuthenticator;
use App\Enum\Common\LanguageEnum;
use App\Exception\InvalidArgumentException;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Scheb\TwoFactorBundle\Model\Google\TwoFactorInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class User.
 *
 * User main entity.
 */
#[
    ORM\Entity(repositoryClass: UserRepository::class),
    ORM\Table('user'),
    ORM\HasLifecycleCallbacks,
]
class User implements UserInterface, PasswordAuthenticatedUserInterface, TwoFactorInterface, DateInterface, DeletableInterface
{
    use DateTrait;
    use DeletableTrait;

    #[
        ORM\Id,
        ORM\GeneratedValue,
        ORM\Column(type: 'integer'),
    ]
    private ?int $id = null;

    #[ORM\Column(name: 'email', type: 'string', length: 180, unique: true, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(name: 'phone_number', type: 'string', unique: true, nullable: true)]
    private ?string $phoneNumber = null;

    #[ORM\Column(name: 'roles', type: 'json', nullable: false)]
    private array $roles = [];

    #[ORM\Column(name: 'password', type: 'string', nullable: false)]
    private string $password;

    #[ORM\Column(name: 'nickname', type: 'string', unique: true, nullable: false)]
    private string $nickname;

    #[ORM\OneToOne(mappedBy: 'user', targetEntity: ProfileData::class, cascade: ['persist', 'remove'])]
    private ProfileData $profileData;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: AuthHistory::class, cascade: ['persist', 'remove'])]
    private Collection $authHistory;

    #[
        ORM\OneToOne(mappedBy: 'user', targetEntity: AbstractUserType::class, cascade: ['persist', 'remove'], orphanRemoval: true),
        ORM\JoinColumn(name: 'abstract_user_type_id', referencedColumnName: 'id'),
    ]
    private AbstractUserType $abstractUserType;

    #[
        ORM\OneToOne(mappedBy: 'user', targetEntity: GoogleAuthenticator::class, cascade: ['persist']),
        ORM\JoinColumn(onDelete: 'SET NULL'),
    ]
    private ?GoogleAuthenticator $googleAuthenticator = null;

    #[ORM\OneToOne(mappedBy: 'user', targetEntity: Notification::class, cascade: ['persist', 'remove'])]
    private Notification $notification;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: AdminLog::class, cascade: ['persist', 'remove'])]
    private Collection $adminLogs;

    #[ORM\Column(name: 'email_verified', type: 'boolean', nullable: false)]
    private bool $emailVerified = false;

    #[ORM\Column(name: 'phone_number_verified', type: 'boolean', nullable: false)]
    private bool $phoneNumberVerified = false;

    #[ORM\Column(name: 'phone_number_country', type: 'string', nullable: false)]
    private string $phoneNumberCountry = '';

    /**
     * Method uses to create user by phone number.
     *
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param string                      $password
     * @param string                      $nickname
     * @param string                      $name
     * @param string                      $surname
     * @param string                      $phoneNumber
     *
     * @return User
     *
     * @throws InvalidArgumentException
     */
    public static function createWithPhoneNumber(UserPasswordHasherInterface $userPasswordHasher, string $password, string $nickname, string $name, string $surname, string $phoneNumber): User
    {
        return new self($userPasswordHasher, $password, $nickname, $name, $surname, phoneNumber: $phoneNumber);
    }

    /**
     * Method uses to create user by email.
     *
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param string                      $password
     * @param string                      $nickname
     * @param string                      $name
     * @param string                      $surname
     * @param string                      $email
     *
     * @return User
     *
     * @throws InvalidArgumentException
     */
    public static function createWithEmail(UserPasswordHasherInterface $userPasswordHasher, string $password, string $nickname, string $name, string $surname, string $email): User
    {
        return new self($userPasswordHasher, $password, $nickname, $name, $surname, email: $email);
    }

    /**
     * Method always calls when entity is pushing into the database.
     * There is validation here.
     *
     * @throws InvalidArgumentException
     */
    #[ORM\PreFlush]
    public function validate(): void
    {
        if (null === $this->email && null === $this->phoneNumber) {
            throw new InvalidArgumentException('Something went wrong');
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        if (null === $this->getEmail()) {
            return $this->getId().': '.$this->getPhoneNumber();
        } else {
            return $this->getId().': '.$this->getEmail();
        }
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * By the site logic, user can not change email, when it's already set.
     *
     * @param string $email
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setEmail(string $email): static
    {
        if (null === $this->email) {
            $this->email = $email;
        } else {
            throw new InvalidArgumentException(sprintf('Email for user with id = %s already set', $this->getId()));
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * By the site logic, user can not change phone number, when it's already set.
     *
     * @param string $phoneNumber
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setPhoneNumber(string $phoneNumber): static
    {
        if (null === $this->phoneNumber) {
            $this->phoneNumber = $phoneNumber;
        } else {
            throw new InvalidArgumentException(sprintf('Phone number for user with id = %s already set', $this->getId()));
        }

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        if (null !== $this->email) {
            return $this->email;
        }

        return $this->phoneNumber;
    }

    /**
     * @see UserInterface
     */
    #[Pure]
    public function getRoles(): array
    {
        $roles = $this->roles;

        foreach ($this->abstractUserType->getRoles() as $role) {
            $roles[] = $role;
        }

        return array_unique($roles);
    }

    /**
     * @param array $roles
     *
     * @return $this
     */
    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param string                      $password
     *
     * @return $this
     */
    public function setPassword(UserPasswordHasherInterface $userPasswordHasher, string $password): static
    {
        $this->password = $userPasswordHasher->hashPassword($this, $password);

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
    }

    /**
     * @return string
     */
    public function getNickname(): string
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     *
     * @return $this
     */
    public function setNickname(string $nickname): static
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * @return ProfileData
     */
    public function getProfileData(): ProfileData
    {
        return $this->profileData;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getAuthHistory(): ArrayCollection|Collection
    {
        return $this->authHistory;
    }

    /**
     * @param AuthHistory $authHistory
     *
     * @return bool
     */
    public function hasAuthHistory(AuthHistory $authHistory): bool
    {
        return $this->authHistory->contains($authHistory);
    }

    /**
     * @param AuthHistory $authHistory
     *
     * @return $this
     */
    public function addAuthHistory(AuthHistory $authHistory): static
    {
        if (!$this->hasAuthHistory($authHistory)) {
            $this->authHistory->add($authHistory);
        }

        return $this;
    }

    /**
     * @param AuthHistory $authHistory
     *
     * @return $this
     */
    public function removeAuthHistory(AuthHistory $authHistory): static
    {
        if ($this->hasAuthHistory($authHistory)) {
            $this->authHistory->removeElement($authHistory);
        }

        $authHistory->delete();

        return $this;
    }

    /**
     * @return AbstractUserType
     */
    public function getAbstractUserType(): AbstractUserType
    {
        return $this->abstractUserType;
    }

    /**
     * @param AbstractUserType $abstractUserType
     *
     * @return $this
     */
    public function setAbstractUserType(AbstractUserType $abstractUserType): static
    {
        $this->abstractUserType = $abstractUserType;

        return $this;
    }

    /**
     * @return GoogleAuthenticator|null
     */
    public function getGoogleAuthenticator(): ?GoogleAuthenticator
    {
        return $this->googleAuthenticator;
    }

    /**
     * @param GoogleAuthenticator $googleAuthenticator
     *
     * @return $this
     */
    public function setGoogleAuthenticator(GoogleAuthenticator $googleAuthenticator): static
    {
        $this->googleAuthenticator = $googleAuthenticator;

        return $this;
    }

    /**
     * @return Notification
     */
    public function getNotification(): Notification
    {
        return $this->notification;
    }

    /**
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param string                      $password
     * @param string                      $nickname
     * @param string                      $name
     * @param string                      $surname
     * @param string|null                 $email
     * @param string|null                 $phoneNumber
     *
     * @throws InvalidArgumentException
     */
    private function __construct(UserPasswordHasherInterface $userPasswordHasher, string $password, string $nickname, string $name, string $surname, ?string $email = null, ?string $phoneNumber = null)
    {
        $this->setPassword($userPasswordHasher, $password);
        $this->nickname = $nickname;
        $this->profileData = new ProfileData($this, $name, $surname);
        $this->authHistory = new ArrayCollection();
        $this->email = $email;
        $this->phoneNumber = $phoneNumber;
        $this->profileData->setLanguage(LanguageEnum::EN);
        $this->notification = new Notification($this);
    }

    /**
     * @return bool
     */
    public function isGoogleAuthenticatorEnabled(): bool
    {
        return null !== $this->googleAuthenticator;
    }

    /**
     * @return string
     */
    public function getGoogleAuthenticatorUsername(): string
    {
        if (null === $this->email) {
            return $this->phoneNumber;
        }

        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getGoogleAuthenticatorSecret(): ?string
    {
        return $this->googleAuthenticator?->getSecretCode();
    }

    /**
     * @return Collection
     */
    public function getAdminLogs(): Collection
    {
        return $this->adminLogs;
    }

    /**
     * @param AdminLog $adminLog
     *
     * @return bool
     */
    public function hasAdminLog(AdminLog $adminLog): bool
    {
        return $this->adminLogs->contains($adminLog);
    }

    /**
     * @param AdminLog $adminLog
     *
     * @return $this
     */
    public function addAdminLog(AdminLog $adminLog): static
    {
        if (!$this->hasAdminLog($adminLog)) {
            $this->adminLogs->add($adminLog);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isDoctor(): bool
    {
        return $this->getAbstractUserType() instanceof DoctorUserType;
    }

    /**
     * @return bool
     */
    public function isEmailVerified(): bool
    {
        return $this->emailVerified;
    }

    /**
     * @param bool $emailVerified
     *
     * @return $this
     */
    public function setEmailVerified(bool $emailVerified): static
    {
        $this->emailVerified = $emailVerified;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPhoneNumberVerified(): bool
    {
        return $this->phoneNumberVerified;
    }

    /**
     * @param bool $phoneNumberVerified
     *
     * @return $this
     */
    public function setPhoneNumberVerified(bool $phoneNumberVerified): static
    {
        $this->phoneNumberVerified = $phoneNumberVerified;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumberCountry(): string
    {
        return $this->phoneNumberCountry;
    }

    /**
     * @param string $phoneNumberCountry
     *
     * @return $this
     */
    public function setPhoneNumberCountry(string $phoneNumberCountry): static
    {
        $this->phoneNumberCountry = $phoneNumberCountry;

        return $this;
    }
}
