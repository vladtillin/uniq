<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\Admin;

use App\Entity\Common\DateInterface;
use App\Entity\Common\DateTrait;
use App\Entity\Common\DeletableInterface;
use App\Entity\Common\DeletableTrait;
use App\Entity\User;
use App\Repository\Admin\AdminLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AdminLog.
 *
 * Admin logs entity.
 *
 * @author freevan04@gmail.com
 */
#[
    ORM\Entity(repositoryClass: AdminLogRepository::class),
    ORM\Table('admin_log'),
    ORM\HasLifecycleCallbacks,
]
class AdminLog implements DateInterface, DeletableInterface
{
    use DateTrait;
    use DeletableTrait;

    #[
        ORM\Id,
        ORM\GeneratedValue,
        ORM\Column(type: 'integer'),
    ]
    private ?int $id = null;

    #[ORM\Column(name: 'log_text', type: 'text', nullable: false)]
    private string $logText;

    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist', 'remove'], inversedBy: 'adminLogs')]
    private User $user;

    /**
     * @param User   $user
     * @param string $logText
     */
    public function __construct(User $user, string $logText)
    {
        $user->addAdminLog($this);
        $this->user = $user;
        $this->logText = $logText;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLogText(): string
    {
        return $this->logText;
    }

    /**
     * @param string $text
     *
     * @return $this
     */
    public function setLogText(string $text): static
    {
        $this->logText = $text;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
