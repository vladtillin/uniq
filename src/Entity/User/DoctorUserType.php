<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\User;

use App\Entity\User;
use App\Entity\User\Doctor\DoctorVerification;
use App\Enum\User\UserRoleEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

/**
 * Class DoctorUserType.
 *
 * Doctor role entity.
 */
#[
    ORM\Entity,
    ORM\Table(name: 'user_doctor_user_type'),
    ORM\HasLifecycleCallbacks,
]
class DoctorUserType extends AbstractUserType
{
    #[ORM\Column(name: 'verified', type: 'boolean', nullable: false)]
    private bool $verified = false;

    #[ORM\OneToOne(mappedBy: 'doctorUserType', targetEntity: DoctorVerification::class, cascade: ['persist', 'remove'])]
    private ?DoctorVerification $verification = null;

    #[ORM\OneToMany(mappedBy: 'doctor', targetEntity: PatientUserType::class, cascade: ['persist', 'remove'])]
    private Collection $patients;

    /**
     * @param User $user
     */
    #[Pure]
    public function __construct(User $user)
    {
        $this->patients = new ArrayCollection();
        parent::__construct($user);
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return [UserRoleEnum::DOCTOR];
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->verified;
    }

    /**
     * @param bool $verified
     *
     * @return $this
     */
    public function setVerified(bool $verified): static
    {
        $this->verified = $verified;

        return $this;
    }

    /**
     * @return DoctorVerification|null
     */
    public function getVerification(): ?DoctorVerification
    {
        return $this->verification;
    }

    /**
     * @param DoctorVerification $doctorVerification
     *
     * @return $this
     */
    public function setVerification(DoctorVerification $doctorVerification): static
    {
        $this->verification = $doctorVerification;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getPatients(): Collection
    {
        return $this->patients;
    }
}
