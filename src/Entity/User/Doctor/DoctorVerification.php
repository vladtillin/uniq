<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\User\Doctor;

use App\Entity\Common\DateInterface;
use App\Entity\Common\DateTrait;
use App\Entity\Common\DeletableInterface;
use App\Entity\Common\DeletableTrait;
use App\Entity\User\DoctorUserType;
use App\Repository\User\Doctor\DoctorVerificationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class DoctorVerification.
 *
 * Verification for doctor.
 */
#[
    ORM\Entity(repositoryClass: DoctorVerificationRepository::class),
    ORM\Table(name: 'user_doctor_doctor_verification'),
    ORM\HasLifecycleCallbacks,
]
class DoctorVerification implements DateInterface, DeletableInterface
{
    use DateTrait;
    use DeletableTrait;

    #[
        ORM\Id,
        ORM\GeneratedValue,
        ORM\Column(type: 'integer'),
    ]
    private ?int $id = null;

    /**
     * @var DoctorUserType doctor role entity
     */
    #[ORM\OneToOne(inversedBy: 'verification', targetEntity: DoctorUserType::class, cascade: ['persist', 'remove'])]
    private DoctorUserType $doctorUserType;

    #[ORM\Column(name: 'name', type: 'string', nullable: false)]
    private string $name;

    #[ORM\Column(name: 'surname', type: 'string', nullable: false)]
    private string $surname;

    #[ORM\Column(name: 'patronymic', type: 'string', nullable: false)]
    private string $patronymic;

    /**
     * @var string user selfie with document photo
     */
    #[ORM\Column(name: 'selfie_file_id', type: 'text', nullable: false)]
    private string $selfieFileId;

    /**
     * @var string user's document frontside photo
     */
    #[ORM\Column(name: 'document_front_side_file_id', type: 'text', nullable: false)]
    private string $documentFrontSideFileId;

    /**
     * @var string user's document backside photo
     */
    #[ORM\Column(name: 'document_bask_side_file_id', type: 'text', nullable: false)]
    private string $documentBaskSideFileId;

    /**
     * @param DoctorUserType $doctorUserType
     * @param string         $name
     * @param string         $surname
     * @param string         $patronymic
     * @param string         $selfieFileId
     * @param string         $documentFrontSideFileId
     * @param string         $documentBaskSideFileId
     */
    public function __construct(DoctorUserType $doctorUserType, string $name, string $surname, string $patronymic, string $selfieFileId, string $documentFrontSideFileId, string $documentBaskSideFileId)
    {
        $this->doctorUserType = $doctorUserType;
        $this->name = $name;
        $this->surname = $surname;
        $this->patronymic = $patronymic;
        $this->selfieFileId = $selfieFileId;
        $this->documentFrontSideFileId = $documentFrontSideFileId;
        $this->documentBaskSideFileId = $documentBaskSideFileId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return DoctorUserType
     */
    public function getDoctorUserType(): DoctorUserType
    {
        return $this->doctorUserType;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getPatronymic(): string
    {
        return $this->patronymic;
    }

    /**
     * @return string
     */
    public function getSelfieFileId(): string
    {
        return $this->selfieFileId;
    }

    /**
     * @return string
     */
    public function getDocumentFrontSideFileId(): string
    {
        return $this->documentFrontSideFileId;
    }

    /**
     * @return string
     */
    public function getDocumentBaskSideFileId(): string
    {
        return $this->documentBaskSideFileId;
    }
}
