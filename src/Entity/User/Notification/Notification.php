<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\User\Notification;

use App\Entity\Common\DateInterface;
use App\Entity\Common\DateTrait;
use App\Entity\Common\DeletableInterface;
use App\Entity\Common\DeletableTrait;
use App\Entity\User;
use App\Repository\User\Notification\NotificationRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Notification.
 *
 * User's notification settings.
 */
#[
    ORM\Entity(repositoryClass: NotificationRepository::class),
    ORM\Table(name: 'user_notification_notification'),
    ORM\HasLifecycleCallbacks,
]
class Notification implements DateInterface, DeletableInterface
{
    use DateTrait;
    use DeletableTrait;

    #[
        ORM\Id,
        ORM\GeneratedValue,
        ORM\Column(type: 'integer'),
    ]
    private ?int $id = null;

    #[
        ORM\OneToOne(inversedBy: 'notification', targetEntity: User::class, cascade: ['persist', 'remove']),
        Serializer\Exclude,
    ]
    private User $user;

    /**
     * @var bool is notification about new messages from users enabled for this user
     */
    #[ORM\Column(name: 'messages_notifications', type: 'boolean', nullable: false)]
    private bool $messagesNotifications = true;

    /**
     * @var bool is updates notifications enabled for this user
     */
    #[ORM\Column(name: 'updates_notifications', type: 'boolean', nullable: false)]
    private bool $updatesNotifications = true;

    /**
     * @var bool is advertising notifications enabled for this user
     */
    #[ORM\Column(name: 'advertising_notifications', type: 'boolean', nullable: false)]
    private bool $advertisingNotifications = true;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getId().': '.$this->getUser()->getId();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function isMessagesNotifications(): bool
    {
        return $this->messagesNotifications;
    }

    /**
     * @param bool $messagesNotifications
     *
     * @return $this
     */
    public function setMessagesNotifications(bool $messagesNotifications): static
    {
        $this->messagesNotifications = $messagesNotifications;

        return $this;
    }

    /**
     * @return bool
     */
    public function isUpdatesNotifications(): bool
    {
        return $this->updatesNotifications;
    }

    /**
     * @param bool $updatesNotifications
     *
     * @return $this
     */
    public function setUpdatesNotifications(bool $updatesNotifications): static
    {
        $this->updatesNotifications = $updatesNotifications;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdvertisingNotifications(): bool
    {
        return $this->advertisingNotifications;
    }

    /**
     * @param bool $advertisingNotifications
     *
     * @return $this
     */
    public function setAdvertisingNotifications(bool $advertisingNotifications): static
    {
        $this->advertisingNotifications = $advertisingNotifications;

        return $this;
    }
}
