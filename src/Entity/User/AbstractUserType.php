<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\User;

use App\Entity\User;
use App\Repository\User\AbstractUserTypeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractUserType.
 *
 * Abstract type for roles (doctor, patient).
 */
#[
    ORM\Entity(repositoryClass: AbstractUserTypeRepository::class),
    ORM\Table(name: 'user_abstract_user_type'),
    ORM\InheritanceType('JOINED'),
    ORM\DiscriminatorColumn(name: 'type', type: 'string', length: 32),
    ORM\DiscriminatorMap([
        'doctor' => DoctorUserType::class,
        'patient' => PatientUserType::class,
    ]),
    ORM\HasLifecycleCallbacks,
]
class AbstractUserType
{
    #[
        ORM\Id,
        ORM\GeneratedValue,
        ORM\Column(type: 'integer'),
    ]
    private ?int $id = null;

    #[
        ORM\OneToOne(inversedBy: 'abstractUserType', targetEntity: User::class, cascade: ['persist', 'remove']),
        ORM\JoinColumn(onDelete: 'CASCADE'),
    ]
    private User $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
