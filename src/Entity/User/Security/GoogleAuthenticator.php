<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\User\Security;

use App\Entity\Common\DateInterface;
use App\Entity\Common\DateTrait;
use App\Entity\Common\DeletableInterface;
use App\Entity\Common\DeletableTrait;
use App\Entity\User;
use App\Repository\User\Security\GoogleAuthenticatorRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class GoogleAuthenticator.
 *
 * Google authenticator for user. For two-factor auth.
 */
#[
    ORM\Entity(repositoryClass: GoogleAuthenticatorRepository::class),
    ORM\Table(name: 'user_security_google_authenticator'),
    ORM\HasLifecycleCallbacks,
]
class GoogleAuthenticator implements DateInterface, DeletableInterface
{
    use DateTrait;
    use DeletableTrait;

    #[
        ORM\Id,
        ORM\GeneratedValue,
        ORM\Column(type: 'integer'),
    ]
    private ?int $id = null;

    #[
        ORM\OneToOne(inversedBy: 'googleAuthenticator', targetEntity: User::class, cascade: ['persist']),
        ORM\JoinColumn(onDelete: 'SET NULL'),
    ]
    private ?User $user;

    /**
     * @var string secret code, to verify is the code from user valid
     */
    #[ORM\Column(name: 'secret_code', type: 'string', nullable: false)]
    private string $secretCode;

    /**
     * @param User   $user
     * @param string $secretCode
     */
    public function __construct(User $user, string $secretCode)
    {
        $this->user = $user;
        $this->secretCode = $secretCode;
        $user->setGoogleAuthenticator($this);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getUser()->getId();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    public function remove(): void
    {
        $this->user = null;
    }

    /**
     * @return string
     */
    public function getSecretCode(): string
    {
        return $this->secretCode;
    }
}
