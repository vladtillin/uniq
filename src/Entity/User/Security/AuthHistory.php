<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\User\Security;

use App\Entity\Common\DateInterface;
use App\Entity\Common\DateTrait;
use App\Entity\Common\DeletableInterface;
use App\Entity\Common\DeletableTrait;
use App\Entity\User;
use App\Repository\User\Security\AuthHistoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AuthHistory.
 *
 * History of user's auth. To detect and notify user, if he will be hacked.
 */
#[
    ORM\Entity(repositoryClass: AuthHistoryRepository::class),
    ORM\HasLifecycleCallbacks,
    ORM\Table(name: 'user_security_auth_history'),
]
class AuthHistory implements DateInterface, DeletableInterface
{
    use DateTrait;
    use DeletableTrait;

    #[
        ORM\Id,
        ORM\GeneratedValue,
        ORM\Column(type: 'integer'),
    ]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist', 'remove'], inversedBy: 'authHistory')]
    private User $user;

    /**
     * @var string the token of authorization
     */
    #[ORM\Column(name: 'token', type: 'string', nullable: false)]
    private string $token;

    /**
     * @var string identifier of authorization, email or phoneNumber
     */
    #[ORM\Column(name: 'identifier', type: 'string', nullable: false)]
    private string $identifier;

    /**
     * @param User   $user
     * @param string $token
     * @param string $identifier
     */
    public function __construct(User $user, string $token, string $identifier)
    {
        $this->user = $user;
        $this->token = $token;
        $this->identifier = $identifier;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getId().': '.$this->getIdentifier();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }
}
