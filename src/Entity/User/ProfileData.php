<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\User;

use App\Entity\Common\DateInterface;
use App\Entity\Common\DateTrait;
use App\Entity\User;
use App\Exception\InvalidArgumentException;
use App\Repository\User\ProfileDataRepository;
use App\ValueObject\Common\LanguageValueObject;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ProfileData.
 *
 * Profile data of the user.
 */
#[
    ORM\Entity(repositoryClass: ProfileDataRepository::class),
    ORM\Table(name: 'user_profile_data'),
    ORM\HasLifecycleCallbacks,
]
class ProfileData implements DateInterface
{
    use DateTrait;

    #[
        ORM\Id,
        ORM\GeneratedValue,
        ORM\Column(type: 'integer'),
    ]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'profileData', targetEntity: User::class, cascade: ['persist', 'remove'])]
    private User $user;

    #[ORM\Column(name: 'name', type: 'string', nullable: false)]
    private string $name;

    #[ORM\Column(name: 'surname', type: 'string', nullable: false)]
    private string $surname;

    #[ORM\Column(name: 'patronymic', type: 'string', nullable: true)]
    private ?string $patronymic = null;

    #[ORM\Column(name: 'birth_date', type: 'datetime', nullable: true)]
    private ?DateTime $birthDate = null;

    #[ORM\Column(name: 'avatar', type: 'text', nullable: true)]
    private ?string $avatar = null;

    #[ORM\Column(name: 'country', type: 'string', nullable: true)]
    private ?string $country = null;

    #[ORM\Column(name: 'city', type: 'string', nullable: true)]
    private ?string $city = null;

    #[ORM\Column(name: 'language', type: 'string', nullable: false)]
    private string $language;

    /**
     * @param User   $user
     * @param string $name
     * @param string $surname
     */
    public function __construct(User $user, string $name, string $surname)
    {
        $this->user = $user;
        $this->name = $name;
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getId().': '.$this->getName().' '.$this->getSurname();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @return string|null
     */
    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    /**
     * @param string $patronymic
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function setPatronymic(string $patronymic): string
    {
        if (empty(trim($patronymic))) {
            throw new InvalidArgumentException(sprintf('Empty patronymic for user with id %s', $this->getId()));
        } elseif (null !== $this->patronymic) {
            throw new InvalidArgumentException(sprintf('Patronymic already set for user with id %s', $this->getId()));
        } else {
            $this->patronymic = $patronymic;
        }

        return $patronymic;
    }

    /**
     * @return DateTime|null
     */
    public function getBirthDate(): ?DateTime
    {
        return $this->birthDate;
    }

    /**
     * Set birthDate of user. If birthdate is more than now - the date is invalid.
     * If the birthDate already set - exception.
     *
     * @param DateTime|null $birthDate
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setBirthDate(?DateTime $birthDate = null): static
    {
        if (null === $birthDate) {
            $this->birthDate = $birthDate;

            return $this;
        }

        if ((new DateTime('now'))->getTimestamp() <= $birthDate->getTimestamp()) {
            throw new InvalidArgumentException(sprintf('Invalid birthDate, timestamp = %s', $birthDate->getTimestamp()));
        } elseif (null !== $this->birthDate) {
            throw new InvalidArgumentException('Birthdate already set, for user with id = %s', $this->getId());
        }

        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * If empty string - remove avatar.
     *
     * @param string|null $avatar
     *
     * @return $this
     */
    public function setAvatar(?string $avatar = null): static
    {
        if (null === $avatar) {
            $this->avatar = null;
        } else {
            $this->avatar = $avatar;
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setCountry(string $country): static
    {
        if (empty(trim($country))) {
            throw new InvalidArgumentException(sprintf('Invalid country in ProfileData, value = %s', $country));
        } elseif (null !== $this->country) {
            throw new InvalidArgumentException(sprintf('Country already set for user with id = %s', $this->getId()));
        }

        $this->country = $country;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setCity(string $city): static
    {
        if (empty(trim($city))) {
            throw new InvalidArgumentException(sprintf('Invalid city in ProfileData, value = %s', $city));
        } elseif (null !== $this->city) {
            throw new InvalidArgumentException(sprintf('City already set for user with id = %s', $this->getId()));
        }

        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $languageKey
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setLanguage(string $languageKey): static
    {
        if (false === LanguageValueObject::validateLanguage($languageKey)) {
            throw new InvalidArgumentException(sprintf('Invalid language, language value = %s', $languageKey));
        }

        $this->language = $languageKey;

        return $this;
    }
}
