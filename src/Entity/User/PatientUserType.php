<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Entity\User;

use App\Entity\User;
use App\Enum\User\UserRoleEnum;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

/**
 * Class PatientUserType.
 *
 * Patient role entity.
 */
#[
    ORM\Entity,
    ORM\Table(name: 'user_patient_user_type'),
    ORM\HasLifecycleCallbacks,
]
class PatientUserType extends AbstractUserType
{
    #[ORM\ManyToOne(targetEntity: DoctorUserType::class, cascade: ['persist', 'remove'], inversedBy: 'patients')]
    private ?DoctorUserType $doctor = null;

    /**
     * @param User $user
     */
    #[Pure]
    public function __construct(User $user)
    {
        parent::__construct($user);
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return [UserRoleEnum::PATIENT];
    }

    /**
     * @return DoctorUserType|null
     */
    public function getDoctor(): ?DoctorUserType
    {
        return $this->doctor;
    }

    /**
     * @param DoctorUserType|null $doctorUserType
     *
     * @return $this
     */
    public function setDoctor(?DoctorUserType $doctorUserType = null): static
    {
        $this->doctor = $doctorUserType;

        return $this;
    }
}
