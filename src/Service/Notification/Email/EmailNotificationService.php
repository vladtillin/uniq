<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Service\Notification\Email;

use App\DTO\Notification\Email\EmailMessageDTO;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class EmailNotificationService.
 *
 * Service to send emails for users.
 *
 * @author freevan04@gmail.com
 */
class EmailNotificationService
{
    private string $senderEmail;

    private MailerInterface $mailer;

    private Environment $twig;

    private LoggerInterface $emailNotificationLogger;

    /**
     * @param string          $senderEmail
     * @param MailerInterface $mailer
     * @param Environment     $twig
     * @param LoggerInterface $emailNotificationLogger
     */
    public function __construct(string $senderEmail, MailerInterface $mailer, Environment $twig, LoggerInterface $emailNotificationLogger)
    {
        $this->senderEmail = $senderEmail;
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->emailNotificationLogger = $emailNotificationLogger;
    }

    /**
     * Sending email.
     *
     * @param EmailMessageDTO $emailMessageDTO
     *
     * @throws TransportExceptionInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendEmail(EmailMessageDTO $emailMessageDTO): void
    {
        $email = (new Email())
            ->from(new Address($this->senderEmail, 'DoctorTillin'))
            ->to($emailMessageDTO->email)
            ->subject($emailMessageDTO->subject)
            ->html($this->twig->render($emailMessageDTO->htmlTemplate, $emailMessageDTO->context), 'text/html');

        $this->mailer->send($email);
        $this->emailNotificationLogger->info(sprintf('Message for email %s was sent', $emailMessageDTO->email));
    }
}
