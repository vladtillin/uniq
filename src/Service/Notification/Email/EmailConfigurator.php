<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Service\Notification\Email;

use App\DTO\Notification\Email\EmailMessageDTO;
use App\Service\User\UserService;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class EmailConfigurator.
 *
 * Used to configure emails for different situations.
 *
 * @author freevan04@gmail.com
 */
class EmailConfigurator
{
    private TranslatorInterface $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param string $email
     *
     * @return EmailMessageDTO
     */
    public function emailForVerification(string $email): EmailMessageDTO
    {
        $verificationCode = UserService::generateCodeForEmailVerification();

        $emailMessageDTO = new EmailMessageDTO();
        $emailMessageDTO->email = $email;
        $emailMessageDTO->subject = $this->translator->trans('email_verification.title', domain: 'emails');
        $emailMessageDTO->htmlTemplate = 'emails/email_verification.html.twig';
        $emailMessageDTO->context = ['verificationCode' => $verificationCode];

        return $emailMessageDTO;
    }
}
