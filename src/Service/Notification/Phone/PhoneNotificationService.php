<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Service\Notification\Phone;

use App\Enum\Notification\SendPulse\SendPulseEnum;
use App\Model\Notification\Phone\SmsNotificationModel;
use App\Model\Notification\Phone\ViberAndSmsNotificationModel;
use JMS\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Class PhoneNotificationService.
 *
 * The service for interaction with SendPulse.
 *
 * @see https://sendpulse.com/ru/integrations/api/viber
 *
 * @author freevan04@gmail.com
 */
class PhoneNotificationService
{
    private HttpClientInterface $httpClient;

    private string $sendPulseClientId;

    private string $sendPulseClientSecret;

    private SerializerInterface $serializer;

    private string $sendPulseSenderId;

    /**
     * @param HttpClientInterface $httpClient
     * @param string              $sendPulseClientId
     * @param string              $sendPulseClientSecret
     * @param SerializerInterface $serializer
     * @param string              $sendPulseSenderId
     */
    public function __construct(HttpClientInterface $httpClient, string $sendPulseClientId, string $sendPulseClientSecret, SerializerInterface $serializer, string $sendPulseSenderId)
    {
        $this->httpClient = $httpClient;
        $this->sendPulseClientId = $sendPulseClientId;
        $this->sendPulseClientSecret = $sendPulseClientSecret;
        $this->serializer = $serializer;
        $this->sendPulseSenderId = $sendPulseSenderId;
    }

    /**
     * Getting auth token for SendPulse.
     *
     * @return string
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getAuthToken(): string
    {
        $body = ['grant_type' => 'client_credentials', 'client_id' => $this->sendPulseClientId, 'client_secret' => $this->sendPulseClientSecret];

        $response = $this->httpClient->request('POST', SendPulseEnum::SENDPULSE_URL.'oauth/access_token', ['headers' => ['Content-Type' => 'application/json'], 'body' => json_encode($body, true)]);

        return json_decode($response->getContent(false), associative: true)['access_token'];
    }

    /**
     * Request to SendPulse with authorization headers.
     *
     * @param string $method
     * @param string $path
     * @param string $body
     * @param array  $headers
     *
     * @return ResponseInterface
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function sendPulseRequest(string $method, string $path, string $body = '', array $headers = []): ResponseInterface
    {
        $options = [];

        $currentHeaders = array_merge($headers, ['Authorization' => 'Bearer '.$this->getAuthToken(), 'Content-Type' => 'application/json']);
        $options['headers'] = $currentHeaders;
        $options['body'] = $body;

        return $this->httpClient->request($method, SendPulseEnum::SENDPULSE_URL.$path, $options);
    }

    /**
     * Try to send message in viber, then in sms.
     *
     * @param ViberAndSmsNotificationModel $viberAndSmsNotificationModel
     *
     * @return ResponseInterface
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function sendViberAndSmsNotification(ViberAndSmsNotificationModel $viberAndSmsNotificationModel): ResponseInterface
    {
        return $this->sendPulseRequest('POST', SendPulseEnum::SENDPULSE_VIBER_SMS_URL, $this->serializer->serialize($viberAndSmsNotificationModel, 'json'));
    }

    /**
     * Sending message in sms only.
     *
     * @param SmsNotificationModel $smsNotificationModel
     *
     * @return ResponseInterface
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function sendSms(SmsNotificationModel $smsNotificationModel): ResponseInterface
    {
        return $this->sendPulseRequest('POST', SendPulseEnum::SENDPULSE_SMS_URL, $this->serializer->serialize($smsNotificationModel, 'json'));
    }

    /**
     * Get info about sender (me).
     *
     * @return array
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getSenderInfo(): array
    {
        return json_decode($this->sendPulseRequest('GET', SendPulseEnum::SENDPULSE_SENDER_INFO)->getContent(false), true);
    }
}
