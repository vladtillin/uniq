<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Service\Upload;

use App\DTO\Minio\FileDTO;
use App\Entity\User;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * Class FileUpload.
 *
 * Service for uploading files on server.
 *
 * @author freevan04@gmail.com
 */
class FileUploader
{
    private string $avatarDirectory;

    private SluggerInterface $slugger;

    /**
     * @param string           $avatarDirectory
     * @param SluggerInterface $slugger
     */
    public function __construct(string $avatarDirectory, SluggerInterface $slugger)
    {
        $this->avatarDirectory = $avatarDirectory;
        $this->slugger = $slugger;
    }

    /**
     * The method, that upload user's avatar on the server.
     *
     * @param UploadedFile $file
     *
     * @return FileDTO
     */
    public function uploadUserAvatar(UploadedFile $file): FileDTO
    {
        return $this->upload($file, $this->avatarDirectory);
    }

    /**
     * Upload file on server.
     *
     * @param UploadedFile $file
     * @param string       $targetDirectory
     *
     * @return FileDTO
     */
    private function upload(UploadedFile $file, string $targetDirectory): FileDTO
    {
        $fileDTO = new FileDTO();
        $fileDTO->type = $file->getMimeType();

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        try {
            $file->move($targetDirectory, $fileName);
        } catch (FileException $e) {
        }

        $path = $targetDirectory.'/'.$fileName;

        $fileDTO->path = $path;

        return $fileDTO;
    }

    /**
     * The method generating filename for user.
     *
     * @param User $user
     *
     * @return string
     */
    public function generateFilenameForUser(User $user): string
    {
        return Uuid::uuid4().$user->getNickname().$user->getId();
    }
}
