<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Service\User;

/**
 * Class UserService.
 *
 * @author freevan04@gmail.com
 */
class UserService
{
    /**
     * @return string
     */
    public static function generateCodeForEmailVerification(): string
    {
        return substr(md5(uniqid(rand(), true)), 6, 6);
    }
}
