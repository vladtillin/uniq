<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Service\Minio;

use App\DTO\Minio\FileDTO;
use App\Enum\Minio\MinioBucketsEnum;
use Aws\Result;
use Aws\S3\S3Client;

/**
 * Class MinioService.
 *
 * Service for interaction with MinIO.
 *
 * @see https://docs.min.io/docs/how-to-use-aws-sdk-for-php-with-minio-server.html
 *
 * @author freevan04@gmail.com
 */
class MinioService
{
    private S3Client $s3;

    /**
     * @param string $minioApiLogin
     * @param string $minioApiPassword
     * @param string $minioApiEndpointUrl
     */
    public function __construct(string $minioApiLogin, string $minioApiPassword, string $minioApiEndpointUrl)
    {
        $this->s3 = $this->configureClient($minioApiLogin, $minioApiPassword, $minioApiEndpointUrl);
    }

    /**
     * Configuring MinIO(AWS S3) client.
     *
     * @param string $minioApiLogin
     * @param string $minioApiPassword
     * @param string $minioApiEndpointUrl
     *
     * @return S3Client
     */
    private function configureClient(string $minioApiLogin, string $minioApiPassword, string $minioApiEndpointUrl): S3Client
    {
        return new S3Client([
            'version' => 'latest',
            'region' => 'us-east-1',
            'endpoint' => $minioApiEndpointUrl,
            'use_path_style_endpoint' => true,
            'credentials' => [
                'key' => $minioApiLogin,
                'secret' => $minioApiPassword,
            ],
        ]);
    }

    /**
     * Putting file into MinIO bucket.
     *
     * @param FileDTO $fileDTO
     *
     * @return Result
     */
    public function putFile(FileDTO $fileDTO): Result
    {
        return $this->s3->putObject([
            'Bucket' => $fileDTO->minioBucketName,
            'Key' => $fileDTO->minioFilename,
            'ACL' => 'public-read',
            'SourceFile' => $fileDTO->path,
            'ContentType' => $fileDTO->type,
        ]);
    }

    /**
     * Get public file url, from MinIO bucket.
     *
     * @param string $bucketName
     * @param string $minioFileName
     *
     * @return string
     */
    public function getFileUrl(string $bucketName, string $minioFileName): string
    {
        $cmd = $this->s3->getCommand('GetObject', [
            'Bucket' => $bucketName,
            'Key' => $minioFileName,
        ]);

        $request = $this->s3->createPresignedRequest($cmd, '+10 minutes');

        return (string) $request->getUri();
    }

    /**
     * Delete file from MinIO bucket.
     *
     * @param string $bucketName
     * @param string $minioFileName
     *
     * @return Result
     */
    public function deleteFile(string $bucketName, string $minioFileName): Result
    {
        return $this->s3->deleteObject([
            'Bucket' => $bucketName,
            'Key' => $minioFileName,
        ]);
    }

    /**
     * Creating bucket in MinIO.
     *
     * @param string $bucket
     * @param bool   $publicBucket
     *
     * @return bool
     */
    public function createBucket(string $bucket, bool $publicBucket = false): bool
    {
        if (!$this->s3->doesBucketExist($bucket)) {
            $this->s3->createBucket(['Bucket' => $bucket]);

            if ($publicBucket) {
                $this->configurePublicBucket($bucket);
            }

            return true;
        }

        return false;
    }

    /**
     * Configure bucket for public policy in MinIO.
     *
     * @param string $bucket
     *
     * @return bool
     */
    public function configurePublicBucket(string $bucket): bool
    {
        $this->s3->putBucketPolicy(['Bucket' => $bucket, 'Policy' => sprintf(MinioBucketsEnum::PUBLIC_BUCKET_POLICY, $bucket, $bucket)]);

        return true;
    }
}
