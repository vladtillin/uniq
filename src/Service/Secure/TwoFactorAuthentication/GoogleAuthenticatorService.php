<?php
/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

namespace App\Service\Secure\TwoFactorAuthentication;

use App\DTO\Secure\TwoFactorAuthentication\GoogleAuthenticatorSecretDTO;
use App\Entity\User;
use Sonata\GoogleAuthenticator\GoogleAuthenticator;
use Sonata\GoogleAuthenticator\GoogleAuthenticatorInterface;
use Sonata\GoogleAuthenticator\GoogleQrUrl;

/**
 * Class GoogleAuthenticatorService.
 *
 * Service for interaction with GoogleAuthenticator.
 *
 * @author freevan04@gmail.com
 */
class GoogleAuthenticatorService
{
    private GoogleAuthenticatorInterface $googleAuthenticator;

    public function __construct()
    {
        $this->googleAuthenticator = new GoogleAuthenticator();
    }

    /**
     * Generating secretCode and QRCode image url for googleAuthenticator.
     *
     * @param User $user
     *
     * @return GoogleAuthenticatorSecretDTO
     */
    public function generateGoogleAuthenticatorData(User $user): GoogleAuthenticatorSecretDTO
    {
        $googleAuthenticatorSecretDTO = new GoogleAuthenticatorSecretDTO();
        $googleAuthenticatorSecretDTO->secretKey = $this->googleAuthenticator->generateSecret();
        $googleAuthenticatorSecretDTO->QRCodeUrl = GoogleQrUrl::generate($user->getNickname().'@doctor-tillin', $googleAuthenticatorSecretDTO->secretKey);

        return $googleAuthenticatorSecretDTO;
    }

    /**
     * Check if user's code is valid for GoogleAuthenticator.
     *
     * @param string $secretCode
     * @param string $code
     *
     * @return bool
     */
    public function checkCode(string $secretCode, string $code): bool
    {
        return $this->googleAuthenticator->checkCode($secretCode, $code, 0);
    }
}
