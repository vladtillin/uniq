<?php

namespace ContainerAky3rNp;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolderdd4cd = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerfcc8d = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties771d1 = [
        
    ];

    public function getConnection()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getConnection', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getMetadataFactory', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getExpressionBuilder', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'beginTransaction', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getCache', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getCache();
    }

    public function transactional($func)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'transactional', array('func' => $func), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'wrapInTransaction', array('func' => $func), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'commit', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->commit();
    }

    public function rollback()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'rollback', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getClassMetadata', array('className' => $className), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'createQuery', array('dql' => $dql), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'createNamedQuery', array('name' => $name), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'createQueryBuilder', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'flush', array('entity' => $entity), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'clear', array('entityName' => $entityName), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->clear($entityName);
    }

    public function close()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'close', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->close();
    }

    public function persist($entity)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'persist', array('entity' => $entity), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'remove', array('entity' => $entity), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'refresh', array('entity' => $entity), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'detach', array('entity' => $entity), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'merge', array('entity' => $entity), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getRepository', array('entityName' => $entityName), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'contains', array('entity' => $entity), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getEventManager', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getConfiguration', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'isOpen', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getUnitOfWork', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getProxyFactory', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'initializeObject', array('obj' => $obj), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'getFilters', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'isFiltersStateClean', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'hasFilters', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return $this->valueHolderdd4cd->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerfcc8d = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolderdd4cd) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolderdd4cd = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolderdd4cd->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, '__get', ['name' => $name], $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        if (isset(self::$publicProperties771d1[$name])) {
            return $this->valueHolderdd4cd->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderdd4cd;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderdd4cd;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderdd4cd;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderdd4cd;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, '__isset', array('name' => $name), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderdd4cd;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderdd4cd;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, '__unset', array('name' => $name), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderdd4cd;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderdd4cd;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, '__clone', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        $this->valueHolderdd4cd = clone $this->valueHolderdd4cd;
    }

    public function __sleep()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, '__sleep', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;

        return array('valueHolderdd4cd');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerfcc8d = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerfcc8d;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'initializeProxy', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderdd4cd;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderdd4cd;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
