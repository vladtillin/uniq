<?php

namespace ContainerAky3rNp;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_FPmWZfoService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.fPmWZfo' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.fPmWZfo'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'googleAuthenticatorManager' => ['privates', 'App\\Manager\\User\\Security\\GoogleAuthenticatorManager', 'getGoogleAuthenticatorManagerService', true],
            'googleAuthenticatorService' => ['privates', 'App\\Service\\Secure\\TwoFactorAuthentication\\GoogleAuthenticatorService', 'getGoogleAuthenticatorServiceService', true],
        ], [
            'googleAuthenticatorManager' => 'App\\Manager\\User\\Security\\GoogleAuthenticatorManager',
            'googleAuthenticatorService' => 'App\\Service\\Secure\\TwoFactorAuthentication\\GoogleAuthenticatorService',
        ]);
    }
}
