<?php

namespace ContainerAky3rNp;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getSettingsControllerService extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\App\Settings\SettingsController' shared autowired service.
     *
     * @return \App\Controller\App\Settings\SettingsController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/App/Settings/SettingsController.php';

        $container->services['App\\Controller\\App\\Settings\\SettingsController'] = $instance = new \App\Controller\App\Settings\SettingsController();

        $instance->setContainer(($container->privates['.service_locator.61_2oPg'] ?? $container->load('get_ServiceLocator_612oPgService'))->withContext('App\\Controller\\App\\Settings\\SettingsController', $container));

        return $instance;
    }
}
