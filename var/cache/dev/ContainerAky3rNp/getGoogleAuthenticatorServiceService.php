<?php

namespace ContainerAky3rNp;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getGoogleAuthenticatorServiceService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Service\Secure\TwoFactorAuthentication\GoogleAuthenticatorService' shared autowired service.
     *
     * @return \App\Service\Secure\TwoFactorAuthentication\GoogleAuthenticatorService
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/Service/Secure/TwoFactorAuthentication/GoogleAuthenticatorService.php';

        return $container->privates['App\\Service\\Secure\\TwoFactorAuthentication\\GoogleAuthenticatorService'] = new \App\Service\Secure\TwoFactorAuthentication\GoogleAuthenticatorService();
    }
}
