<?php

namespace ContainerAky3rNp;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getMinioServiceService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Service\Minio\MinioService' shared autowired service.
     *
     * @return \App\Service\Minio\MinioService
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/Service/Minio/MinioService.php';

        return $container->privates['App\\Service\\Minio\\MinioService'] = new \App\Service\Minio\MinioService($container->getEnv('MINIO_ROOT_USER'), $container->getEnv('MINIO_ROOT_PASSWORD'), $container->getEnv('MINIO_ENDPOINT_URL'));
    }
}
