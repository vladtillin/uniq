<?php

namespace ContainerAky3rNp;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getGoogleAuthenticatorCrudControllerService extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\Admin\User\Security\GoogleAuthenticatorCrudController' shared autowired service.
     *
     * @return \App\Controller\Admin\User\Security\GoogleAuthenticatorCrudController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/vendor/easycorp/easyadmin-bundle/src/Contracts/Controller/CrudControllerInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/easycorp/easyadmin-bundle/src/Controller/AbstractCrudController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/Admin/User/Security/GoogleAuthenticatorCrudController.php';

        $container->services['App\\Controller\\Admin\\User\\Security\\GoogleAuthenticatorCrudController'] = $instance = new \App\Controller\Admin\User\Security\GoogleAuthenticatorCrudController(($container->privates['App\\Manager\\User\\Security\\GoogleAuthenticatorManager'] ?? $container->load('getGoogleAuthenticatorManagerService')), ($container->privates['App\\Manager\\Admin\\AdminLogManager'] ?? $container->load('getAdminLogManagerService')));

        $instance->setContainer(($container->privates['.service_locator.kOEw5YM'] ?? $container->load('get_ServiceLocator_KOEw5YMService'))->withContext('App\\Controller\\Admin\\User\\Security\\GoogleAuthenticatorCrudController', $container));

        return $instance;
    }
}
