<?php

namespace ContainerAky3rNp;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getUserManagerService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Manager\UserManager' shared autowired service.
     *
     * @return \App\Manager\UserManager
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/Manager/UserManager.php';

        return $container->privates['App\\Manager\\UserManager'] = new \App\Manager\UserManager(($container->privates['App\\Repository\\UserRepository'] ?? $container->load('getUserRepositoryService')), ($container->privates['security.user_password_hasher'] ?? $container->load('getSecurity_UserPasswordHasherService')));
    }
}
