<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing_pages/template.html.twig */
class __TwigTemplate_3e39f62fca4f61c80ced9ae98a9442f6c5981f557dbb3dcbbe3625999d663408 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'base_doctype_template' => [$this, 'block_base_doctype_template'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landing_pages/template.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landing_pages/template.html.twig"));

        // line 1
        $this->displayBlock('base_doctype_template', $context, $blocks);
        // line 14
        echo "<body>
";
        // line 16
        echo "<div class=\"template_wrapper\">
    <div class=\"template_header\">
        <a class=\"template_header--logo\">DoctorTillin</a>
    </div>
    <div class=\"template_content\">
        <button class=\"template_button--prev\" type=\"submit\" href=\"#\"><img src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo/button-prev.png"), "html", null, true);
        echo "\"
                                                                          alt=\"Back\" width=\"15px\" height=\"25px\">
        </button>

    </div>
";
        // line 27
        echo "
";
        // line 29
        echo "    <div class=\"template_footer--flex\">
        <div class=\"template_footer--col1\">
            <button class=\"template_button--footer\" type=\"submit\" href=\"#\"><img
                        src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo/ellipsis-circle-o.png"), "html", null, true);
        echo "\" alt=\"Info\" width=\"33px\" height=\"33px\">
            </button>
        </div>
        <div class=\"template_footer--col1\">
            <button class=\"template_button--footer\" type=\"submit\" href=\"#\"><img
                        src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo/comments-o.png"), "html", null, true);
        echo "\" alt=\"Comments\" width=\"33px\" height=\"33px\"></button>
        </div>
        <div class=\"template_footer--col1\">
            <button class=\"template_button--footer\" type=\"submit\" href=\"#\"><img src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo/cog-o.png"), "html", null, true);
        echo "\"
                                                                                alt=\"Settings\" width=\"33px\"
                                                                                height=\"33px\"></button>
        </div>
    </div>
";
        // line 46
        echo "</div>
</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 1
    public function block_base_doctype_template($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "base_doctype_template"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "base_doctype_template"));

        // line 2
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Template</title>
    <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/libraries/bootstrap/bootstrap.min.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/landing_pages/template.css"), "html", null, true);
        echo "\">
</head>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "landing_pages/template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 11,  126 => 10,  116 => 2,  106 => 1,  94 => 46,  86 => 40,  80 => 37,  72 => 32,  67 => 29,  64 => 27,  56 => 21,  49 => 16,  46 => 14,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block base_doctype_template %}
<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Template</title>
    <link rel=\"stylesheet\" href=\"{{ asset('css/libraries/bootstrap/bootstrap.min.css') }}\">
    <link rel=\"stylesheet\" href=\"{{ asset('css/landing_pages/template.css') }}\">
</head>
{% endblock %}
<body>
{#{% block template_page %}#}
<div class=\"template_wrapper\">
    <div class=\"template_header\">
        <a class=\"template_header--logo\">DoctorTillin</a>
    </div>
    <div class=\"template_content\">
        <button class=\"template_button--prev\" type=\"submit\" href=\"#\"><img src=\"{{ asset('img/logo/button-prev.png') }}\"
                                                                          alt=\"Back\" width=\"15px\" height=\"25px\">
        </button>

    </div>
{#    {% endblock %}#}

{#    {% block template_footer--buttons %}#}
    <div class=\"template_footer--flex\">
        <div class=\"template_footer--col1\">
            <button class=\"template_button--footer\" type=\"submit\" href=\"#\"><img
                        src=\"{{ asset('img/logo/ellipsis-circle-o.png') }}\" alt=\"Info\" width=\"33px\" height=\"33px\">
            </button>
        </div>
        <div class=\"template_footer--col1\">
            <button class=\"template_button--footer\" type=\"submit\" href=\"#\"><img
                        src=\"{{ asset('img/logo/comments-o.png') }}\" alt=\"Comments\" width=\"33px\" height=\"33px\"></button>
        </div>
        <div class=\"template_footer--col1\">
            <button class=\"template_button--footer\" type=\"submit\" href=\"#\"><img src=\"{{ asset('img/logo/cog-o.png') }}\"
                                                                                alt=\"Settings\" width=\"33px\"
                                                                                height=\"33px\"></button>
        </div>
    </div>
{#    {% endblock %}#}
</div>
</body>
</html>", "landing_pages/template.html.twig", "/doctor-tillin/templates/landing_pages/template.html.twig");
    }
}
