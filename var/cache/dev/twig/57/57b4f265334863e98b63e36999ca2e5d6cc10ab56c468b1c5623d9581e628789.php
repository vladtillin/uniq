<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* app/blocks/footer_menu--buttons.html.twig */
class __TwigTemplate_44db086adf2e63aea83b2a77f072fcfa14699dbcf0e8b6b5d0320331677d14fd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "app/blocks/footer_menu--buttons.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "app/blocks/footer_menu--buttons.html.twig"));

        // line 2
        echo "
<div class=\"template_footer--flex\">
    <div class=\"template_footer--col1\">
        <button class=\"template_button--footer\" type=\"submit\" href=\"#\"><img
                    src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo/ellipsis-circle-o.png"), "html", null, true);
        echo "\" alt=\"Info\" width=\"33px\" height=\"33px\">
        </button>
    </div>
    <div class=\"template_footer--col1\">
        <button class=\"template_button--footer\" type=\"submit\" href=\"#\"><img
                    src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo/comments-o.png"), "html", null, true);
        echo "\" alt=\"Comments\" width=\"33px\" height=\"33px\"></button>
    </div>
    <div class=\"template_footer--col1\">
        <button class=\"template_button--footer\" type=\"submit\" href=\"#\"><img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo/cog-o.png"), "html", null, true);
        echo "\"
                                                                            alt=\"Settings\" width=\"33px\"
                                                                            height=\"33px\"></button>
    </div>
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "app/blocks/footer_menu--buttons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 14,  57 => 11,  49 => 6,  43 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{#{% extends 'landing_pages/registration.html.twig' %}#}

<div class=\"template_footer--flex\">
    <div class=\"template_footer--col1\">
        <button class=\"template_button--footer\" type=\"submit\" href=\"#\"><img
                    src=\"{{ asset('img/logo/ellipsis-circle-o.png') }}\" alt=\"Info\" width=\"33px\" height=\"33px\">
        </button>
    </div>
    <div class=\"template_footer--col1\">
        <button class=\"template_button--footer\" type=\"submit\" href=\"#\"><img
                    src=\"{{ asset('img/logo/comments-o.png') }}\" alt=\"Comments\" width=\"33px\" height=\"33px\"></button>
    </div>
    <div class=\"template_footer--col1\">
        <button class=\"template_button--footer\" type=\"submit\" href=\"#\"><img src=\"{{ asset('img/logo/cog-o.png') }}\"
                                                                            alt=\"Settings\" width=\"33px\"
                                                                            height=\"33px\"></button>
    </div>
</div>
", "app/blocks/footer_menu--buttons.html.twig", "/doctor-tillin/templates/app/blocks/footer_menu--buttons.html.twig");
    }
}
