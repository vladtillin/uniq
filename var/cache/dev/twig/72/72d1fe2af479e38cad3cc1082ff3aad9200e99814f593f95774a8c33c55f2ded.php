<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing_pages/registration.html.twig */
class __TwigTemplate_4e85ec249db7fb8c38a81e16e5b86b93d586ab9eb411da2e41e5c60a2428d8fb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landing_pages/registration.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landing_pages/registration.html.twig"));

        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Template</title>
    <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/libraries/bootstrap/bootstrap.min.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/landing_pages/template.css"), "html", null, true);
        echo "\">
</head>
<body>
<div class=\"template_content\">
    ";
        // line 14
        $this->loadTemplate("app/blocks/template_header.html.twig", "landing_pages/registration.html.twig", 14)->display($context);
        // line 15
        echo "    <div class=\"block-who-are-you\">
        <h1 class=\"text-who-are-you\">Who are <br> you?</h1>
    </div><div class=\"block-choose-user\">
        <div class=\"block-choose-user--shadow\">
            <button class=\"button-choose-user\" type=\"submit\" href=\"#\"><img alt=\"Choose doctor\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo/doctor.png"), "html", null, true);
        echo "\" width=\"85px\" height=\"100px\"></button>
        </div>
        <div class=\"block-choose-user--shadow\">
            <button class=\"button-choose-user\" type=\"submit\" href=\"#\"><img alt=\"Choose patient\" src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo/patient.png"), "html", null, true);
        echo "\" width=\"85px\" height=\"100px\"></button>
        </div>
    </div>
</div>
</body>
<footer>
    <div class=\"footer_content\">
        ";
        // line 29
        $this->loadTemplate("app/blocks/footer_menu--urls.html.twig", "landing_pages/registration.html.twig", 29)->display($context);
        // line 30
        echo "    </div>
</footer>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "landing_pages/registration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 30,  88 => 29,  78 => 22,  72 => 19,  66 => 15,  64 => 14,  57 => 10,  53 => 9,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Template</title>
    <link rel=\"stylesheet\" href=\"{{ asset('css/libraries/bootstrap/bootstrap.min.css') }}\">
    <link rel=\"stylesheet\" href=\"{{ asset('css/landing_pages/template.css') }}\">
</head>
<body>
<div class=\"template_content\">
    {% include \"app/blocks/template_header.html.twig\" %}
    <div class=\"block-who-are-you\">
        <h1 class=\"text-who-are-you\">Who are <br> you?</h1>
    </div><div class=\"block-choose-user\">
        <div class=\"block-choose-user--shadow\">
            <button class=\"button-choose-user\" type=\"submit\" href=\"#\"><img alt=\"Choose doctor\" src=\"{{ asset('img/logo/doctor.png') }}\" width=\"85px\" height=\"100px\"></button>
        </div>
        <div class=\"block-choose-user--shadow\">
            <button class=\"button-choose-user\" type=\"submit\" href=\"#\"><img alt=\"Choose patient\" src=\"{{asset ('img/logo/patient.png')}}\" width=\"85px\" height=\"100px\"></button>
        </div>
    </div>
</div>
</body>
<footer>
    <div class=\"footer_content\">
        {% include \"app/blocks/footer_menu--urls.html.twig\" %}
    </div>
</footer>", "landing_pages/registration.html.twig", "/doctor-tillin/templates/landing_pages/registration.html.twig");
    }
}
