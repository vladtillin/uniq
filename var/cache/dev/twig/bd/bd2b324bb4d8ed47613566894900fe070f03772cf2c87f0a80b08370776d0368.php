<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing_pages/auth.html.twig */
class __TwigTemplate_fb313adc5406a4484de90aee8cb1fecf15ff96f8c301c322953d843fb99bf439 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landing_pages/auth.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landing_pages/auth.html.twig"));

        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\SEO\TitleExtension']->resolveTitle("landing_pages/auth.html.twig"), "html", null, true);
        echo "</title>
    <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/libraries/bootstrap/bootstrap.min.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/landing_pages/auth.css"), "html", null, true);
        echo "\">
</head>
<body>
<div class=\"container\">
    <div class=\"row d-flex justify-content-center\">
        <div class=\"col-lg-6 col-md-6\">
            <form method=\"post\">
                ";
        // line 17
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 17, $this->source); })())) {
            // line 18
            echo "                    <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 18, $this->source); })()), "messageKey", [], "any", false, false, false, 18), twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 18, $this->source); })()), "messageData", [], "any", false, false, false, 18), "security"), "html", null, true);
            echo "</div>
                ";
        }
        // line 20
        echo "
                <label for=\"inputEmail\">Никнейм</label>
                <input type=\"text\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new RuntimeError('Variable "last_username" does not exist.', 22, $this->source); })()), "html", null, true);
        echo "\" name=\"email\" id=\"inputEmail\" class=\"form-control\"
                       autocomplete=\"nickname\" required autofocus>

                <label for=\"inputPassword\">Пароль</label>

                <div class=\"input-group mb-3\">
                    <input type=\"password\" name=\"password\" id=\"inputPassword\" class=\"form-control\"
                           autocomplete=\"current-password\"
                           required>
                    <div class=\"input-group-append\">
                        <button type=\"button\" id=\"auth-show-hide-password\">
                            <svg width=\"20\" height=\"12\" viewBox=\"0 0 20 12\" fill=\"none\"
                                 xmlns=\"http://www.w3.org/2000/svg\">
                                <path d=\"M10 12C5.51049 12 0 8.09276 0 6C0 3.90724 5.51049 0 10 0C14.4895 0 20 3.90724 20 6C20 8.09276 14.4895 12 10 12ZM1.92582 6.46234C2.4058 6.99398 3.06532 7.57615 3.81317 8.10641C5.71956 9.45814 7.87506 10.2774 10 10.2774C12.1249 10.2774 14.2804 9.45814 16.1868 8.10641C16.9347 7.57615 17.5942 6.99398 18.0742 6.46234C18.2291 6.29078 18.3571 6.13295 18.4518 6C18.3571 5.86705 18.2291 5.70922 18.0742 5.53766C17.5942 5.00602 16.9347 4.42385 16.1868 3.89359C14.2804 2.54186 12.1249 1.72256 10 1.72256C7.87506 1.72256 5.71956 2.54186 3.81317 3.89359C3.06532 4.42385 2.4058 5.00602 1.92582 5.53766C1.77093 5.70922 1.64286 5.86705 1.54819 6C1.64286 6.13295 1.77093 6.29078 1.92582 6.46234ZM10 9.5C8.067 9.5 6.5 7.933 6.5 6C6.5 4.067 8.067 2.5 10 2.5C11.933 2.5 13.5 4.067 13.5 6C13.5 7.933 11.933 9.5 10 9.5ZM10 7.7C10.9389 7.7 11.7 6.93888 11.7 6C11.7 5.06112 10.9389 4.3 10 4.3C9.06112 4.3 8.3 5.06112 8.3 6C8.3 6.93888 9.06112 7.7 10 7.7Z\"
                                      fill=\"#1C2E45\" fill-opacity=\"0.6\"/>
                            </svg>
                        </button>
                    </div>
                </div>

                <input type=\"hidden\" name=\"_csrf_token\"
                       value=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\"
                >

                <div class=\"checkbox mb-3\">
                    <label>
                        <input type=\"checkbox\" name=\"_remember_me\"> Запомнить меня
                    </label>
                </div>

                <div class=\"d-grid gap-2 col-6 mx-auto\">
                    <button class=\"btn btn-lg btn-primary\" type=\"submit\">
                        Войти
                    </button>
                </div>
            </form>
            <br>
            <div class=\"mb-3\">
                <p class=\"text-center\"><a href=\"#\">Забыли пароль ?</a></p>
                <p class=\"text-center\"><a href=\"";
        // line 61
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_security_user_registration");
        echo "\">Регистрация</a></p>
            </div>
        </div>
    </div>
</div>

<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js\"
        integrity=\"sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13\"
        crossorigin=\"anonymous\"></script>
<script src=\"https://code.jquery.com/jquery-3.6.0.js\" integrity=\"sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=\"
        crossorigin=\"anonymous\"></script>
<script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/landing_pages/auth.js"), "html", null, true);
        echo "\"></script>
</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "landing_pages/auth.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 72,  127 => 61,  106 => 43,  82 => 22,  78 => 20,  72 => 18,  70 => 17,  60 => 10,  56 => 9,  52 => 8,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>{{ title('landing_pages/auth.html.twig') }}</title>
    <link rel=\"stylesheet\" href=\"{{ asset('css/libraries/bootstrap/bootstrap.min.css') }}\">
    <link rel=\"stylesheet\" href=\"{{ asset('css/landing_pages/auth.css') }}\">
</head>
<body>
<div class=\"container\">
    <div class=\"row d-flex justify-content-center\">
        <div class=\"col-lg-6 col-md-6\">
            <form method=\"post\">
                {% if error %}
                    <div class=\"alert alert-danger\">{{ error.messageKey|trans(error.messageData, 'security') }}</div>
                {% endif %}

                <label for=\"inputEmail\">Никнейм</label>
                <input type=\"text\" value=\"{{ last_username }}\" name=\"email\" id=\"inputEmail\" class=\"form-control\"
                       autocomplete=\"nickname\" required autofocus>

                <label for=\"inputPassword\">Пароль</label>

                <div class=\"input-group mb-3\">
                    <input type=\"password\" name=\"password\" id=\"inputPassword\" class=\"form-control\"
                           autocomplete=\"current-password\"
                           required>
                    <div class=\"input-group-append\">
                        <button type=\"button\" id=\"auth-show-hide-password\">
                            <svg width=\"20\" height=\"12\" viewBox=\"0 0 20 12\" fill=\"none\"
                                 xmlns=\"http://www.w3.org/2000/svg\">
                                <path d=\"M10 12C5.51049 12 0 8.09276 0 6C0 3.90724 5.51049 0 10 0C14.4895 0 20 3.90724 20 6C20 8.09276 14.4895 12 10 12ZM1.92582 6.46234C2.4058 6.99398 3.06532 7.57615 3.81317 8.10641C5.71956 9.45814 7.87506 10.2774 10 10.2774C12.1249 10.2774 14.2804 9.45814 16.1868 8.10641C16.9347 7.57615 17.5942 6.99398 18.0742 6.46234C18.2291 6.29078 18.3571 6.13295 18.4518 6C18.3571 5.86705 18.2291 5.70922 18.0742 5.53766C17.5942 5.00602 16.9347 4.42385 16.1868 3.89359C14.2804 2.54186 12.1249 1.72256 10 1.72256C7.87506 1.72256 5.71956 2.54186 3.81317 3.89359C3.06532 4.42385 2.4058 5.00602 1.92582 5.53766C1.77093 5.70922 1.64286 5.86705 1.54819 6C1.64286 6.13295 1.77093 6.29078 1.92582 6.46234ZM10 9.5C8.067 9.5 6.5 7.933 6.5 6C6.5 4.067 8.067 2.5 10 2.5C11.933 2.5 13.5 4.067 13.5 6C13.5 7.933 11.933 9.5 10 9.5ZM10 7.7C10.9389 7.7 11.7 6.93888 11.7 6C11.7 5.06112 10.9389 4.3 10 4.3C9.06112 4.3 8.3 5.06112 8.3 6C8.3 6.93888 9.06112 7.7 10 7.7Z\"
                                      fill=\"#1C2E45\" fill-opacity=\"0.6\"/>
                            </svg>
                        </button>
                    </div>
                </div>

                <input type=\"hidden\" name=\"_csrf_token\"
                       value=\"{{ csrf_token('authenticate') }}\"
                >

                <div class=\"checkbox mb-3\">
                    <label>
                        <input type=\"checkbox\" name=\"_remember_me\"> Запомнить меня
                    </label>
                </div>

                <div class=\"d-grid gap-2 col-6 mx-auto\">
                    <button class=\"btn btn-lg btn-primary\" type=\"submit\">
                        Войти
                    </button>
                </div>
            </form>
            <br>
            <div class=\"mb-3\">
                <p class=\"text-center\"><a href=\"#\">Забыли пароль ?</a></p>
                <p class=\"text-center\"><a href=\"{{ path('user_security_user_registration') }}\">Регистрация</a></p>
            </div>
        </div>
    </div>
</div>

<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js\"
        integrity=\"sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13\"
        crossorigin=\"anonymous\"></script>
<script src=\"https://code.jquery.com/jquery-3.6.0.js\" integrity=\"sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=\"
        crossorigin=\"anonymous\"></script>
<script src=\"{{ asset('js/landing_pages/auth.js') }}\"></script>
</body>
</html>", "landing_pages/auth.html.twig", "/doctor-tillin/templates/landing_pages/auth.html.twig");
    }
}
