<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing_pages/patient_registration.html.twig */
class __TwigTemplate_a71fff9183d380f07336781016104211f486c7b3684d7994f042422e18968934 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landing_pages/patient_registration.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landing_pages/patient_registration.html.twig"));

        // line 1
        echo "<!doctype html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\SEO\TitleExtension']->resolveTitle("landing_pages/patient_registration.html.twig"), "html", null, true);
        echo "</title>
    <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/landing_pages/registration.css"), "html", null, true);
        echo "\">
</head>
<body>
";
        // line 12
        $this->loadTemplate("app/blocks/template_header-with-text.html.twig", "landing_pages/patient_registration.html.twig", 12)->display($context);
        // line 13
        echo "<div class='form-wrapper'>
    <div class=\"choose-email-or-phone\">
        <div class=\"email-on--active text-opacity-choose-active\">
            <a>Email</a>
        </div>
        <div class=\"phone-disable text-opacity-choose\">
            <a>Номер телефона</a>
        </div>
    </div>
    <div class=\"tab-1\">
        <div class=\"form_start-nameAndSurnameRegistrationForm\">
            <div class=\"form-name\">
                ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 25, $this->source); })()), "name", [], "any", false, false, false, 25), 'errors');
        echo "
                ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 26, $this->source); })()), "name", [], "any", false, false, false, 26), 'label');
        echo "
                ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 27, $this->source); })()), "name", [], "any", false, false, false, 27), 'widget', ["attr" => ["class" => "form-name-and-surname--field"]]);
        echo "
            </div>
            <div class=\"form-surname\">
                ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 30, $this->source); })()), "surname", [], "any", false, false, false, 30), 'errors');
        echo "
                ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 31, $this->source); })()), "surname", [], "any", false, false, false, 31), 'label');
        echo "
                ";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 32, $this->source); })()), "surname", [], "any", false, false, false, 32), 'widget', ["attr" => ["class" => "form-name-and-surname--field"]]);
        echo "
            </div>
        </div>
        <div class=\"form_start-Nickname\">
            ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 36, $this->source); })()), "nickname", [], "any", false, false, false, 36), 'errors');
        echo "
            ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 37, $this->source); })()), "nickname", [], "any", false, false, false, 37), 'label');
        echo "
            ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 38, $this->source); })()), "nickname", [], "any", false, false, false, 38), 'widget', ["attr" => ["class" => "form-full-width"]]);
        echo "
        </div>
        <div class=\"form_start-emailRegistrationForm\">
            ";
        // line 41
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 41, $this->source); })()), 'form_start');
        echo "

            ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 43, $this->source); })()), "email", [], "any", false, false, false, 43), 'errors');
        echo "
            ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 44, $this->source); })()), "email", [], "any", false, false, false, 44), 'label');
        echo "
            ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 45, $this->source); })()), "email", [], "any", false, false, false, 45), 'widget', ["attr" => ["class" => "form-full-width"]]);
        echo "
        </div>
        <div class=\"form_start-Password\">
            ";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 48, $this->source); })()), "password", [], "any", false, false, false, 48), 'errors');
        echo "
            ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 49, $this->source); })()), "password", [], "any", false, false, false, 49), 'label');
        echo "
            ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 50, $this->source); })()), "password", [], "any", false, false, false, 50), 'widget', ["attr" => ["class" => "form-full-width"]]);
        echo "
        </div>
        <div class=\"checkbox-wrapper-field\">
            <div class=\"checkbox-button\">
                <label class=\"checkbox-button-center\">
                    <input class=\"checkbox-button-sf\" type=\"checkbox\" name=\"notice\">
                </label>
            </div>
            <div class=\"checkbox-notice\">
                <p class=\"checkbox-notice--font\">Я соглашаюсь с политикой конфиденциальности и публичной оффертой</p>
            </div>
        </div>
        <div class=\"email-notice\">
            <h1 class=\"email--no-change-notice\">Внимательно заполняйте данные, так как email и номер телефона нельзя менять!</h1>
        </div>
        ";
        // line 65
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 65, $this->source); })()), "submit", [], "any", false, false, false, 65), 'widget', ["attr" => ["class" => "form-button"]]);
        echo "

        ";
        // line 67
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 67, $this->source); })()), 'form_end');
        echo "
    </div>
    <div class=\"tab-2\">
        <div class=\"form_start-nameAndSurnameRegistrationForm\">
            ";
        // line 71
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 71, $this->source); })()), 'form_start');
        echo "
            <div class=\"form-name\">
                ";
        // line 73
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 73, $this->source); })()), "name", [], "any", false, false, false, 73), 'errors');
        echo "
                ";
        // line 74
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 74, $this->source); })()), "name", [], "any", false, false, false, 74), 'label');
        echo "
                ";
        // line 75
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 75, $this->source); })()), "name", [], "any", false, false, false, 75), 'widget', ["attr" => ["class" => "form-name-and-surname--field"]]);
        echo "
            </div>
            <div class=\"form-surname\">
                ";
        // line 78
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 78, $this->source); })()), "surname", [], "any", false, false, false, 78), 'errors');
        echo "
                ";
        // line 79
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 79, $this->source); })()), "surname", [], "any", false, false, false, 79), 'label');
        echo "
                ";
        // line 80
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 80, $this->source); })()), "surname", [], "any", false, false, false, 80), 'widget', ["attr" => ["class" => "form-name-and-surname--field"]]);
        echo "
            </div>
        </div>
        <div class=\"form_start-Nickname\">
            ";
        // line 84
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 84, $this->source); })()), "nickname", [], "any", false, false, false, 84), 'errors');
        echo "
            ";
        // line 85
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 85, $this->source); })()), "nickname", [], "any", false, false, false, 85), 'label');
        echo "
            ";
        // line 86
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 86, $this->source); })()), "nickname", [], "any", false, false, false, 86), 'widget', ["attr" => ["class" => "form-full-width"]]);
        echo "
        </div>
        <div class=\"form_start-emailRegistrationForm\">
            ";
        // line 89
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 89, $this->source); })()), "phoneNumber", [], "any", false, false, false, 89), 'errors');
        echo "
            ";
        // line 90
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 90, $this->source); })()), "phoneNumber", [], "any", false, false, false, 90), 'label');
        echo "
            ";
        // line 91
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 91, $this->source); })()), "phoneNumber", [], "any", false, false, false, 91), 'widget', ["attr" => ["class" => "form-full-width"]]);
        echo "
        </div>
        <div class=\"form_start-Password\">
            ";
        // line 94
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 94, $this->source); })()), "password", [], "any", false, false, false, 94), 'errors');
        echo "
            ";
        // line 95
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 95, $this->source); })()), "password", [], "any", false, false, false, 95), 'label');
        echo "
            ";
        // line 96
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 96, $this->source); })()), "password", [], "any", false, false, false, 96), 'widget', ["attr" => ["class" => "form-full-width"]]);
        echo "
        </div>
        <div class=\"checkbox-wrapper-field\">
            <div class=\"checkbox-button\">
                <label class=\"checkbox-button-center\">
                    <input class=\"checkbox-button-sf\" type=\"checkbox\" name=\"notice\">
                </label>
            </div>
            <div class=\"checkbox-notice\">
                <p class=\"checkbox-notice--font\">Я соглашаюсь с политикой конфиденциальности и публичной оффертой</p>
            </div>
        </div>
        <div class=\"email-notice\">
            <h1 class=\"email--no-change-notice\">Внимательно заполняйте данные, так как email и номер телефона нельзя
                менять!</h1>
        </div>
        ";
        // line 112
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 112, $this->source); })()), "submit", [], "any", false, false, false, 112), 'widget', ["attr" => ["class" => "form-button"]]);
        echo "

        ";
        // line 114
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 114, $this->source); })()), 'form_end');
        echo "

    </div>
</div>
<footer>
    ";
        // line 119
        $this->loadTemplate("app/blocks/footer_menu--urls.html.twig", "landing_pages/patient_registration.html.twig", 119)->display($context);
        // line 120
        echo "</footer>
</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "landing_pages/patient_registration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  282 => 120,  280 => 119,  272 => 114,  267 => 112,  248 => 96,  244 => 95,  240 => 94,  234 => 91,  230 => 90,  226 => 89,  220 => 86,  216 => 85,  212 => 84,  205 => 80,  201 => 79,  197 => 78,  191 => 75,  187 => 74,  183 => 73,  178 => 71,  171 => 67,  166 => 65,  148 => 50,  144 => 49,  140 => 48,  134 => 45,  130 => 44,  126 => 43,  121 => 41,  115 => 38,  111 => 37,  107 => 36,  100 => 32,  96 => 31,  92 => 30,  86 => 27,  82 => 26,  78 => 25,  64 => 13,  62 => 12,  56 => 9,  52 => 8,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!doctype html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>{{ title('landing_pages/patient_registration.html.twig') }}</title>
    <link rel=\"stylesheet\" href=\"{{ asset('css/landing_pages/registration.css') }}\">
</head>
<body>
{% include \"app/blocks/template_header-with-text.html.twig\" %}
<div class='form-wrapper'>
    <div class=\"choose-email-or-phone\">
        <div class=\"email-on--active text-opacity-choose-active\">
            <a>Email</a>
        </div>
        <div class=\"phone-disable text-opacity-choose\">
            <a>Номер телефона</a>
        </div>
    </div>
    <div class=\"tab-1\">
        <div class=\"form_start-nameAndSurnameRegistrationForm\">
            <div class=\"form-name\">
                {{ form_errors(emailRegistrationForm.name) }}
                {{ form_label(emailRegistrationForm.name) }}
                {{ form_widget(emailRegistrationForm.name , {'attr': {'class': 'form-name-and-surname--field'}}) }}
            </div>
            <div class=\"form-surname\">
                {{ form_errors(emailRegistrationForm.surname) }}
                {{ form_label(emailRegistrationForm.surname) }}
                {{ form_widget(emailRegistrationForm.surname , {'attr': {'class': 'form-name-and-surname--field'}}) }}
            </div>
        </div>
        <div class=\"form_start-Nickname\">
            {{ form_errors(emailRegistrationForm.nickname) }}
            {{ form_label(emailRegistrationForm.nickname) }}
            {{ form_widget(emailRegistrationForm.nickname , {'attr': {'class': 'form-full-width'}}) }}
        </div>
        <div class=\"form_start-emailRegistrationForm\">
            {{ form_start(emailRegistrationForm) }}

            {{ form_errors(emailRegistrationForm.email) }}
            {{ form_label(emailRegistrationForm.email) }}
            {{ form_widget(emailRegistrationForm.email , {'attr': {'class': 'form-full-width'}}) }}
        </div>
        <div class=\"form_start-Password\">
            {{ form_errors(emailRegistrationForm.password) }}
            {{ form_label(emailRegistrationForm.password) }}
            {{ form_widget(emailRegistrationForm.password , {'attr': {'class': 'form-full-width'}}) }}
        </div>
        <div class=\"checkbox-wrapper-field\">
            <div class=\"checkbox-button\">
                <label class=\"checkbox-button-center\">
                    <input class=\"checkbox-button-sf\" type=\"checkbox\" name=\"notice\">
                </label>
            </div>
            <div class=\"checkbox-notice\">
                <p class=\"checkbox-notice--font\">Я соглашаюсь с политикой конфиденциальности и публичной оффертой</p>
            </div>
        </div>
        <div class=\"email-notice\">
            <h1 class=\"email--no-change-notice\">Внимательно заполняйте данные, так как email и номер телефона нельзя менять!</h1>
        </div>
        {{ form_widget(emailRegistrationForm.submit ,  {'attr': {'class': 'form-button'}}) }}

        {{ form_end(emailRegistrationForm) }}
    </div>
    <div class=\"tab-2\">
        <div class=\"form_start-nameAndSurnameRegistrationForm\">
            {{ form_start(phoneNumberRegistrationForm) }}
            <div class=\"form-name\">
                {{ form_errors(phoneNumberRegistrationForm.name) }}
                {{ form_label(phoneNumberRegistrationForm.name) }}
                {{ form_widget(phoneNumberRegistrationForm.name , {'attr': {'class': 'form-name-and-surname--field'}}) }}
            </div>
            <div class=\"form-surname\">
                {{ form_errors(phoneNumberRegistrationForm.surname) }}
                {{ form_label(phoneNumberRegistrationForm.surname) }}
                {{ form_widget(phoneNumberRegistrationForm.surname , {'attr': {'class': 'form-name-and-surname--field'}}) }}
            </div>
        </div>
        <div class=\"form_start-Nickname\">
            {{ form_errors(phoneNumberRegistrationForm.nickname) }}
            {{ form_label(phoneNumberRegistrationForm.nickname) }}
            {{ form_widget(phoneNumberRegistrationForm.nickname , {'attr': {'class': 'form-full-width'}} ) }}
        </div>
        <div class=\"form_start-emailRegistrationForm\">
            {{ form_errors(phoneNumberRegistrationForm.phoneNumber) }}
            {{ form_label(phoneNumberRegistrationForm.phoneNumber) }}
            {{ form_widget(phoneNumberRegistrationForm.phoneNumber , {'attr': {'class': 'form-full-width'}}) }}
        </div>
        <div class=\"form_start-Password\">
            {{ form_errors(phoneNumberRegistrationForm.password) }}
            {{ form_label(phoneNumberRegistrationForm.password) }}
            {{ form_widget(phoneNumberRegistrationForm.password , {'attr': {'class': 'form-full-width'}}) }}
        </div>
        <div class=\"checkbox-wrapper-field\">
            <div class=\"checkbox-button\">
                <label class=\"checkbox-button-center\">
                    <input class=\"checkbox-button-sf\" type=\"checkbox\" name=\"notice\">
                </label>
            </div>
            <div class=\"checkbox-notice\">
                <p class=\"checkbox-notice--font\">Я соглашаюсь с политикой конфиденциальности и публичной оффертой</p>
            </div>
        </div>
        <div class=\"email-notice\">
            <h1 class=\"email--no-change-notice\">Внимательно заполняйте данные, так как email и номер телефона нельзя
                менять!</h1>
        </div>
        {{ form_widget(phoneNumberRegistrationForm.submit ,  {'attr': {'class': 'form-button'}}) }}

        {{ form_end(phoneNumberRegistrationForm) }}

    </div>
</div>
<footer>
    {% include \"app/blocks/footer_menu--urls.html.twig\" %}
</footer>
</body>
</html>
", "landing_pages/patient_registration.html.twig", "/doctor-tillin/templates/landing_pages/patient_registration.html.twig");
    }
}
