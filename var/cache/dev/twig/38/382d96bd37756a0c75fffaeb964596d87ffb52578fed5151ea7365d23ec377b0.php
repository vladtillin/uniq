<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* app/settings/verification/phone_number_verification.html.twig */
class __TwigTemplate_282d1ba571a3c66cdbea0d40ef167abf2341f111eaf00b06394198a221d07d9d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'css' => [$this, 'block_css'],
            'body' => [$this, 'block_body'],
            'scripts' => [$this, 'block_scripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "app/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "app/settings/verification/phone_number_verification.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "app/settings/verification/phone_number_verification.html.twig"));

        $this->parent = $this->loadTemplate("app/base.html.twig", "app/settings/verification/phone_number_verification.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\SEO\TitleExtension']->resolveTitle("app/settings/verification/phone_number_verification.html.twig"), "html", null, true);
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "css"));

        // line 6
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/libraries/intl-tel-input/intlTelInput.css"), "html", null, true);
        echo "\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "    <style>
        .iti__flag {
            background-image: url(\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/libraries/intl-tel-input/flags.png"), "html", null, true);
        echo "\");
        }

        @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) {
            .iti__flag {
                background-image: url(\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/libraries/intl-tel-input/flags@2x.png"), "html", null, true);
        echo "\");
            }
        }
    </style>
    <span id=\"phone-number-valid\"></span>
    <span id=\"phone-number-error\"></span>
    ";
        // line 23
        if (( !(null === twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\AppExtension']->getUser(), "phoneNumber", [], "any", false, false, false, 23)) && (twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\AppExtension']->getUser(), "phoneNumberVerified", [], "any", false, false, false, 23) == false))) {
            // line 24
            echo "        <p>Ваш номер привязан к Вашей записи! Чтобы верифицировать номер - <a
                    href=\"https://t.me/doctortillinbot\" target=\"_blank\">https://t.me/doctortillinbot</a></p>
    ";
        } elseif ((twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\AppExtension']->getUser(), "phoneNumberVerified", [], "any", false, false, false, 26) == true)) {
            // line 27
            echo "        <p>Ваш номер телефона верифицирован! Чтобы изменить номер - свяжитесь с администрацией сайта</p>
    ";
        }
        // line 29
        echo "    <form id=\"phone-number-verification-form\">
        ";
        // line 30
        if ((isset($context["userHasPhoneNumber"]) || array_key_exists("userHasPhoneNumber", $context) ? $context["userHasPhoneNumber"] : (function () { throw new RuntimeError('Variable "userHasPhoneNumber" does not exist.', 30, $this->source); })())) {
            // line 31
            echo "            <input type=\"tel\" name=\"phoneNumber\" value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\AppExtension']->getUser(), "getPhoneNumber", [], "method", false, false, false, 31), "html", null, true);
            echo "\" id=\"phone-number-field\"
                   disabled required>
        ";
        } else {
            // line 34
            echo "            <input type=\"tel\" name=\"phoneNumber\" value=\"\" id=\"phone-number-field\" required>
            <button type=\"submit\">Подтвердить</button>
        ";
        }
        // line 37
        echo "    </form>

    <input type=\"text\" minlength=\"6\" maxlength=\"6\" id=\"verification-code\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 42
    public function block_scripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "scripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "scripts"));

        // line 43
        echo "    <script src=\"https://code.jquery.com/jquery-3.6.0.js\"
            integrity=\"sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=\" crossorigin=\"anonymous\"></script>
    <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/libraries/int-tel-input/intlTelInput.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$('#verification-code').hide()
        var input = document.querySelector('#phone-number-field')
        var iti = window.intlTelInput(input, {
            initialCountry: \"auto\",
            geoIpLookup: function (success, failure) {
                \$.get(\"https://ipinfo.io\", function () {
                }, \"jsonp\").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : \"ua\";
                    success(countryCode);
                });
            },
            preferredCountries: [\"ua\", \"es\", 'us'],
            utilsScript: \"";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/libraries/int-tel-input/utils.js"), "html", null, true);
        echo "\",
            onlyCountries: [\"al\", \"ad\", \"at\", \"be\", \"ba\", \"bg\", \"hr\", \"cz\", \"dk\",
                \"ee\", \"fo\", \"fi\", \"fr\", \"de\", \"gi\", \"gr\", \"va\", \"hu\", \"is\", \"ie\", \"it\", \"lv\",
                \"li\", \"lt\", \"lu\", \"mk\", \"mt\", \"md\", \"mc\", \"me\", \"nl\", \"no\", \"pl\", \"pt\", \"ro\",
                \"sm\", \"rs\", \"sk\", \"si\", \"es\", \"se\", \"ch\", \"ua\", \"gb\", \"us\"],
            separateDialCode: true,
        });

        \$('#phone-number-verification-form').on('submit', function (e) {
            e.preventDefault()
            if (iti.isValidNumber()) {
                \$.ajax({
                    url: \"";
        // line 71
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_user_secure_check_phone_number_unique");
        echo "\",
                    type: \"POST\",
                    data: {phoneNumber: iti.getNumber()},
                    success: function (result) {
                        clearError()
                        \$.ajax({
                            url: \"";
        // line 77
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_user_secure_set_phone_number");
        echo "\",
                            type: \"POST\",
                            data: {phoneNumber: iti.getNumber(), country: iti.getSelectedCountryData().iso2},
                            success: function (result) {
                                clearError()
                                \$('#phone-number-field').prop('disabled', true)
                                \$('#phone-number-valid').html('Ваш номер привязан к Вашей записи! Чтобы верифицировать номер - <a href=\"https://t.me/doctortillinbot\" target=\"_blank\">https://t.me/doctortillinbot</a>')
                            },
                            error: function (error) {
                                console.log('error')
                                \$('#phone-number-error').html('Что-то пошло не так')
                            }
                        })
                    },
                    error: function (result) {
                        \$('#phone-number-error').html('Пользователь с таким номером уже зарегестрирован')
                    }
                })
            } else {
                \$('#phone-number-error').html('Неверный формат номера телефона')
            }
        })

        function clearError() {
            \$('#phone-number-error').html('')
        }
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "app/settings/verification/phone_number_verification.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 77,  223 => 71,  208 => 59,  191 => 45,  187 => 43,  177 => 42,  164 => 37,  159 => 34,  152 => 31,  150 => 30,  147 => 29,  143 => 27,  138 => 24,  136 => 23,  127 => 17,  119 => 12,  115 => 10,  105 => 9,  92 => 6,  82 => 5,  61 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'app/base.html.twig' %}

{% block title %} {{ title('app/settings/verification/phone_number_verification.html.twig') }} {% endblock %}

{% block css %}
    <link rel=\"stylesheet\" href=\"{{ asset('css/libraries/intl-tel-input/intlTelInput.css') }}\">
{% endblock %}

{% block body %}
    <style>
        .iti__flag {
            background-image: url(\"{{ asset('img/libraries/intl-tel-input/flags.png') }}\");
        }

        @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) {
            .iti__flag {
                background-image: url(\"{{ asset('img/libraries/intl-tel-input/flags@2x.png') }}\");
            }
        }
    </style>
    <span id=\"phone-number-valid\"></span>
    <span id=\"phone-number-error\"></span>
    {% if getUser().phoneNumber is not null and getUser().phoneNumberVerified == false %}
        <p>Ваш номер привязан к Вашей записи! Чтобы верифицировать номер - <a
                    href=\"https://t.me/doctortillinbot\" target=\"_blank\">https://t.me/doctortillinbot</a></p>
    {% elseif getUser().phoneNumberVerified == true %}
        <p>Ваш номер телефона верифицирован! Чтобы изменить номер - свяжитесь с администрацией сайта</p>
    {% endif %}
    <form id=\"phone-number-verification-form\">
        {% if userHasPhoneNumber %}
            <input type=\"tel\" name=\"phoneNumber\" value=\"{{ getUser().getPhoneNumber() }}\" id=\"phone-number-field\"
                   disabled required>
        {% else %}
            <input type=\"tel\" name=\"phoneNumber\" value=\"\" id=\"phone-number-field\" required>
            <button type=\"submit\">Подтвердить</button>
        {% endif %}
    </form>

    <input type=\"text\" minlength=\"6\" maxlength=\"6\" id=\"verification-code\">
{% endblock %}

{% block scripts %}
    <script src=\"https://code.jquery.com/jquery-3.6.0.js\"
            integrity=\"sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=\" crossorigin=\"anonymous\"></script>
    <script src=\"{{ asset('js/libraries/int-tel-input/intlTelInput.js') }}\"></script>
    <script>
        \$('#verification-code').hide()
        var input = document.querySelector('#phone-number-field')
        var iti = window.intlTelInput(input, {
            initialCountry: \"auto\",
            geoIpLookup: function (success, failure) {
                \$.get(\"https://ipinfo.io\", function () {
                }, \"jsonp\").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : \"ua\";
                    success(countryCode);
                });
            },
            preferredCountries: [\"ua\", \"es\", 'us'],
            utilsScript: \"{{ asset('js/libraries/int-tel-input/utils.js') }}\",
            onlyCountries: [\"al\", \"ad\", \"at\", \"be\", \"ba\", \"bg\", \"hr\", \"cz\", \"dk\",
                \"ee\", \"fo\", \"fi\", \"fr\", \"de\", \"gi\", \"gr\", \"va\", \"hu\", \"is\", \"ie\", \"it\", \"lv\",
                \"li\", \"lt\", \"lu\", \"mk\", \"mt\", \"md\", \"mc\", \"me\", \"nl\", \"no\", \"pl\", \"pt\", \"ro\",
                \"sm\", \"rs\", \"sk\", \"si\", \"es\", \"se\", \"ch\", \"ua\", \"gb\", \"us\"],
            separateDialCode: true,
        });

        \$('#phone-number-verification-form').on('submit', function (e) {
            e.preventDefault()
            if (iti.isValidNumber()) {
                \$.ajax({
                    url: \"{{ path('api_v1_user_secure_check_phone_number_unique') }}\",
                    type: \"POST\",
                    data: {phoneNumber: iti.getNumber()},
                    success: function (result) {
                        clearError()
                        \$.ajax({
                            url: \"{{ path('api_v1_user_secure_set_phone_number') }}\",
                            type: \"POST\",
                            data: {phoneNumber: iti.getNumber(), country: iti.getSelectedCountryData().iso2},
                            success: function (result) {
                                clearError()
                                \$('#phone-number-field').prop('disabled', true)
                                \$('#phone-number-valid').html('Ваш номер привязан к Вашей записи! Чтобы верифицировать номер - <a href=\"https://t.me/doctortillinbot\" target=\"_blank\">https://t.me/doctortillinbot</a>')
                            },
                            error: function (error) {
                                console.log('error')
                                \$('#phone-number-error').html('Что-то пошло не так')
                            }
                        })
                    },
                    error: function (result) {
                        \$('#phone-number-error').html('Пользователь с таким номером уже зарегестрирован')
                    }
                })
            } else {
                \$('#phone-number-error').html('Неверный формат номера телефона')
            }
        })

        function clearError() {
            \$('#phone-number-error').html('')
        }
    </script>
{% endblock %}
", "app/settings/verification/phone_number_verification.html.twig", "/doctor-tillin/templates/app/settings/verification/phone_number_verification.html.twig");
    }
}
