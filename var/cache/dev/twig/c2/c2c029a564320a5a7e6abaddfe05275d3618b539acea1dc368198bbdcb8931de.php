<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing_pages/doctor_registration.html.twig */
class __TwigTemplate_8e460ba3038739c34667a9dd32dc29fd5b9e4d37143e0017b65d67b6aac81676 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landing_pages/doctor_registration.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landing_pages/doctor_registration.html.twig"));

        // line 1
        echo "<!doctype html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\SEO\TitleExtension']->resolveTitle("landing_pages/doctor_registration.html.twig"), "html", null, true);
        echo "</title>
    <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/libraries/bootstrap/bootstrap.min.css"), "html", null, true);
        echo "\">
</head>
<body>
<div class=\"container\">
    <div class=\"row d-flex justify-content-center\">

        <div>
            <a href=\"#email\" id=\"emailTabButton\">Email</a>
            <a href=\"#phoneNumber\" id=\"phoneNumberTabButton\">Номер телефона</a>
        </div>

        <div class=\"col-lg-6 col-md-6\">
            ";
        // line 21
        if (($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 21, $this->source); })()), 'errors') != "")) {
            // line 22
            echo "                <div class=\"alert alert-danger\">";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 22, $this->source); })()), 'errors');
            echo "</div>
            ";
        }
        // line 24
        echo "            ";
        if ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 24, $this->source); })()), 'errors')) {
            // line 25
            echo "                <div class=\"alert alert-danger\">";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 25, $this->source); })()), 'errors');
            echo "</div>
            ";
        }
        // line 27
        echo "            <div class=\"tab-1\">
                ";
        // line 28
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 28, $this->source); })()), 'form_start');
        echo "

                <div class=\"form-group\">
                    ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 31, $this->source); })()), "email", [], "any", false, false, false, 31), 'errors');
        echo "
                    ";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 32, $this->source); })()), "email", [], "any", false, false, false, 32), 'label');
        echo "
                    ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 33, $this->source); })()), "email", [], "any", false, false, false, 33), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                </div>

                <div class=\"form-group\">
                    ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 37, $this->source); })()), "nickname", [], "any", false, false, false, 37), 'errors');
        echo "
                    ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 38, $this->source); })()), "nickname", [], "any", false, false, false, 38), 'label');
        echo "
                    ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 39, $this->source); })()), "nickname", [], "any", false, false, false, 39), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                </div>

                <div class=\"form-group\">
                    ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 43, $this->source); })()), "name", [], "any", false, false, false, 43), 'errors');
        echo "
                    ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 44, $this->source); })()), "name", [], "any", false, false, false, 44), 'label');
        echo "
                    ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 45, $this->source); })()), "name", [], "any", false, false, false, 45), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                </div>

                <div class=\"form-group\">
                    ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 49, $this->source); })()), "surname", [], "any", false, false, false, 49), 'errors');
        echo "
                    ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 50, $this->source); })()), "surname", [], "any", false, false, false, 50), 'label');
        echo "
                    ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 51, $this->source); })()), "surname", [], "any", false, false, false, 51), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                </div>

                <div class=\"form-group\">
                    ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 55, $this->source); })()), "password", [], "any", false, false, false, 55), 'errors');
        echo "
                    ";
        // line 56
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 56, $this->source); })()), "password", [], "any", false, false, false, 56), 'label');
        echo "
                    ";
        // line 57
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 57, $this->source); })()), "password", [], "any", false, false, false, 57), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                </div>

                <div class=\"form-group d-grid gap-2 col-6 mx-auto\">
                    ";
        // line 61
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 61, $this->source); })()), "submit", [], "any", false, false, false, 61), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary"]]);
        echo "
                </div>

                ";
        // line 64
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["emailRegistrationForm"]) || array_key_exists("emailRegistrationForm", $context) ? $context["emailRegistrationForm"] : (function () { throw new RuntimeError('Variable "emailRegistrationForm" does not exist.', 64, $this->source); })()), 'form_end');
        echo "
            </div>
            <div class=\"tab-2\">
                ";
        // line 67
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 67, $this->source); })()), 'form_start');
        echo "

                <div class=\"form-group\">
                    ";
        // line 70
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 70, $this->source); })()), "phoneNumber", [], "any", false, false, false, 70), 'errors');
        echo "
                    ";
        // line 71
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 71, $this->source); })()), "phoneNumber", [], "any", false, false, false, 71), 'label');
        echo "
                    ";
        // line 72
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 72, $this->source); })()), "phoneNumber", [], "any", false, false, false, 72), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                </div>

                <div class=\"form-group\">
                    ";
        // line 76
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 76, $this->source); })()), "nickname", [], "any", false, false, false, 76), 'errors');
        echo "
                    ";
        // line 77
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 77, $this->source); })()), "nickname", [], "any", false, false, false, 77), 'label');
        echo "
                    ";
        // line 78
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 78, $this->source); })()), "nickname", [], "any", false, false, false, 78), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                </div>

                <div class=\"form-group\">
                    ";
        // line 82
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 82, $this->source); })()), "name", [], "any", false, false, false, 82), 'errors');
        echo "
                    ";
        // line 83
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 83, $this->source); })()), "name", [], "any", false, false, false, 83), 'label');
        echo "
                    ";
        // line 84
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 84, $this->source); })()), "name", [], "any", false, false, false, 84), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                </div>

                <div class=\"form-group\">
                    ";
        // line 88
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 88, $this->source); })()), "surname", [], "any", false, false, false, 88), 'errors');
        echo "
                    ";
        // line 89
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 89, $this->source); })()), "surname", [], "any", false, false, false, 89), 'label');
        echo "
                    ";
        // line 90
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 90, $this->source); })()), "surname", [], "any", false, false, false, 90), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                </div>

                <div class=\"form-group\">
                    ";
        // line 94
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 94, $this->source); })()), "password", [], "any", false, false, false, 94), 'errors');
        echo "
                    ";
        // line 95
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 95, $this->source); })()), "password", [], "any", false, false, false, 95), 'label');
        echo "
                    ";
        // line 96
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 96, $this->source); })()), "password", [], "any", false, false, false, 96), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                </div>

                <div class=\"form-group d-grid gap-2 col-6 mx-auto\">
                    ";
        // line 100
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 100, $this->source); })()), "submit", [], "any", false, false, false, 100), 'widget', ["attr" => ["class" => "btn btn-lg btn-primary"]]);
        echo "
                </div>

                ";
        // line 103
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["phoneNumberRegistrationForm"]) || array_key_exists("phoneNumberRegistrationForm", $context) ? $context["phoneNumberRegistrationForm"] : (function () { throw new RuntimeError('Variable "phoneNumberRegistrationForm" does not exist.', 103, $this->source); })()), 'form_end');
        echo "
            </div>
        </div>
    </div>
</div>
<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js\"
        integrity=\"sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13\"
        crossorigin=\"anonymous\"></script>
<script src=\"https://code.jquery.com/jquery-3.6.0.js\" integrity=\"sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=\"
        crossorigin=\"anonymous\"></script>
<script>
    \$(document).ready(function () {
        \$('.tab-2').hide()

        \$(document).on('click', '#emailTabButton', function () {
            \$('.tab-2').hide();
            \$('.tab-1').show();
        })

        \$(document).on('click', '#phoneNumberTabButton', function () {
            \$('.tab-1').hide();
            \$('.tab-2').show();
        })
    })
</script>
</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "landing_pages/doctor_registration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  271 => 103,  265 => 100,  258 => 96,  254 => 95,  250 => 94,  243 => 90,  239 => 89,  235 => 88,  228 => 84,  224 => 83,  220 => 82,  213 => 78,  209 => 77,  205 => 76,  198 => 72,  194 => 71,  190 => 70,  184 => 67,  178 => 64,  172 => 61,  165 => 57,  161 => 56,  157 => 55,  150 => 51,  146 => 50,  142 => 49,  135 => 45,  131 => 44,  127 => 43,  120 => 39,  116 => 38,  112 => 37,  105 => 33,  101 => 32,  97 => 31,  91 => 28,  88 => 27,  82 => 25,  79 => 24,  73 => 22,  71 => 21,  56 => 9,  52 => 8,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!doctype html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>{{ title('landing_pages/doctor_registration.html.twig') }}</title>
    <link rel=\"stylesheet\" href=\"{{ asset('css/libraries/bootstrap/bootstrap.min.css') }}\">
</head>
<body>
<div class=\"container\">
    <div class=\"row d-flex justify-content-center\">

        <div>
            <a href=\"#email\" id=\"emailTabButton\">Email</a>
            <a href=\"#phoneNumber\" id=\"phoneNumberTabButton\">Номер телефона</a>
        </div>

        <div class=\"col-lg-6 col-md-6\">
            {% if form_errors(emailRegistrationForm) != '' %}
                <div class=\"alert alert-danger\">{{ form_errors(emailRegistrationForm) }}</div>
            {% endif %}
            {% if form_errors(phoneNumberRegistrationForm) %}
                <div class=\"alert alert-danger\">{{ form_errors(phoneNumberRegistrationForm) }}</div>
            {% endif %}
            <div class=\"tab-1\">
                {{ form_start(emailRegistrationForm) }}

                <div class=\"form-group\">
                    {{ form_errors(emailRegistrationForm.email) }}
                    {{ form_label(emailRegistrationForm.email) }}
                    {{ form_widget(emailRegistrationForm.email, {'attr': {'class': 'form-control'}}) }}
                </div>

                <div class=\"form-group\">
                    {{ form_errors(emailRegistrationForm.nickname) }}
                    {{ form_label(emailRegistrationForm.nickname) }}
                    {{ form_widget(emailRegistrationForm.nickname, {'attr': {'class': 'form-control'}}) }}
                </div>

                <div class=\"form-group\">
                    {{ form_errors(emailRegistrationForm.name) }}
                    {{ form_label(emailRegistrationForm.name) }}
                    {{ form_widget(emailRegistrationForm.name, {'attr': {'class': 'form-control'}}) }}
                </div>

                <div class=\"form-group\">
                    {{ form_errors(emailRegistrationForm.surname) }}
                    {{ form_label(emailRegistrationForm.surname) }}
                    {{ form_widget(emailRegistrationForm.surname, {'attr': {'class': 'form-control'}}) }}
                </div>

                <div class=\"form-group\">
                    {{ form_errors(emailRegistrationForm.password) }}
                    {{ form_label(emailRegistrationForm.password) }}
                    {{ form_widget(emailRegistrationForm.password, {'attr': {'class': 'form-control'}}) }}
                </div>

                <div class=\"form-group d-grid gap-2 col-6 mx-auto\">
                    {{ form_widget(emailRegistrationForm.submit, {'attr': {'class': 'btn btn-lg btn-primary'}}) }}
                </div>

                {{ form_end(emailRegistrationForm) }}
            </div>
            <div class=\"tab-2\">
                {{ form_start(phoneNumberRegistrationForm) }}

                <div class=\"form-group\">
                    {{ form_errors(phoneNumberRegistrationForm.phoneNumber) }}
                    {{ form_label(phoneNumberRegistrationForm.phoneNumber) }}
                    {{ form_widget(phoneNumberRegistrationForm.phoneNumber, {'attr': {'class': 'form-control'}}) }}
                </div>

                <div class=\"form-group\">
                    {{ form_errors(phoneNumberRegistrationForm.nickname) }}
                    {{ form_label(phoneNumberRegistrationForm.nickname) }}
                    {{ form_widget(phoneNumberRegistrationForm.nickname, {'attr': {'class': 'form-control'}}) }}
                </div>

                <div class=\"form-group\">
                    {{ form_errors(phoneNumberRegistrationForm.name) }}
                    {{ form_label(phoneNumberRegistrationForm.name) }}
                    {{ form_widget(phoneNumberRegistrationForm.name, {'attr': {'class': 'form-control'}}) }}
                </div>

                <div class=\"form-group\">
                    {{ form_errors(phoneNumberRegistrationForm.surname) }}
                    {{ form_label(phoneNumberRegistrationForm.surname) }}
                    {{ form_widget(phoneNumberRegistrationForm.surname, {'attr': {'class': 'form-control'}}) }}
                </div>

                <div class=\"form-group\">
                    {{ form_errors(phoneNumberRegistrationForm.password) }}
                    {{ form_label(phoneNumberRegistrationForm.password) }}
                    {{ form_widget(phoneNumberRegistrationForm.password, {'attr': {'class': 'form-control'}}) }}
                </div>

                <div class=\"form-group d-grid gap-2 col-6 mx-auto\">
                    {{ form_widget(phoneNumberRegistrationForm.submit, {'attr': {'class': 'btn btn-lg btn-primary'}}) }}
                </div>

                {{ form_end(phoneNumberRegistrationForm) }}
            </div>
        </div>
    </div>
</div>
<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js\"
        integrity=\"sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13\"
        crossorigin=\"anonymous\"></script>
<script src=\"https://code.jquery.com/jquery-3.6.0.js\" integrity=\"sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=\"
        crossorigin=\"anonymous\"></script>
<script>
    \$(document).ready(function () {
        \$('.tab-2').hide()

        \$(document).on('click', '#emailTabButton', function () {
            \$('.tab-2').hide();
            \$('.tab-1').show();
        })

        \$(document).on('click', '#phoneNumberTabButton', function () {
            \$('.tab-1').hide();
            \$('.tab-2').show();
        })
    })
</script>
</body>
</html>
", "landing_pages/doctor_registration.html.twig", "/doctor-tillin/templates/landing_pages/doctor_registration.html.twig");
    }
}
