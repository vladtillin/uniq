<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* app/settings/notifications_settings.html.twig */
class __TwigTemplate_29f0db3d5b7693f028cbe8ff75061c52bfec583607268c05dec88f60ff16b0a2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'scripts' => [$this, 'block_scripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "app/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "app/settings/notifications_settings.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "app/settings/notifications_settings.html.twig"));

        $this->parent = $this->loadTemplate("app/base.html.twig", "app/settings/notifications_settings.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\SEO\TitleExtension']->resolveTitle("app/settings/notifications_settings.html.twig"), "html", null, true);
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div>
        <label for=\"messagesNotifications\">Сообщения</label><input type=\"checkbox\" name=\"messagesNotifications\"
                                                                   id=\"messagesNotifications\">
        <label for=\"updatesNotifications\">Обновления</label><input type=\"checkbox\" name=\"updatesNotifications\"
                                                                   id=\"updatesNotifications\">
        <label for=\"advertisingNotifications\">Рекламные сообщения</label><input type=\"checkbox\"
                                                                                name=\"advertisingNotifications\"
                                                                                id=\"advertisingNotifications\">
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 17
    public function block_scripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "scripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "scripts"));

        // line 18
        echo "    <script src=\"https://code.jquery.com/jquery-3.6.0.js\"
            integrity=\"sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=\" crossorigin=\"anonymous\"></script>
    <script>
        \$(document).ready(function () {
            fillCheckboxes()
        })

        function fillCheckboxes() {
            \$.ajax({
                url: \"";
        // line 27
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_app_notifications_settings_get");
        echo "\",
                type: 'GET',
                success: function (data) {
                    notificationsSettingsDTO = JSON.parse(data)
                    \$('#messagesNotifications').prop(\"checked\", notificationsSettingsDTO.messagesNotifications)
                    \$('#updatesNotifications').prop('checked', notificationsSettingsDTO.updatesNotifications)
                    \$('#advertisingNotifications').prop('checked', notificationsSettingsDTO.advertisingNotifications)
                }, error: function (error) {
                    console.log(error)
                    alert('Something went wrong. Sorry! :(')
                }
            })
        }

        \$('#messagesNotifications').on('change', function () {
            \$.ajax({
                contentType: 'application/json',
                url: \"";
        // line 44
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_app_notifications_settings_post");
        echo "\",
                data: JSON.stringify({
                    \"messagesNotifications\": document.querySelector('#messagesNotifications').checked,
                    \"updatesNotifications\": document.querySelector('#updatesNotifications').checked,
                    \"advertisingNotifications\": document.querySelector('#advertisingNotifications').checked
                }),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    fillCheckboxes()
                }
            })
        })

        \$('#updatesNotifications').on('change', function () {
            \$.ajax({
                contentType: 'application/json',
                url: \"";
        // line 61
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_app_notifications_settings_post");
        echo "\",
                data: JSON.stringify({
                    \"messagesNotifications\": document.querySelector('#messagesNotifications').checked,
                    \"updatesNotifications\": document.querySelector('#updatesNotifications').checked,
                    \"advertisingNotifications\": document.querySelector('#advertisingNotifications').checked
                }),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    fillCheckboxes()
                }
            })
        })

        \$('#advertisingNotifications').on('change', function () {
            \$.ajax({
                contentType: 'application/json',
                url: \"";
        // line 78
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_app_notifications_settings_post");
        echo "\",
                data: JSON.stringify({
                    \"messagesNotifications\": document.querySelector('#messagesNotifications').checked,
                    \"updatesNotifications\": document.querySelector('#updatesNotifications').checked,
                    \"advertisingNotifications\": document.querySelector('#advertisingNotifications').checked
                }),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    fillCheckboxes()
                }
            })
        })
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "app/settings/notifications_settings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 78,  171 => 61,  151 => 44,  131 => 27,  120 => 18,  110 => 17,  91 => 6,  81 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'app/base.html.twig' %}

{% block title %} {{ title('app/settings/notifications_settings.html.twig') }} {% endblock %}

{% block body %}
    <div>
        <label for=\"messagesNotifications\">Сообщения</label><input type=\"checkbox\" name=\"messagesNotifications\"
                                                                   id=\"messagesNotifications\">
        <label for=\"updatesNotifications\">Обновления</label><input type=\"checkbox\" name=\"updatesNotifications\"
                                                                   id=\"updatesNotifications\">
        <label for=\"advertisingNotifications\">Рекламные сообщения</label><input type=\"checkbox\"
                                                                                name=\"advertisingNotifications\"
                                                                                id=\"advertisingNotifications\">
    </div>
{% endblock %}

{% block scripts %}
    <script src=\"https://code.jquery.com/jquery-3.6.0.js\"
            integrity=\"sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=\" crossorigin=\"anonymous\"></script>
    <script>
        \$(document).ready(function () {
            fillCheckboxes()
        })

        function fillCheckboxes() {
            \$.ajax({
                url: \"{{ path('api_v1_app_notifications_settings_get') }}\",
                type: 'GET',
                success: function (data) {
                    notificationsSettingsDTO = JSON.parse(data)
                    \$('#messagesNotifications').prop(\"checked\", notificationsSettingsDTO.messagesNotifications)
                    \$('#updatesNotifications').prop('checked', notificationsSettingsDTO.updatesNotifications)
                    \$('#advertisingNotifications').prop('checked', notificationsSettingsDTO.advertisingNotifications)
                }, error: function (error) {
                    console.log(error)
                    alert('Something went wrong. Sorry! :(')
                }
            })
        }

        \$('#messagesNotifications').on('change', function () {
            \$.ajax({
                contentType: 'application/json',
                url: \"{{ path('api_v1_app_notifications_settings_post') }}\",
                data: JSON.stringify({
                    \"messagesNotifications\": document.querySelector('#messagesNotifications').checked,
                    \"updatesNotifications\": document.querySelector('#updatesNotifications').checked,
                    \"advertisingNotifications\": document.querySelector('#advertisingNotifications').checked
                }),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    fillCheckboxes()
                }
            })
        })

        \$('#updatesNotifications').on('change', function () {
            \$.ajax({
                contentType: 'application/json',
                url: \"{{ path('api_v1_app_notifications_settings_post') }}\",
                data: JSON.stringify({
                    \"messagesNotifications\": document.querySelector('#messagesNotifications').checked,
                    \"updatesNotifications\": document.querySelector('#updatesNotifications').checked,
                    \"advertisingNotifications\": document.querySelector('#advertisingNotifications').checked
                }),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    fillCheckboxes()
                }
            })
        })

        \$('#advertisingNotifications').on('change', function () {
            \$.ajax({
                contentType: 'application/json',
                url: \"{{ path('api_v1_app_notifications_settings_post') }}\",
                data: JSON.stringify({
                    \"messagesNotifications\": document.querySelector('#messagesNotifications').checked,
                    \"updatesNotifications\": document.querySelector('#updatesNotifications').checked,
                    \"advertisingNotifications\": document.querySelector('#advertisingNotifications').checked
                }),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    fillCheckboxes()
                }
            })
        })
    </script>
{% endblock %}
", "app/settings/notifications_settings.html.twig", "/doctor-tillin/templates/app/settings/notifications_settings.html.twig");
    }
}
