<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* app/settings/verification/email_verification.html.twig */
class __TwigTemplate_37ecb257b65131dc38179f5c914da8dbc030a9188780105b04e3da5593e24f86 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'scripts' => [$this, 'block_scripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "app/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "app/settings/verification/email_verification.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "app/settings/verification/email_verification.html.twig"));

        $this->parent = $this->loadTemplate("app/base.html.twig", "app/settings/verification/email_verification.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\SEO\TitleExtension']->resolveTitle("app/settings/verification/email_verification.html.twig"), "html", null, true);
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <span id=\"verification-email-success\"></span>
    <span id=\"verification-email-error\"></span>
    ";
        // line 8
        if ((twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\AppExtension']->getUser(), "emailVerified", [], "any", false, false, false, 8) == true)) {
            // line 9
            echo "        <p>Ваш Email верифицирован! Чтобы изменить его - свяжитесь с администрацией сайта</p>
    ";
        }
        // line 11
        echo "    ";
        if ((isset($context["userHasEmail"]) || array_key_exists("userHasEmail", $context) ? $context["userHasEmail"] : (function () { throw new RuntimeError('Variable "userHasEmail" does not exist.', 11, $this->source); })())) {
            // line 12
            echo "        <input type=\"email\" name=\"email\" value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $this->extensions['App\Twig\AppExtension']->getUser(), "getEmail", [], "method", false, false, false, 12), "html", null, true);
            echo "\" id=\"email-field\" disabled required>
    ";
        } else {
            // line 14
            echo "        <input type=\"email\" name=\"email\" value=\"\" id=\"email-field\" required>
    ";
        }
        // line 16
        echo "    <button type=\"submit\" id=\"send-verification-email\">Отправить письмо</button>

    <form id=\"verify-verification-code\">
        <input type=\"text\" maxlength=\"6\" minlength=\"6\" id=\"verificationCode\">
    </form>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 24
    public function block_scripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "scripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "scripts"));

        // line 25
        echo "    <script src=\"https://code.jquery.com/jquery-3.6.0.js\"
            integrity=\"sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=\" crossorigin=\"anonymous\"></script>
    <script>
        \$('#verificationCode').hide()

        \$('#send-verification-email').on('click', function (e) {
            e.preventDefault()
            const email = \$('#email-field').val()

            if (\"";
        // line 34
        echo twig_escape_filter($this->env, (isset($context["userHasEmail"]) || array_key_exists("userHasEmail", $context) ? $context["userHasEmail"] : (function () { throw new RuntimeError('Variable "userHasEmail" does not exist.', 34, $this->source); })()), "html", null, true);
        echo "\" == false) {
                \$.ajax({
                    url: \"";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_user_secure_check_email_unique");
        echo "\",
                    type: \"POST\",
                    data: {email: email},
                    success: function (result) {
                        clearError()
                        \$.ajax({
                            url: \"";
        // line 42
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_user_secure_set_email");
        echo "\",
                            type: \"POST\",
                            data: {email: email},
                            success: function (result) {
                                clearError()
                                \$.ajax({
                                    url: \"";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_user_secure_email_send_verify_email");
        echo "\",
                                    type: \"POST\",
                                    data: {},
                                    success: function (result) {
                                        <!-- Алерт об успехе -->
                                        clearError()
                                        \$('#email-field').prop('disabled', true)
                                        \$('#verificationCode').show()
                                    },
                                    error: function (error) {
                                        \$('#verification-email-error').html('Что-то пошло не так. Извиняемся за неудобства')
                                    }
                                })
                            },
                            error: function (error) {
                                console.log(error)
                                \$('#verification-email-error').html('Пользователь с таким email уже зарегестрирован')
                            }
                        })
                    },
                    error: function (error) {
                        console.log(error)
                        \$('#verification-email-error').html('Пользователь с таким Email уже существует')
                    }
                })
            } else {
                \$.ajax({
                    url: \"";
        // line 75
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_user_secure_set_email");
        echo "\",
                    type: \"POST\",
                    data: {email: email},
                    success: function (result) {
                        clearError()
                        \$.ajax({
                            url: \"";
        // line 81
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_user_secure_email_send_verify_email");
        echo "\",
                            type: \"POST\",
                            data: {},
                            success: function (result) {
                                <!-- Алерт об успехе -->
                                clearError()
                                \$('#email-field').prop('disabled', true)
                                \$('#verificationCode').show()
                            },
                            error: function (error) {
                                \$('#verification-email-error').html('Что-то пошло не так. Извиняемся за неудобства')
                            }
                        })
                    },
                    error: function (error) {
                        console.log(error)
                        \$('#verification-email-error').html('Пользователь с таким email уже зарегестрирован')
                    }
                })
            }
        })

        \$('#verificationCode').on('keyup', function (e) {
            e.preventDefault()
            var length = \$(this).val().length
            var code = \$(this).val()
            if (length === 6) {
                \$.ajax({
                    url: \"";
        // line 109
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_user_secure_email_check_verify_email_code");
        echo "\",
                    type: \"POST\",
                    data: {code: code},
                    success: function () {
                        clearError()
                        \$('#verification-email-success').html('Ваш email верифицирован!')
                        \$('#email-field').prop('disabled', true)
                        \$('#verificationCode').hide()
                        \$('#verificationCode').val('')
                    },
                    error: function (error) {
                        \$('#verification-email-error').html('Неверный код')
                        \$('#verificationCode').val('')
                    }
                })
            }
        })

        // Отменяет Ctrl + V
        \$('#verificationCode').bind('cut copy paste', function (e) {
            e.preventDefault()
        })

        function clearError() {
            \$('#verification-email-error').html('')
        }
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "app/settings/verification/email_verification.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 109,  213 => 81,  204 => 75,  174 => 48,  165 => 42,  156 => 36,  151 => 34,  140 => 25,  130 => 24,  114 => 16,  110 => 14,  104 => 12,  101 => 11,  97 => 9,  95 => 8,  91 => 6,  81 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'app/base.html.twig' %}

{% block title %} {{ title('app/settings/verification/email_verification.html.twig') }} {% endblock %}

{% block body %}
    <span id=\"verification-email-success\"></span>
    <span id=\"verification-email-error\"></span>
    {% if getUser().emailVerified == true %}
        <p>Ваш Email верифицирован! Чтобы изменить его - свяжитесь с администрацией сайта</p>
    {% endif %}
    {% if userHasEmail %}
        <input type=\"email\" name=\"email\" value=\"{{ getUser().getEmail() }}\" id=\"email-field\" disabled required>
    {% else %}
        <input type=\"email\" name=\"email\" value=\"\" id=\"email-field\" required>
    {% endif %}
    <button type=\"submit\" id=\"send-verification-email\">Отправить письмо</button>

    <form id=\"verify-verification-code\">
        <input type=\"text\" maxlength=\"6\" minlength=\"6\" id=\"verificationCode\">
    </form>

{% endblock %}

{% block scripts %}
    <script src=\"https://code.jquery.com/jquery-3.6.0.js\"
            integrity=\"sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=\" crossorigin=\"anonymous\"></script>
    <script>
        \$('#verificationCode').hide()

        \$('#send-verification-email').on('click', function (e) {
            e.preventDefault()
            const email = \$('#email-field').val()

            if (\"{{ userHasEmail }}\" == false) {
                \$.ajax({
                    url: \"{{ path('api_v1_user_secure_check_email_unique') }}\",
                    type: \"POST\",
                    data: {email: email},
                    success: function (result) {
                        clearError()
                        \$.ajax({
                            url: \"{{ path('api_v1_user_secure_set_email') }}\",
                            type: \"POST\",
                            data: {email: email},
                            success: function (result) {
                                clearError()
                                \$.ajax({
                                    url: \"{{ path('api_v1_user_secure_email_send_verify_email') }}\",
                                    type: \"POST\",
                                    data: {},
                                    success: function (result) {
                                        <!-- Алерт об успехе -->
                                        clearError()
                                        \$('#email-field').prop('disabled', true)
                                        \$('#verificationCode').show()
                                    },
                                    error: function (error) {
                                        \$('#verification-email-error').html('Что-то пошло не так. Извиняемся за неудобства')
                                    }
                                })
                            },
                            error: function (error) {
                                console.log(error)
                                \$('#verification-email-error').html('Пользователь с таким email уже зарегестрирован')
                            }
                        })
                    },
                    error: function (error) {
                        console.log(error)
                        \$('#verification-email-error').html('Пользователь с таким Email уже существует')
                    }
                })
            } else {
                \$.ajax({
                    url: \"{{ path('api_v1_user_secure_set_email') }}\",
                    type: \"POST\",
                    data: {email: email},
                    success: function (result) {
                        clearError()
                        \$.ajax({
                            url: \"{{ path('api_v1_user_secure_email_send_verify_email') }}\",
                            type: \"POST\",
                            data: {},
                            success: function (result) {
                                <!-- Алерт об успехе -->
                                clearError()
                                \$('#email-field').prop('disabled', true)
                                \$('#verificationCode').show()
                            },
                            error: function (error) {
                                \$('#verification-email-error').html('Что-то пошло не так. Извиняемся за неудобства')
                            }
                        })
                    },
                    error: function (error) {
                        console.log(error)
                        \$('#verification-email-error').html('Пользователь с таким email уже зарегестрирован')
                    }
                })
            }
        })

        \$('#verificationCode').on('keyup', function (e) {
            e.preventDefault()
            var length = \$(this).val().length
            var code = \$(this).val()
            if (length === 6) {
                \$.ajax({
                    url: \"{{ path('api_v1_user_secure_email_check_verify_email_code') }}\",
                    type: \"POST\",
                    data: {code: code},
                    success: function () {
                        clearError()
                        \$('#verification-email-success').html('Ваш email верифицирован!')
                        \$('#email-field').prop('disabled', true)
                        \$('#verificationCode').hide()
                        \$('#verificationCode').val('')
                    },
                    error: function (error) {
                        \$('#verification-email-error').html('Неверный код')
                        \$('#verificationCode').val('')
                    }
                })
            }
        })

        // Отменяет Ctrl + V
        \$('#verificationCode').bind('cut copy paste', function (e) {
            e.preventDefault()
        })

        function clearError() {
            \$('#verification-email-error').html('')
        }
    </script>
{% endblock %}
", "app/settings/verification/email_verification.html.twig", "/doctor-tillin/templates/app/settings/verification/email_verification.html.twig");
    }
}
