<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* app/settings/new_password.html.twig */
class __TwigTemplate_53fa53a93194ffa6dd2999642b820ae2d8ecee0238c1b12590fd8fab6d8d0708 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'scripts' => [$this, 'block_scripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "app/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "app/settings/new_password.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "app/settings/new_password.html.twig"));

        $this->parent = $this->loadTemplate("app/base.html.twig", "app/settings/new_password.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\SEO\TitleExtension']->resolveTitle("app/settings/new_password.html.twig"), "html", null, true);
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <h1>Новый пароль</h1>
    <p id=\"new-password-errors\"></p>
    <p id=\"new-password-success\"></p>
    <form id=\"new-password-form\">
        <input type=\"password\" name=\"oldPassword\" id=\"new-password-form-old-password-field\">
        <input type=\"password\" name=\"newPassword\" id=\"new-password-form-new-password-field\">
        <input type=\"password\" name=\"newPasswordRepeat\" id=\"new-password-form-new-password-repeat-field\">
        <button type=\"submit\" style=\"cursor: pointer;\">Сохранить</button>
    </form>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 17
    public function block_scripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "scripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "scripts"));

        // line 18
        echo "    <script src=\"https://code.jquery.com/jquery-3.6.0.js\"
            integrity=\"sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=\" crossorigin=\"anonymous\"></script>
    <script>
        \$(document).on('submit', '#new-password-form', function (e) {
            e.preventDefault()
            var oldPassword = \$('#new-password-form-old-password-field').val()
            var newPassword = \$('#new-password-form-new-password-field').val()
            var newPasswordRepeat = \$('#new-password-form-new-password-repeat-field').val()

            if (newPassword !== newPasswordRepeat) {
                \$('#new-password-errors').html('Пароли не совпадают')

                return null;
            }

            \$.ajax({
                url: \"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_app_security_check_password");
        echo "\",
                type: \"POST\",
                data: {oldPassword: oldPassword},
                success: function (result) {
                    changePassword(newPassword)
                },
                error: function (e) {
                    \$('#new-password-errors').html('Старый пароль неверный')
                }
            })
        })

        function changePassword(newPassword) {
            \$.ajax({
                url: \"";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("api_v1_app_security_change_password");
        echo "\",
                type: \"POST\",
                data: {newPassword: newPassword},
                success: function (result) {
                    \$('#new-password-form').trigger(\"reset\")
                    \$('#new-password-success').html('Пароль успешно изменен!')
                },
                error: function (e) {
                    alert('Something went wrong. Sorry :(')
                }
            })
        }

        // \$('#new-password-form').on('submit', function (event) {
        //     event.preventDefault()
        //     console.log(\$(this).serialize())
        // })
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "app/settings/new_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 48,  138 => 34,  120 => 18,  110 => 17,  91 => 6,  81 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'app/base.html.twig' %}

{% block title %} {{ title('app/settings/new_password.html.twig') }} {% endblock %}

{% block body %}
    <h1>Новый пароль</h1>
    <p id=\"new-password-errors\"></p>
    <p id=\"new-password-success\"></p>
    <form id=\"new-password-form\">
        <input type=\"password\" name=\"oldPassword\" id=\"new-password-form-old-password-field\">
        <input type=\"password\" name=\"newPassword\" id=\"new-password-form-new-password-field\">
        <input type=\"password\" name=\"newPasswordRepeat\" id=\"new-password-form-new-password-repeat-field\">
        <button type=\"submit\" style=\"cursor: pointer;\">Сохранить</button>
    </form>
{% endblock %}

{% block scripts %}
    <script src=\"https://code.jquery.com/jquery-3.6.0.js\"
            integrity=\"sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=\" crossorigin=\"anonymous\"></script>
    <script>
        \$(document).on('submit', '#new-password-form', function (e) {
            e.preventDefault()
            var oldPassword = \$('#new-password-form-old-password-field').val()
            var newPassword = \$('#new-password-form-new-password-field').val()
            var newPasswordRepeat = \$('#new-password-form-new-password-repeat-field').val()

            if (newPassword !== newPasswordRepeat) {
                \$('#new-password-errors').html('Пароли не совпадают')

                return null;
            }

            \$.ajax({
                url: \"{{ path('api_v1_app_security_check_password') }}\",
                type: \"POST\",
                data: {oldPassword: oldPassword},
                success: function (result) {
                    changePassword(newPassword)
                },
                error: function (e) {
                    \$('#new-password-errors').html('Старый пароль неверный')
                }
            })
        })

        function changePassword(newPassword) {
            \$.ajax({
                url: \"{{ path('api_v1_app_security_change_password') }}\",
                type: \"POST\",
                data: {newPassword: newPassword},
                success: function (result) {
                    \$('#new-password-form').trigger(\"reset\")
                    \$('#new-password-success').html('Пароль успешно изменен!')
                },
                error: function (e) {
                    alert('Something went wrong. Sorry :(')
                }
            })
        }

        // \$('#new-password-form').on('submit', function (event) {
        //     event.preventDefault()
        //     console.log(\$(this).serialize())
        // })
    </script>
{% endblock %}
", "app/settings/new_password.html.twig", "/doctor-tillin/templates/app/settings/new_password.html.twig");
    }
}
