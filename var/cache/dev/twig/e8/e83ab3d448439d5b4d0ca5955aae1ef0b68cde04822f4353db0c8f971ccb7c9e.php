<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* app/blocks/template_header-with-text.html.twig */
class __TwigTemplate_8995fa2e891e4c606c2b2ec7334c075451ab4b28b1975da569c5186f46efca5e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "app/blocks/template_header-with-text.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "app/blocks/template_header-with-text.html.twig"));

        // line 1
        echo "<div class=\"template_wrapper\">
    <div class=\"template_header\">
        <a class=\"template_header--logo\">DoctorTillin</a>
    </div>
    <div class=\"template_content\">
        <button class=\"template_button--prev\" type=\"submit\" href=\"#\"><img
                    src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo/button-prev.png"), "html", null, true);
        echo "\"
                    alt=\"Back\" width=\"15px\" height=\"25px\">
        </button>
        <div class=\"template-text--after-button\">
            <h1 class=\"template-text--after-button\">Регистрация</h1>
        </div>
    </div>
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "app/blocks/template_header-with-text.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 7,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"template_wrapper\">
    <div class=\"template_header\">
        <a class=\"template_header--logo\">DoctorTillin</a>
    </div>
    <div class=\"template_content\">
        <button class=\"template_button--prev\" type=\"submit\" href=\"#\"><img
                    src=\"{{ asset('img/logo/button-prev.png') }}\"
                    alt=\"Back\" width=\"15px\" height=\"25px\">
        </button>
        <div class=\"template-text--after-button\">
            <h1 class=\"template-text--after-button\">Регистрация</h1>
        </div>
    </div>
</div>
", "app/blocks/template_header-with-text.html.twig", "/doctor-tillin/templates/app/blocks/template_header-with-text.html.twig");
    }
}
