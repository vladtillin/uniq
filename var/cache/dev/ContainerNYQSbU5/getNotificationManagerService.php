<?php

namespace ContainerNYQSbU5;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getNotificationManagerService extends App_KernelDevContainer
{
    /*
     * Gets the private 'App\Manager\User\Notification\NotificationManager' shared autowired service.
     *
     * @return \App\Manager\User\Notification\NotificationManager
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['App\\Manager\\User\\Notification\\NotificationManager'] = new \App\Manager\User\Notification\NotificationManager(($container->privates['App\\Repository\\User\\Notification\\NotificationRepository'] ?? $container->load('getNotificationRepositoryService')));
    }
}
