<?php

namespace ContainerNYQSbU5;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getDoctorUserTypeCrudControllerService extends App_KernelDevContainer
{
    /*
     * Gets the public 'App\Controller\Admin\User\DoctorUserTypeCrudController' shared autowired service.
     *
     * @return \App\Controller\Admin\User\DoctorUserTypeCrudController
     */
    public static function do($container, $lazyLoad = true)
    {
        $container->services['App\\Controller\\Admin\\User\\DoctorUserTypeCrudController'] = $instance = new \App\Controller\Admin\User\DoctorUserTypeCrudController(new \App\Manager\User\AbstractUserTypeManager(($container->privates['App\\Repository\\User\\AbstractUserTypeRepository'] ?? $container->load('getAbstractUserTypeRepositoryService'))), ($container->privates['App\\Manager\\Admin\\AdminLogManager'] ?? $container->load('getAdminLogManagerService')));

        $instance->setContainer(($container->privates['.service_locator.kOEw5YM'] ?? $container->load('get_ServiceLocator_KOEw5YMService'))->withContext('App\\Controller\\Admin\\User\\DoctorUserTypeCrudController', $container));

        return $instance;
    }
}
