<?php

namespace ContainerNYQSbU5;

class IpWhitelistCondition_82bfceb extends \Scheb\TwoFactorBundle\Security\TwoFactor\Condition\IpWhitelistCondition implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolderdd4cd = null;
    private $initializerfcc8d = null;
    private static $publicProperties771d1 = [
        
    ];
    public function shouldPerformTwoFactorAuthentication(\Scheb\TwoFactorBundle\Security\TwoFactor\AuthenticationContextInterface $context) : bool
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'shouldPerformTwoFactorAuthentication', array('context' => $context), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;
        return $this->valueHolderdd4cd->shouldPerformTwoFactorAuthentication($context);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Scheb\TwoFactorBundle\Security\TwoFactor\Condition\IpWhitelistCondition $instance) {
            unset($instance->ipWhitelistProvider);
        }, $instance, 'Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Condition\\IpWhitelistCondition')->__invoke($instance);
        $instance->initializerfcc8d = $initializer;
        return $instance;
    }
    public function __construct(private \Scheb\TwoFactorBundle\Security\TwoFactor\IpWhitelist\IpWhitelistProviderInterface $ipWhitelistProvider)
    {
        static $reflection;
        if (! $this->valueHolderdd4cd) {
            $reflection = $reflection ?? new \ReflectionClass('Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Condition\\IpWhitelistCondition');
            $this->valueHolderdd4cd = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Scheb\TwoFactorBundle\Security\TwoFactor\Condition\IpWhitelistCondition $instance) {
            unset($instance->ipWhitelistProvider);
        }, $this, 'Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Condition\\IpWhitelistCondition')->__invoke($this);
        }
        $this->valueHolderdd4cd->__construct($ipWhitelistProvider);
    }
    public function & __get($name)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, '__get', ['name' => $name], $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;
        if (isset(self::$publicProperties771d1[$name])) {
            return $this->valueHolderdd4cd->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Condition\\IpWhitelistCondition');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderdd4cd;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolderdd4cd;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;
        $realInstanceReflection = new \ReflectionClass('Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Condition\\IpWhitelistCondition');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderdd4cd;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolderdd4cd;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, '__isset', array('name' => $name), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;
        $realInstanceReflection = new \ReflectionClass('Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Condition\\IpWhitelistCondition');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderdd4cd;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolderdd4cd;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, '__unset', array('name' => $name), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;
        $realInstanceReflection = new \ReflectionClass('Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Condition\\IpWhitelistCondition');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderdd4cd;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolderdd4cd;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, '__clone', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;
        $this->valueHolderdd4cd = clone $this->valueHolderdd4cd;
    }
    public function __sleep()
    {
        $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, '__sleep', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;
        return array('valueHolderdd4cd');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Scheb\TwoFactorBundle\Security\TwoFactor\Condition\IpWhitelistCondition $instance) {
            unset($instance->ipWhitelistProvider);
        }, $this, 'Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Condition\\IpWhitelistCondition')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerfcc8d = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerfcc8d;
    }
    public function initializeProxy() : bool
    {
        return $this->initializerfcc8d && ($this->initializerfcc8d->__invoke($valueHolderdd4cd, $this, 'initializeProxy', array(), $this->initializerfcc8d) || 1) && $this->valueHolderdd4cd = $valueHolderdd4cd;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderdd4cd;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderdd4cd;
    }
}

if (!\class_exists('IpWhitelistCondition_82bfceb', false)) {
    \class_alias(__NAMESPACE__.'\\IpWhitelistCondition_82bfceb', 'IpWhitelistCondition_82bfceb', false);
}
