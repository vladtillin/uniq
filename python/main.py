import telebot
from telebot import types
import mysql.connector
import os

bot = telebot.TeleBot(os.getenv('TELEGRAM_BOT_TOKEN'), parse_mode=None)

doctor_tillin_database = mysql.connector.connect(
    host='doctor_tillin_mysql',
    port=os.getenv('MYSQL_EXPOSE_PORT'),
    user='root',
    password=os.getenv('MYSQL_ROOT_PASSWORD'),
    database=os.getenv('MYSQL_DATABASE')

)


@bot.message_handler(commands=['start'])
def start(message):
    keyboard = types.ReplyKeyboardMarkup(one_time_keyboard=True, row_width=1)
    reg_button = types.KeyboardButton(text="Send phone number", request_contact=True)
    keyboard.add(reg_button)
    bot.send_message(message.chat.id, 'Send me your phone number, for verify', reply_markup=keyboard)


@bot.message_handler(content_types=["contact"])
def contact(message):
    sender_id = message.from_user.id
    contact_id = message.contact.user_id
    phone_number = message.contact.phone_number

    if sender_id == contact_id:
        user_data = is_exist(phone_number)
        if user_data is None:
            bot.send_message(message.chat.id, 'Invalid phone number')
        else:
            cursor = doctor_tillin_database.cursor(dictionary=True)
            cursor.execute("UPDATE user SET phone_number_verified = 1 WHERE id = " + str(user_data['id']))
            doctor_tillin_database.commit()
            cursor.close()
            bot.send_message(message.chat.id, 'Verification success for ' + user_data['nickname'] + ' :)')

    else:
        bot.send_message(message.chat.id, 'Invalid contact')


def is_exist(phone_number):
    cursor = doctor_tillin_database.cursor(dictionary=True)
    cursor.execute("SELECT * FROM user WHERE phone_number = " + str(phone_number))
    result = cursor.fetchone()
    cursor.close()
    if result is None:
        return None
    else:
        return {'id': result['id'], 'nickname': result['nickname']}


if __name__ == '__main__':
    bot.infinity_polling()
