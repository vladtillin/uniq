<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220424034959 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE admin_log (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, log_text LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted TINYINT(1) NOT NULL, INDEX IDX_F9383BB0A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, abstract_user_type_id INT DEFAULT NULL, google_authenticator_id INT DEFAULT NULL, email VARCHAR(180) DEFAULT NULL, phone_number VARCHAR(255) DEFAULT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, nickname VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D6496B01BC5B (phone_number), UNIQUE INDEX UNIQ_8D93D649A188FE64 (nickname), UNIQUE INDEX UNIQ_8D93D649E2A2BA8 (abstract_user_type_id), UNIQUE INDEX UNIQ_8D93D64960529F2F (google_authenticator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_abstract_user_type (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, type VARCHAR(32) NOT NULL, UNIQUE INDEX UNIQ_8F8B11DBA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_doctor_doctor_verification (id INT AUTO_INCREMENT NOT NULL, doctor_user_type_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, patronymic VARCHAR(255) NOT NULL, selfie_file_id LONGTEXT NOT NULL, document_front_side_file_id LONGTEXT NOT NULL, document_bask_side_file_id LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_13A7CAA2B7A986B9 (doctor_user_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_doctor_user_type (id INT NOT NULL, verified TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_notification_notification (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, messages_notifications TINYINT(1) NOT NULL, updates_notifications TINYINT(1) NOT NULL, advertising_notifications TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_B2F05277A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_patient_user_type (id INT NOT NULL, doctor_id INT DEFAULT NULL, INDEX IDX_A8CB72F487F4FB17 (doctor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_profile_data (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, patronymic VARCHAR(255) DEFAULT NULL, birth_date DATETIME DEFAULT NULL, avatar LONGTEXT DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, language VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_9BF700F5A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_security_auth_history (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, identifier VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted TINYINT(1) NOT NULL, INDEX IDX_930FE35FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_security_google_authenticator (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, secret_code VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_2CA6DE92A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE admin_log ADD CONSTRAINT FK_F9383BB0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649E2A2BA8 FOREIGN KEY (abstract_user_type_id) REFERENCES user_abstract_user_type (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64960529F2F FOREIGN KEY (google_authenticator_id) REFERENCES user_security_google_authenticator (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE user_abstract_user_type ADD CONSTRAINT FK_8F8B11DBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_doctor_doctor_verification ADD CONSTRAINT FK_13A7CAA2B7A986B9 FOREIGN KEY (doctor_user_type_id) REFERENCES user_doctor_user_type (id)');
        $this->addSql('ALTER TABLE user_doctor_user_type ADD CONSTRAINT FK_445838D6BF396750 FOREIGN KEY (id) REFERENCES user_abstract_user_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_notification_notification ADD CONSTRAINT FK_B2F05277A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_patient_user_type ADD CONSTRAINT FK_A8CB72F487F4FB17 FOREIGN KEY (doctor_id) REFERENCES user_doctor_user_type (id)');
        $this->addSql('ALTER TABLE user_patient_user_type ADD CONSTRAINT FK_A8CB72F4BF396750 FOREIGN KEY (id) REFERENCES user_abstract_user_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_profile_data ADD CONSTRAINT FK_9BF700F5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_security_auth_history ADD CONSTRAINT FK_930FE35FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_security_google_authenticator ADD CONSTRAINT FK_2CA6DE92A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin_log DROP FOREIGN KEY FK_F9383BB0A76ED395');
        $this->addSql('ALTER TABLE user_abstract_user_type DROP FOREIGN KEY FK_8F8B11DBA76ED395');
        $this->addSql('ALTER TABLE user_notification_notification DROP FOREIGN KEY FK_B2F05277A76ED395');
        $this->addSql('ALTER TABLE user_profile_data DROP FOREIGN KEY FK_9BF700F5A76ED395');
        $this->addSql('ALTER TABLE user_security_auth_history DROP FOREIGN KEY FK_930FE35FA76ED395');
        $this->addSql('ALTER TABLE user_security_google_authenticator DROP FOREIGN KEY FK_2CA6DE92A76ED395');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649E2A2BA8');
        $this->addSql('ALTER TABLE user_doctor_user_type DROP FOREIGN KEY FK_445838D6BF396750');
        $this->addSql('ALTER TABLE user_patient_user_type DROP FOREIGN KEY FK_A8CB72F4BF396750');
        $this->addSql('ALTER TABLE user_doctor_doctor_verification DROP FOREIGN KEY FK_13A7CAA2B7A986B9');
        $this->addSql('ALTER TABLE user_patient_user_type DROP FOREIGN KEY FK_A8CB72F487F4FB17');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64960529F2F');
        $this->addSql('DROP TABLE admin_log');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_abstract_user_type');
        $this->addSql('DROP TABLE user_doctor_doctor_verification');
        $this->addSql('DROP TABLE user_doctor_user_type');
        $this->addSql('DROP TABLE user_notification_notification');
        $this->addSql('DROP TABLE user_patient_user_type');
        $this->addSql('DROP TABLE user_profile_data');
        $this->addSql('DROP TABLE user_security_auth_history');
        $this->addSql('DROP TABLE user_security_google_authenticator');
    }
}
