/*
 * Copyright (c) 2022. This site is part of DoctorTillin project.
 */

$(document).on('click', '#auth-show-hide-password', function (e) {
    e.preventDefault();

    if ($('#inputPassword').attr('type') === 'password') {
        $('#inputPassword').attr('type', 'text');
    } else {
        $('#inputPassword').attr('type', 'password');
    }
})